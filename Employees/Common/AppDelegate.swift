//
//  AppDelegate.swift
//  Employees
//
//  Created by Sergey Erokhin on 20/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

let EmployeesServerURLPrefix = "http://app.gossluzhba.mosreg.ru"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    UserDefaults.standard.set(["ru"], forKey: "AppleLanguages")
    UserDefaults.standard.synchronize()
    Fabric.with([Crashlytics.self])
    if !URLProtocol.registerClass(LocalProtocol.self) {
      fatalError("Can't register LocalProtocol")
    }
    return true
  }

  func applicationDidEnterBackground(_ application: UIApplication) {
    EventLog.instance.add("Приложение в фоне")
    EventLog.instance.save()
  }

  func applicationDidBecomeActive(_ application: UIApplication) {
    EventLog.instance.sendAll()
  }

}
