//
//  AvatarProcessor.swift
//  Employees
//
//  Created by Sergey Erokhin on 02.04.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import Kingfisher

final class AvatarProcessor : ImageProcessor {

  private(set) var identifier: String
  let width: CGFloat

  init(width: CGFloat) {
    self.width = width
    identifier = "avatar\(Int(width))"
  }

  func process(item: ImageProcessItem, options: KingfisherOptionsInfo) -> Image? {
    switch item {
    case .data(let data):
      guard let image = UIImage(data: data) else {
        return nil
      }
      return process(image: image)
    case .image(let image):
      return process(image: image)
    }
  }

  private func process(image: UIImage) -> UIImage? {
    let width = min(self.width, min(image.size.width, image.size.height))
    let size = CGSize(width: width, height: width)
    var rect = CGRect(origin: .zero, size: size)
    UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
    let path = UIBezierPath(roundedRect: rect, cornerRadius: size.width * 0.5)
    UIGraphicsGetCurrentContext()?.setFillColor(UIColor.clear.cgColor)
    UIGraphicsGetCurrentContext()?.fill(rect)
    path.addClip()
    let ratio = image.size.width / image.size.height
    if ratio > 1 {
      rect.size.width *= ratio
      rect.origin.x -= (rect.size.width - rect.size.height) / 2
    } else if ratio < 1 {
      rect.size.height /= ratio
      rect.origin.y -= (rect.size.height - rect.size.width) / 2
    }
    image.draw(in: rect)
    let result = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return result
  }
  
}
