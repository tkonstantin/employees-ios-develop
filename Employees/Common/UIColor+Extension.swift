//
//  UIColor+Extension.swift
//  Employees
//
//  Created by Sergey Erokhin on 24/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

extension UIColor {

  convenience init(rgba: UInt32) {
    self.init(
      red: CGFloat((rgba >> 24) & 0xff) / 255,
      green: CGFloat((rgba >> 16) & 0xff) / 255,
      blue: CGFloat((rgba >> 8) & 0xff) / 255,
      alpha: CGFloat(rgba & 0xff) / 255
    )
  }

  class var gubDarkSeafoamGreen: UIColor {
    return UIColor(rgba: 0x38a97bff)
  }

  class var gubSeparator: UIColor {
    return UIColor(rgba: 0xcfcfcfff)
  }

  class var gubGreyish: UIColor {
    return UIColor(rgba: 0xb7b7b7ff)
  }

  class var gubAccent: UIColor {
    return UIColor(rgba: 0x2589deff)
  }

  class var gubAlert1: UIColor {
    return UIColor(rgba: 0x2589deff)
  }

  class var gubAlert2: UIColor {
    return UIColor(rgba: 0xfbcc01ff)
  }

  class var gubAlert3: UIColor {
    return UIColor(rgba: 0xf83837ff)
  }

  class var gubGrid: UIColor {
    return UIColor(rgba: 0xcfcfcfff)
  }

  class var gubGraphLine: UIColor {
    return UIColor(rgba: 0xc2c6cdff)
  }

  class var gubNavbar: UIColor {
    return UIColor(rgba: 0x0350a2ff)
  }
  
}
