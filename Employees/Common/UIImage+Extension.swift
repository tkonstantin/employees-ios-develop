//
//  UIImage+Extension.swift
//  Employees
//
//  Created by Sergey Erokhin on 17/04/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

extension UIImage {

  convenience init(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
    let rect = CGRect(origin: .zero, size: size)
    UIGraphicsBeginImageContextWithOptions(size, false, 0)
    color.setFill()
    UIRectFill(rect)
    let image = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    self.init(cgImage: image.cgImage!)
  }
}
