//
//  UIView+Extension.swift
//  Employees
//
//  Created by Sergey Erokhin on 21/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

extension UIView {

  public func findViewController() -> UIViewController? {
    var responder = self.next
    while responder != nil {
      if let vc = responder as? UIViewController {
        return vc
      }
      responder = responder!.next
    }
    return nil
  }

}
