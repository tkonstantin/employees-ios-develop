//
//  CabinetController.swift
//  Employees
//
//  Created by Sergey Erokhin on 14/04/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import RxSwift

class DashBoardSection {

  let status: OrderStatus
  private(set) var orders: [Order] = []

  enum Event {
    case reloaded
    case add(CountableRange<Int>)
    case remove(CountableRange<Int>)
  }

  private var eventsSubject = PublishSubject<Event>()

  var events: Observable<Event> {
    return eventsSubject.asObservable()
  }

  var count: Int { return orders.count }

  subscript(index: Int) -> Order {
    return orders[index]
  }

  init(_ status: OrderStatus) {
    self.status = status
  }

  fileprivate func reload(orders: [Order]) {
    self.orders.removeAll()
    for order in orders {
      if order.status == status {
        self.orders.append(order)
      }
    }
    eventsSubject.onNext(.reloaded)
  }

  fileprivate func add(order: Order) {
    guard order.status == status && !orders.contains(where: { $0.id == order.id }) else { return }
    let index = orders.count
    orders.append(order)
    eventsSubject.onNext(.add(index ..< index + 1))
  }

  fileprivate func remove(order: Order) {
    guard order.status == status, let index = orders.index(where: { $0.id == order.id }) else { return }
    orders.remove(at: index)
    eventsSubject.onNext(.remove(index ..< index + 1))
  }

}

class DashBoard {

  static let shared = DashBoard()

  private(set) var sections: [OrderStatus : DashBoardSection] = [:]
  private(set) var isNeedReload: Bool = true
  private(set) var isEmpty: Bool = true

  private init() {
    for status: OrderStatus in [.new, .confirmed, .pending, .done, .canceled] {
      sections[status] = DashBoardSection(status)
    }
  }

  func createOrder(_ type: OrderType, position: Position) -> Observable<Order> {
    return Client.instance.createOrder(type, position: position).flatMap({ order -> Observable<Order> in
      self.setNeedReload()
      return .just(order)
    })
  }

  func move(_ order: Order, to: OrderStatus) -> Observable<Void> {
    guard order.status != to else { return .just() }
    if to == .canceled || to == .done {
      FakeAction.set(.none, for: order.position!.id)
    }
    return Client.instance.move(order, to: to).flatMap({ newOrder -> Observable<Void> in
      self.sections[order.status]?.remove(order: order)
      self.sections[to]?.add(order: newOrder)
      return .just()
    })
  }

  func updateAll(oldStatus: OrderStatus, newStatus: OrderStatus) -> Observable<Void> {
    guard oldStatus != newStatus else { return .just() }
    if newStatus == .canceled || newStatus == .done {
      sections[oldStatus]?.orders.forEach {
        FakeAction.set(.none, for: $0.position!.id)
      }
    }
    return Client.instance.moveAllOrders(from: oldStatus, to: newStatus).flatMap({ orders -> Observable<Void> in
      self.isNeedReload = false
      self.isEmpty = orders.isEmpty
      self.sections.forEach {
        $0.value.reload(orders: orders)
      }
      return .just()
    })
  }

  func setNeedReload() {
    isNeedReload = true
  }

  func reload(force: Bool = false) -> Observable<Void> {
    guard force || isNeedReload else {
      return .just()
    }
    return Client.instance.getOrders().flatMap({ orders -> Observable<Void> in
      self.isNeedReload = false
      self.isEmpty = orders.isEmpty
      self.sections.forEach {
        $0.value.reload(orders: orders)
      }
      return .just()
    })
  }

}

final class CabinetController : SearchBaseController {

  @IBOutlet private var columnViews: [OrdersColumnView] = []
  @IBOutlet private var placeholderView: UIView!
  @IBOutlet private var placeholderTextView: UITextView!
  private var dashboard = DashBoard.shared

  override func viewDidLoad() {
    super.viewDidLoad()
    let statuses: [OrderStatus] = [.new, .confirmed, .pending, .done, .canceled]
    let bbcode = BBCodeHelper()
    bbcode.textColor = UIColor(rgba: 0x868686ff)
    bbcode.font = .systemFont(ofSize: 18)
    placeholderTextView.linkTextAttributes = String.attributes(.fg(UIColor(rgba: 0x2073d6ff)))
    placeholderTextView.attributedText = bbcode.translate(text: "Сейчас поручений нет, вы можете создавать поручения\nпри просмотре карточек сотрудников в окне [url=tab://struct]Cтруктура[/url]")
    statuses.enumerated().forEach {
      columnViews[$0.offset].section = dashboard.sections[$0.element]
    }
    reload()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    if dashboard.isNeedReload {
      reload()
    }
  }

  private func reload() {
    dashboard.reload().subscribe(onNext: { [weak self] in
      guard let `self` = self else { return }
      self.placeholderView.isHidden = !self.dashboard.isEmpty
      print("Dashboard reloaded")
    }, onError: { error in
      print("Dashboard reloading failed: \(error.localizedDescription)")
    }).addDisposableTo(disposeBag)
  }

}

extension CabinetController : UITextViewDelegate {

  func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
    if url.absoluteString == "tab://struct" {
      rootController?.show(tab: .structure)
    }
    return false
  }

}
