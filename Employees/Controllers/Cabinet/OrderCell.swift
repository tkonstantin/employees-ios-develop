//
//  OrderCell.swift
//  Employees
//
//  Created by Sergey Erokhin on 17/04/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import KVNProgress
import RxSwift

final class OrderCell : UITableViewCell {

  @IBOutlet private var avatarImageView: UIImageView!
  @IBOutlet private var positionLabel: UILabel!
  @IBOutlet private var divisionLabel: UILabel!
  @IBOutlet private var nameLabel: UILabel!
  @IBOutlet private var dateLabel: UILabel!
  @IBOutlet private var menuButton: UIButton!
  @IBOutlet private var lineView: UIImageView!
  @IBOutlet private var typeView: UIImageView!

  private var disposeBag = DisposeBag()

  let dateFormatter: DateFormatter = {
    let df = DateFormatter()
    df.dateFormat = "d.MM.yy 'в' HH:mm"
    return df
  }()

  var order: Order! {
    didSet {
      divisionLabel.text = order.position?.division?.name
      positionLabel.text = order.position?.name
      nameLabel.text = order.position?.person?.fullName
      dateLabel.text = dateFormatter.string(from: order.changeDate ?? Date())
      switch order.type {
      case .dismiss:
        lineView.image = UIImage(color: UIColor(rgba: 0xf83837ff))
        typeView.image = #imageLiteral(resourceName: "icOrderDismiss")
      case .training:
        lineView.image = UIImage(color: UIColor(rgba: 0x2589deff))
        typeView.image = #imageLiteral(resourceName: "icOrderTraining")
      case .translate:
        lineView.image = UIImage(color: UIColor(rgba: 0x62cb1dff))
        typeView.image = #imageLiteral(resourceName: "icOrderTranslate")
      }
      switch order.status {
      case .done, .canceled:
        menuButton.isHidden = true
      default:
        menuButton.isHidden = false
      }
      if let person = order?.position?.person {
        var placeholder: UIImage = #imageLiteral(resourceName: "placeholderAvatarArmchair")
        switch person.sex {
        case .male:
          placeholder = #imageLiteral(resourceName: "placeholderAvatarMale")
        case .female:
          placeholder = #imageLiteral(resourceName: "placeholderAvatarFeemale")
        default:
          break
        }
        if let url = person.photoURL {
          avatarImageView.kf.setImage(with: url, placeholder: placeholder, options: [.transition(.fade(0.3)), .processor(AvatarProcessor(width: 50))])
        } else {
          avatarImageView.image = placeholder
        }
      } else {
        avatarImageView.image = #imageLiteral(resourceName: "placeholderAvatarArmchair")
      }
    }
  }

  @IBAction func showMenu() {
    let alertVC = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    switch order.status {
    case .new:
      alertVC.addAction(UIAlertAction(title: "Утвердить", style: .default) { [unowned self] _ in
        self.setStatus(.confirmed)
      })
      alertVC.addAction(UIAlertAction(title: "Отменить", style: .default) { [unowned self] _ in
        self.setStatus(.canceled)
      })
    case .confirmed:
      alertVC.addAction(UIAlertAction(title: "На исполнение", style: .default) { [unowned self] _ in
        self.setStatus(.pending)
      })
      alertVC.addAction(UIAlertAction(title: "Отменить", style: .default) { [unowned self] _ in
        self.setStatus(.canceled)
      })
    case .pending:
      alertVC.addAction(UIAlertAction(title: "Исполнено", style: .default) { [unowned self] _ in
        self.setStatus(.done)
      })
      alertVC.addAction(UIAlertAction(title: "Отменить", style: .default) { [unowned self] _ in
        self.setStatus(.canceled)
      })
    default:
      break
    }
    alertVC.popoverPresentationController?.sourceView = menuButton
    alertVC.popoverPresentationController?.sourceRect = menuButton.convert(menuButton.frame, from: menuButton.superview!)
    findViewController()?.present(alertVC, animated: true)
  }

  private func setStatus(_ newStatus: OrderStatus) {
    KVNProgress.show()
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
      DashBoard.shared.move(self.order, to: newStatus).subscribe(onNext: { _ in
        KVNProgress.showSuccess()
      }, onError: { error in
        KVNProgress.showError(withStatus: error.localizedDescription)
      }).addDisposableTo(self.disposeBag)
    }
  }

  override func didAddSubview(_ subview: UIView) {
    super.didAddSubview(subview)
    if subview.frame.height <= 1 {
      subview.removeFromSuperview()
    }
  }

  override func awakeFromNib() {
    super.awakeFromNib()
  }

}
