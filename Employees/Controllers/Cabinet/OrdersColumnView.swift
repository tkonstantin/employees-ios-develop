//
//  OrdersColumnView.swift
//  Employees
//
//  Created by Sergey Erokhin on 17/04/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import KVNProgress

@IBDesignable
final class OrdersColumnView : UIView, UITableViewDataSource, UITableViewDelegate {

  @IBOutlet private var titleLabel: UILabel!
  @IBOutlet private var tableView: UITableView!
  @IBOutlet private var bottomView: UIView!
  @IBOutlet private var cancelButton: UIButton!
  @IBOutlet private var updateButton: UIButton!
  @IBOutlet private var removeAllButton: UIButton!

  private var disposeBag = DisposeBag()

  @IBInspectable
  var title: String = "" {
    didSet {
      titleLabel.text = title
    }
  }

  var section: DashBoardSection! {
    didSet {
      disposeBag = DisposeBag()
      section.events.subscribe(onNext: { [weak self] event in
        guard let `self` = self else { return }
        self.updateBottomView()
        switch event {
        case .reloaded:
          self.tableView.reloadData()
        case .add(let range):
          self.tableView.insertSections(IndexSet(integersIn: range), with: .automatic)
        case .remove(let range):
          self.tableView.deleteSections(IndexSet(integersIn: range), with: .automatic)
        }
      }).addDisposableTo(disposeBag)
      updateBottomView()
      tableView.reloadData()
    }
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  private func setup() {
    let nib = UINib(nibName: "OrdersColumnView", bundle: Bundle(for: type(of: self)))
    let view = nib.instantiate(withOwner: self, options: nil).first as! UIView

    addSubview(view)
    view.snp.makeConstraints {
      $0.top.equalToSuperview()
      $0.bottom.equalToSuperview()
      $0.left.equalToSuperview()
      $0.right.equalToSuperview()
    }

    tableView.tableFooterView = UIView()

    tableView.register(UINib(nibName: "OrderCell", bundle: Bundle(for: OrderCell.self)), forCellReuseIdentifier: "order")
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 200
    tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 1))
    tableView.contentInset.top = -1

    cancelButton.titleLabel?.numberOfLines = 0
    cancelButton.titleLabel?.textAlignment = .center

    bottomView.layer.borderWidth = 1 / UIScreen.main.scale
    bottomView.layer.borderColor = tableView.backgroundColor?.cgColor

    removeAllButton.layer.borderWidth = 1 / UIScreen.main.scale
    removeAllButton.layer.borderColor = tableView.backgroundColor?.cgColor
  }

  private func updateBottomView() {
    switch section.status {
    case .done, .canceled:
      bottomView.isHidden = true
      if section.status == .canceled {
        removeAllButton.isHidden = section.count == 0
      } else {
        removeAllButton.isHidden = true
      }
    default:
      if section.count > 0 {
        bottomView.isHidden = false
        updateButton.setTitle(String(section.count), for: .normal)
      } else {
        bottomView.isHidden = true
      }
      removeAllButton.isHidden = true
    }
  }

  @IBAction func cancelAllOrders() {
    setStatus(.canceled)
  }

  @IBAction func updateAllOrders() {
    switch section.status {
    case .new:
      setStatus(.confirmed)
    case .confirmed:
      setStatus(.pending)
    case .pending:
      setStatus(.done)
    default:
      break
    }
  }

  @IBAction func removeAll() {
    setStatus(.trash)
  }

  private func setStatus(_ newStatus: OrderStatus) {
    KVNProgress.show()
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
      DashBoard.shared.updateAll(oldStatus: self.section.status, newStatus: newStatus).subscribe(onNext: {
        KVNProgress.showSuccess()
      }, onError: { error in
        KVNProgress.showError(withStatus: error.localizedDescription)
      }).addDisposableTo(self.disposeBag)
    }
  }

  // MARK: - UITableViewDataSource

  func numberOfSections(in tableView: UITableView) -> Int {
    return section?.count ?? 0
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "order", for: indexPath) as! OrderCell
    cell.order = section[indexPath.section]
    return cell
  }

  // MARK: - UITableViewDelegate

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    guard let vc = findViewController(), let positionId = section[indexPath.section].position?.id else { return }
    let employeeVC = vc.storyboard!.instantiateViewController(withIdentifier: "employee") as! EmployeeController
    employeeVC.positionId = positionId
    vc.navigationController?.pushViewController(employeeVC, animated: true)
  }

  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return section > 0 ? 4 : 0
  }



}
