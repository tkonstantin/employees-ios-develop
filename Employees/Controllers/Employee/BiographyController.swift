//
//  BiographyController.swift
//  Employees
//
//  Created by Sergey Erokhin on 23/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

final class BiographyController : UIViewController {

  @IBOutlet private var tableView: UITableView!

  var biography: Biography? {
    didSet {
      tableView?.reloadData()
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 200
    tableView.register(UINib(nibName: "BiographyCell", bundle: nil), forCellReuseIdentifier: "item")
    tableView.alwaysBounceVertical = false
  }

}

// MARK: - UITableViewDataSource

extension BiographyController : UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return biography?.items.count ?? 0
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "item", for: indexPath) as! BiographyCell
    cell.item = biography?.items[indexPath.row]
    return cell
  }

}
