//
//  CompetencyController.swift
//  Employees
//
//  Created by Sergey Erokhin on 29.03.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import Charts

final class CompetencyController : UIViewController {

  @IBOutlet private var competenciesChartView: RadarChartView!
  @IBOutlet private var legendView: UIView!

  var competencies: [Competence] = []
  var hideRequired: Bool = false

  override func viewDidLoad() {
    super.viewDidLoad()
    setupChartView()
  }

  private func setupChartView() {
    if !competencies.isEmpty {

      var sets: [RadarChartDataSet] = []

      if !hideRequired {
        let set1 = RadarChartDataSet(values: competencies.map({ RadarChartDataEntry(value: Double($0.requiredValue)) }), label: "Требуемые компетенции")
        set1.fillAlpha = 0.7
        set1.lineWidth = 2
        set1.drawFilledEnabled = true
        set1.setColor(UIColor(rgba: 0x51ae83ff))
        set1.fillColor = set1.color(atIndex: 0)
        sets.append(set1)
      } else {
        legendView.isHidden = true
      }

      let set2 = RadarChartDataSet(values: competencies.map({ RadarChartDataEntry(value: Double($0.value)) }), label: "Компетенции сотрудника")
      set2.fillAlpha = 0.7
      set2.lineWidth = 2
      set2.drawFilledEnabled = true
      set2.setColor(UIColor(rgba: 0x2073d6ff))
      set2.fillColor = set2.color(atIndex: 0)
      sets.append(set2)

      let data = RadarChartData(dataSets: sets)
      data.setDrawValues(false)
      data.highlightEnabled = false

      competenciesChartView.data = data

      let xAxis = competenciesChartView.xAxis
      xAxis.valueFormatter = IndexAxisValueFormatter(values: competencies.map({ $0.name }))
      xAxis.labelFont = UIFont.systemFont(ofSize: 16)

      let yAxis = competenciesChartView.yAxis
      yAxis.drawLabelsEnabled = true
      yAxis.axisMinimum = 0

      competenciesChartView.legend.enabled = false
      competenciesChartView.webLineWidth = 1
      competenciesChartView.webColor = .gubGrid
    }
    competenciesChartView.noDataText = "Нет данных"
    competenciesChartView.noDataFont = UIFont.systemFont(ofSize: 16)
    competenciesChartView.chartDescription = nil
    competenciesChartView.rotationEnabled = false
  }
}
