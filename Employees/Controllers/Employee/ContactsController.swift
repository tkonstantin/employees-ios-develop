//
//  ContactsController.swift
//  Employees
//
//  Created by Sergey Erokhin on 23/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import SafariServices

final class ContactsController: UIViewController {

  @IBOutlet fileprivate var tableView1: UITableView!
  @IBOutlet fileprivate var tableView2: UITableView!

  fileprivate var contacts1: [Contact] = []
  fileprivate var contacts2: [Contact] = []

  var contacts: [Contact]! {
    didSet {
      contacts1.removeAll()
      contacts2.removeAll()
      contacts?.forEach {
        if $0.type.rawValue < 10 {
          contacts1.append($0)
        } else {
          contacts2.append($0)
        }
      }
      tableView1?.reloadData()
      tableView2?.reloadData()
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    let nib = UINib(nibName: "ContactCell", bundle: nil)
    for tableView in [tableView1, tableView2] {
      tableView?.register(nib, forCellReuseIdentifier: "contact")
      tableView?.alwaysBounceVertical = false
      tableView?.rowHeight = UITableViewAutomaticDimension
      tableView?.estimatedRowHeight = 60
    }
  }

  fileprivate func showInSafari(_ url: URL) {
    let vc = SFSafariViewController(url: url)
    vc.modalPresentationStyle = .formSheet
    vc.modalTransitionStyle = .crossDissolve
    var size = UIScreen.main.bounds.size
    size.width *= 0.8
    size.height *= 0.8
    vc.preferredContentSize = size
    present(vc, animated: true)
  }

}

// MARK: - UITableViewDataSource

extension ContactsController : UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tableView == tableView1 {
      return contacts1.count
    }
    return contacts2.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "contact") as! ContactCell
    if tableView == tableView1 {
      cell.contact = contacts1[indexPath.row]
    } else {
      cell.contact = contacts2[indexPath.row]
    }
    return cell
  }

}

// MARK: - UITableViewDelegate

extension ContactsController : UITableViewDelegate {

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    let cell = tableView.cellForRow(at: indexPath) as! ContactCell
    let contact = cell.contact!
    switch contact.type {
    case .facebook, .instagram, .googlePlus, .vkontakte, .twitter, .odnoklassniki, .www:
      var contactValue = contact.value
      if !contactValue.hasPrefix("http") {
        contactValue = "https://" + contactValue
      }
      guard let url = URL(string: contactValue) else { return }
      EventLog.instance.add("Ссылка \(url)")
      showInSafari(url)
    case .address:
      var urlBuilder = URLComponents(string: "https://maps.apple.com")!
      urlBuilder.query = "address=" + contact.value
      if let url = try? urlBuilder.asURL() {
        UIApplication.shared.openURL(url)
      }
    case .email:
      if let url = URL(string: "mailto:" + contact.value) {
        UIApplication.shared.openURL(url)
      }
    case .workPhone, .mobilePhone:
      let phone = try! NSRegularExpression(pattern: "[^\\+\\d]", options: []).stringByReplacingMatches(in: contact.value, options: [], range: NSRange(location: 0, length: contact.value.characters.count), withTemplate: "")
      if let url = URL(string: "facetime://" + phone) {
        UIApplication.shared.openURL(url)
      }
      break
    default:
      break
    }
  }

}
