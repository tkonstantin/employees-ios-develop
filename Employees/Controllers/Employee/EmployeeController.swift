//
//  EmployeeController.swift
//  Employees
//
//  Created by Sergey Erokhin on 21/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import RxSwift
import Kingfisher
import SafariServices
import ObjectMapper
import KVNProgress

final class EmployeeController : SearchBaseController {

  enum Tab {
    case commonInfo
    case bio
    case competence
    case kinships
    case contacts
    case reserve

    var title: String {
      switch self {
      case .commonInfo:
        return "Информация"
      case .bio:
        return "Биография"
      case .competence:
        return "Компетенции"
      case .kinships:
        return "Родственные связи"
      case .contacts:
        return "Контакты"
      case .reserve:
        return "Кадровый резерв"
      }
    }
  }

  @IBOutlet private var contentView: UIView!
  @IBOutlet private var tabControl: UISegmentedControl!
  @IBOutlet private var mainView: UIView!
  @IBOutlet private var activityIndicatorView: UIActivityIndicatorView!

  @IBOutlet private var avatarImageView: UIImageView!
  @IBOutlet private var infoLabel: UILabel!

  @IBOutlet private var fakeActionsView: UIView!
  @IBOutlet private var fakeActionsCancelView: UIView!
  @IBOutlet private var fakeActionLabel: UILabel!
  @IBOutlet private var attestationButton: UIButton!

  @IBOutlet private var actionsView: UIView!

  var positionId: Int = 0
  var personId: Int = 0

  private var position: Position? {
    didSet {
      guard let position = self.position else { return }
      if let person = position.person {
        actionsView.isHidden = position.id == 1
        attestationButton.isHidden = position.id == 1 || position.typeId == 1
        let fullName = person.fullName
        navigationItem.title = fullName
        infoLabel.attributedText = {
          let text = NSMutableAttributedString()
          if let divisionName = position.division?.name, !divisionName.isEmpty {
            text += (divisionName + "\n").attributed(with: .sysFont(17, .regular), .ps(20), .align(.center))
          }
          if let positionName = position.name, !positionName.isEmpty {
            text += (positionName + "\n").attributed(with: .sysFont(22, .bold), .ps(12), .align(.center))
          }
          if let fullName = position.person?.fullName, !fullName.isEmpty {
            text += fullName.attributed(with: .sysFont(20, .regular), .align(.center))
          }
          return text
        }()
        let placeholder: UIImage = person.sex == .male ? #imageLiteral(resourceName: "placeholderAvatarMale") : #imageLiteral(resourceName: "placeholderAvatarFeemale")
        if let url = person.photoURL {
          avatarImageView.kf.setImage(with: url, placeholder: placeholder, options: [.transition(.fade(0.3)), .processor(AvatarProcessor(width: 232))])
        } else {
          avatarImageView.image = placeholder
        }
      }
    }
  }

  private var person: Person?

  private var kinshipItems: [KinshipItem] = []
  private var subPositions: [Position] = []
  private var candidates: [VacancyCandidate] = []

  private var biography: Biography?

  private var tabs: [Tab] = [.commonInfo, .bio, .competence, .reserve, .contacts, .kinships]
  private var currentTab: Tab = .bio {
    didSet {
      EventLog.instance.add("Экран '\(position?.person?.fullName ?? "")' > \(tabControl.titleForSegment(at: tabControl.selectedSegmentIndex)!)")
      guard currentTab != oldValue else { return }
      switch currentTab {
      case .bio:
        let vc = storyboard?.instantiateViewController(withIdentifier: "biography") as! BiographyController
        if biography == nil {
          biography = Biography(person: person!)
        }
        vc.biography = biography
        currentVC = vc
      case .contacts:
        let vc = storyboard?.instantiateViewController(withIdentifier: "contacts") as! ContactsController
        vc.contacts = person?.contacts
        currentVC = vc
      case .competence:
        let vc = storyboard?.instantiateViewController(withIdentifier: "competency") as! CompetencyController
        vc.competencies = position?.competencies ?? person?.competencies ?? []
        vc.hideRequired = position == nil
        currentVC = vc
      case .kinships:
        let vc = storyboard?.instantiateViewController(withIdentifier: "kinships") as! KinshipsController
        vc.items = kinshipItems
        currentVC = vc
      case .commonInfo:
        let vc = storyboard?.instantiateViewController(withIdentifier: "info") as! InfoController
        vc.position = position
        vc.person = person
        vc.subPositions = subPositions
        currentVC = vc
      case .reserve:
        let vc = storyboard!.instantiateViewController(withIdentifier: "reserve") as! ReserveController
        vc.position = position
        vc.candidates = candidates
        currentVC = vc
      }
    }
  }

  private var currentVC: UIViewController? {
    didSet {
      if let old = oldValue {
        old.willMove(toParentViewController: nil)
        old.view.removeFromSuperview()
        old.removeFromParentViewController()
        old.didMove(toParentViewController: nil)
      }
      if let new = currentVC {
        new.view.frame = contentView.bounds
        new.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        new.willMove(toParentViewController: self)
        contentView.addSubview(new.view)
        addChildViewController(new)
        new.didMove(toParentViewController: self)
      }
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    mainView.isHidden = true
    tabControl.removeAllSegments()
    tabs.forEach {
      tabControl.insertSegment(withTitle: $0.title, at: tabControl.numberOfSegments, animated: false)
    }
    tabControl.selectedSegmentIndex = 0
    load()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    refresh()
  }

  private func load() {
    let cli = Client.instance
    if positionId > 0 {
      var tmpPos: Position?
      cli.getPosition(id: positionId).flatMap({ pos -> Observable<([Kinship], [Position], [VacancyCandidate])> in
        tmpPos = pos
        return Observable.zip(
          cli.getKinships(personId: pos.person!.id),
          cli.getSubordinatePositions(id: self.positionId),
          cli.getCandidates(for: pos, fake: true),
          resultSelector: { ($0, $1, $2) })
      }).subscribe(onNext: { kinships, subPositions, candidates in
        DispatchQueue.global(qos: .background).async {
          self.kinshipItems = kinships.map { KinshipItem($0) }
          self.subPositions = subPositions
          self.candidates = candidates
          DispatchQueue.main.async {
            if tmpPos!.level <= 4 && tmpPos!.person!.id != 95, let index = self.tabs.index(of: .competence) {
              self.tabs.remove(at: index)
              self.tabControl.removeSegment(at: index, animated: false)
            }
            if kinships.isEmpty, let index = self.tabs.index(of: .kinships) {
              self.tabs.remove(at: index)
              self.tabControl.removeSegment(at: index, animated: false)
            }
            if tmpPos!.typeId == 1, let index = self.tabs.index(of: .reserve) {
              self.tabs.remove(at: index)
              self.tabControl.removeSegment(at: index, animated: false)
            }
            self.position = tmpPos
            self.person = tmpPos?.person
            self.loadingCompleted()
          }
        }
      }, onError: { error in
        debugPrint(error)
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
          self?.load()
        }
      }).addDisposableTo(disposeBag)
    } else {
      cli.getPerson(id: personId).subscribe(onNext: { [weak self] person in
        guard let `self` = self else { return }
        self.person = person
        for tab: Tab in [.kinships, .reserve] {
          if let index = self.tabs.index(of: tab) {
            self.tabs.remove(at: index)
            self.tabControl.removeSegment(at: index, animated: false)
          }
        }
        self.loadingCompleted()
      }, onError: { [weak self] error in
        debugPrint(error)
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
          self?.load()
        }
      }).addDisposableTo(disposeBag)
    }

  }

  private func loadingCompleted() {

    if let position = self.position {
      if let person = self.person {
        actionsView.isHidden = position.id == 1
        attestationButton.isHidden = position.id == 1 || position.typeId == 1
        let fullName = person.fullName
        navigationItem.title = fullName
        infoLabel.attributedText = {
          let text = NSMutableAttributedString()
          if let divisionName = position.division?.name, !divisionName.isEmpty {
            text += (divisionName + "\n").attributed(with: .sysFont(17, .regular), .ps(20), .align(.center))
          }
          if let positionName = position.name, !positionName.isEmpty {
            text += (positionName + "\n").attributed(with: .sysFont(22, .bold), .ps(12), .align(.center))
          }
          if let fullName = position.person?.fullName, !fullName.isEmpty {
            text += fullName.attributed(with: .sysFont(20, .regular), .align(.center))
          }
          return text
        }()
        let placeholder: UIImage = person.sex == .male ? #imageLiteral(resourceName: "placeholderAvatarMale") : #imageLiteral(resourceName: "placeholderAvatarFeemale")
        if let url = person.photoURL {
          avatarImageView.kf.setImage(with: url, placeholder: placeholder, options: [.transition(.fade(0.3)), .processor(AvatarProcessor(width: 232))])
        } else {
          avatarImageView.image = placeholder
        }
      }
      refresh()
    } else if let person = self.person {
      let fullName = person.fullName
      navigationItem.title = fullName
      infoLabel.attributedText = fullName.attributed(with: .sysFont(20, .regular), .align(.center))
      actionsView.isHidden = true
      let placeholder: UIImage = person.sex == .male ? #imageLiteral(resourceName: "placeholderAvatarMale") : #imageLiteral(resourceName: "placeholderAvatarFeemale")
      if let url = person.photoURL {
        avatarImageView.kf.setImage(with: url, placeholder: placeholder, options: [.transition(.fade(0.3)), .processor(AvatarProcessor(width: 232))])
      } else {
        avatarImageView.image = placeholder
      }
    }

    currentTab = tabs.first!
    activityIndicatorView.stopAnimating()
    mainView.isHidden = false
  }

  private func createOrder(_ type: OrderType) {
    KVNProgress.show()
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [unowned self] in
      DashBoard.shared.createOrder(type, position: self.position!).subscribe(onNext: { _ in
        KVNProgress.showSuccess()
      }, onError: { error in
        KVNProgress.showError(withStatus: error.localizedDescription)
      }).addDisposableTo(self.disposeBag)
    }
  }

  @IBAction func changeTab(_ sender: UISegmentedControl) {
    currentTab = tabs[sender.selectedSegmentIndex]
  }

  @IBAction func attestation() {
    let vc = SFSafariViewController(url: URL(string: "https://gossluzhba.gov.ru/Testing/App#/testing/run/action/f42fa1cf-5edb-49b0-ab8f-1b61e292966a")!)
    vc.modalPresentationStyle = .formSheet
    vc.modalTransitionStyle = .crossDissolve
    var size = UIScreen.main.bounds.size
    size.width *= 0.8
    size.height *= 0.8
    vc.preferredContentSize = size
    present(vc, animated: true)
  }

  @IBAction func fakeAction1() {
    createOrder(.translate)
    FakeAction.set(.translate, for: position!.id)
    EventLog.instance.add("Экран '\(position?.person?.fullName ?? "")' > Кнопка 'Сменить позицию'")
    refresh()
  }

  @IBAction func fakeAction2() {
    createOrder(.training)
    FakeAction.set(.training, for: position!.id)
    EventLog.instance.add("Экран '\(position?.person?.fullName ?? "")' > Кнопка 'Направить на обучение'")
    refresh()
  }

  @IBAction func fakeAction3() {
    createOrder(.dismiss)
    FakeAction.set(.dismiss, for: position!.id)
    EventLog.instance.add("Экран '\(position?.person?.fullName ?? "")' > Кнопка 'Уволить'")
    refresh()
  }

  @IBAction func cancelFakeAction() {
    rootController?.show(tab: .cabinet)
//    FakeAction.set(.none, for: position!.id)
//    EventLog.instance.add("Экран '\(position?.person?.fullName ?? "")' > Кнопка 'Отменить действие'")
//    refresh()
  }

  func show(tab: Tab) {
    if let index = tabs.index(of: tab) {
      tabControl.selectedSegmentIndex = index
      changeTab(tabControl)
    }
  }

  private func refresh() {
    guard let id = position?.id else { return }
    if let action = FakeAction.get(for: id) {
      fakeActionsView.isHidden = true
      fakeActionsCancelView.isHidden = false
      let df = DateFormatter()
      df.dateFormat = "dd.MM.yyyy"
      var text = ""
      switch action.action {
      case .dismiss:
        text = "Вы предложили уволить "
      case .training:
        text = "Вы предложили отправить на обучение "
      case .translate:
        text = "Вы предложили перевести на другую должность "
      default:
        break
      }
      text += df.string(from: action.date ?? Date())
      fakeActionLabel.text = text
    } else {
      fakeActionsView.isHidden = false
      fakeActionsCancelView.isHidden = true
    }
  }

}

enum EmployeeAction : Int {
  case none
  case dismiss
  case translate
  case training
}

final class FakeAction : Mappable {

  private(set) var action: EmployeeAction = .none
  private(set) var date: Date!

  init(_ action: EmployeeAction) {
    self.action = action
    date = Date()
  }

  init?(map: Map) {}

  func mapping(map: Map) {
    action <- map["a"]
    date <- (map["d"], DateTransform())
  }

  static func get(for id: Int) -> FakeAction? {
    if let str = UserDefaults.standard.string(forKey: "__employee\(id)"), let act = Mapper<FakeAction>().map(JSONString: str) {
      return act
    }
    return nil
  }

  static func set(_ act: EmployeeAction, for id: Int) {
    let key = "__employee\(id)"
    guard act != .none else {
      UserDefaults.standard.removeObject(forKey: key)
      return
    }
    let str = Mapper<FakeAction>().toJSONString(FakeAction(act))
    UserDefaults.standard.set(str, forKey: key)
  }

}
