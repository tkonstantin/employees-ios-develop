//
//  InfoController.swift
//  Employees
//
//  Created by Sergey Erokhin on 31.03.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import SnapKit
import Charts

final class InfoController : UIViewController {

  enum Section {
    case info
    case subPositions
  }

  @IBOutlet private var tableView: UITableView!
  @IBOutlet private var competenciesHeaderLabel: UILabel!
  @IBOutlet private var competenciesLabel: UILabel!
  @IBOutlet private var competenciesChartView: RadarChartView!

  @IBOutlet private var careerView: EmployeeCareerView!
  @IBOutlet fileprivate var rightViewsTopConstraint: NSLayoutConstraint!

  var position: Position?
  var person: Person?
  var subPositions: [Position] = []

  fileprivate var sections: [Section] = [.info]

  fileprivate var info: Info!

  override func viewDidLoad() {
    super.viewDidLoad()
    if !subPositions.isEmpty && position!.id > 1 {
      sections.append(.subPositions)
    }
    info = Info(subj: position ?? person!)
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 200
    tableView.register(UINib(nibName: "BiographyCell", bundle: nil), forCellReuseIdentifier: "infoItem")
    tableView.register(UINib(nibName: "SubPositionCell", bundle: nil), forCellReuseIdentifier: "subPos")
    tableView.register(SubPositionsHeaderView.self, forHeaderFooterViewReuseIdentifier: "header")
    tableView.tableFooterView = UIView()
    setupCompetenciesView()
    setupCareerView()
  }

  private func setupCompetenciesView() {
    guard position == nil || position!.level > 4 || position!.person!.id == 95 else {
      competenciesChartView.superview?.superview?.isHidden = true
      return
    }

    if let competencies = position?.competencies ?? person?.competencies, !competencies.isEmpty {
      var factor: Float = 0
      let step: Float = 1 / Float(competencies.count)

      for item in competencies {
        factor += min(1, Float(item.value) / Float(item.requiredValue)) * step

      }

      var sets: [RadarChartDataSet] = []

      if position != nil {
        competenciesLabel.text = "\(Int(factor * 100))%"

        let set1 = RadarChartDataSet(values: competencies.map({ RadarChartDataEntry(value: Double($0.requiredValue)) }), label: "Требуемые компетенции")
        set1.fillAlpha = 0.7
        set1.lineWidth = 2
        set1.drawFilledEnabled = true
        set1.setColor(UIColor(rgba: 0x51ae83ff))
        set1.fillColor = set1.color(atIndex: 0)
        sets.append(set1)
      } else {
        competenciesLabel.isHidden = true
        competenciesHeaderLabel.text = "Компетенции"
      }

      let set2 = RadarChartDataSet(values: competencies.map({ RadarChartDataEntry(value: Double($0.value)) }), label: "Компетенции сотрудника")
      set2.fillAlpha = 0.7
      set2.lineWidth = 2
      set2.drawFilledEnabled = true
      set2.setColor(UIColor(rgba: 0x2073d6ff))
      set2.fillColor = set2.color(atIndex: 0)
      sets.append(set2)

      let data = RadarChartData(dataSets: sets)
      data.labels = competencies.map({ $0.name })
      data.setDrawValues(false)
      data.highlightEnabled = false

      competenciesChartView.data = data

      for axis in [competenciesChartView.xAxis, competenciesChartView.yAxis] {
        axis.axisLineWidth = 1
        axis.gridLineWidth = 1
        axis.gridColor = .gubGrid
        axis.drawLabelsEnabled = false
        axis.axisMinimum = 0
        axis.labelCount = 5
      }
      competenciesChartView.legend.enabled = false
      competenciesChartView.webLineWidth = 1
      competenciesChartView.webColor = .gubGrid

    } else {
      competenciesLabel.text = "0%"
    }
    competenciesChartView.noDataText = "Нет данных"
    competenciesChartView.chartDescription = nil
    competenciesChartView.rotationEnabled = false
    competenciesChartView.animate(xAxisDuration: 1)
  }

  private func setupCareerView() {
    guard person?.birthDate != nil || person?.startWorkingDate != nil && person?.currentPositionDate != nil else {
      careerView.isHidden = true
      return
    }
    careerView.person = person
  }

  @IBAction func showCompetencies() {
    (parent as? EmployeeController)?.show(tab: .competence)
  }

  @IBAction func showBiography() {
    (parent as? EmployeeController)?.show(tab: .bio)
  }

}

// MARK: - UITableViewDataSource

extension InfoController : UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    return sections.count
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch sections[section] {
    case .info:
      return info.items.count
    case .subPositions:
      return subPositions.count
    }
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch sections[indexPath.section] {
    case .info:
      let cell = tableView.dequeueReusableCell(withIdentifier: "infoItem", for: indexPath) as! BiographyCell
      cell.infoItem = info.items[indexPath.row]
      return cell
    case .subPositions:
      let cell = tableView.dequeueReusableCell(withIdentifier: "subPos", for: indexPath) as! SubPositionCell
      cell.position = subPositions[indexPath.row]
      return cell
    }
  }

}

// MARK: - UITableViewDelegate

extension InfoController : UITableViewDelegate {

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    if sections[indexPath.section] == .subPositions {
      let position = subPositions[indexPath.row]
      if position.person != nil {
        let vc = storyboard?.instantiateViewController(withIdentifier: "employee") as! EmployeeController
        vc.positionId = position.id
        navigationController?.pushViewController(vc, animated: true)
      } else {
        let vc = storyboard?.instantiateViewController(withIdentifier: "vacancy") as! VacancyController
        vc.positionId = position.id
        navigationController?.pushViewController(vc, animated: true)
      }
    }
  }

  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if sections[section] == .subPositions {
      return 96
    }
    return 0
  }

  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if sections[section] == .subPositions {
      let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as! SubPositionsHeaderView
      view.vacancyView.free = position!.numVacancies
      view.vacancyView.total = position!.numWorkplaces
      return view
    }
    return nil
  }

  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    rightViewsTopConstraint.constant = -scrollView.contentOffset.y
  }

}

enum InfoItemType {
  case division
  case position
  case rank
  case experience
  case rewards
}

final class InfoItem {

  let type: InfoItemType
  let text: NSAttributedString

  init(type: InfoItemType, text: NSAttributedString) {
    self.type = type
    self.text = text
  }

}

protocol InfoSubj : class {}

extension Person : InfoSubj {}
extension Position : InfoSubj {}

final class Info {

  struct UX {
    static let bulletPrefix = "•   "
    static let prefixWidth = (bulletPrefix as NSString).size(attributes: String.attributes(.sysFont(16, .regular))).width
    static let textAttrs = String.attributes(.sysFont(16, .regular), .ps(10))
    static let listTextAttrs = String.attributes(.sysFont(16, .regular), .ps(10), .headIndent(prefixWidth), .firstLineHeadIndent(0))
  }

  private(set) var items: [InfoItem] = []

  init(subj: InfoSubj) {
    let now = Date()
    var person = subj as? Person
    if let position = subj as? Position {
      person = position.person
      if let division = position.headDivision {
        let text = NSAttributedString(string: division.name, attributes: UX.textAttrs)
        items.append(InfoItem(type: .division, text: text))
      }
      {
        var text = position.name!
        if let date = position.person?.currentPositionDate {
          text += "\nВ должности"
          let components = Calendar.current.dateComponents([.year, .month, .day], from: date, to: now)
          let df = DateFormatter()
          df.locale = Locale(identifier: "ru")
          df.dateFormat = "d MMMM y"
          if components.year! > 3 {
            text += " "
            text += String.localizedStringWithFormat(NSLocalizedString("%d год", comment: ""), components.year!)
          } else {
            if components.year! > 0 {
              text += " "
              text += String.localizedStringWithFormat(NSLocalizedString("%d год", comment: ""), components.year!)
            }
            if components.month! > 0 {
              text += " "
              text += String.localizedStringWithFormat(NSLocalizedString("%d месяц", comment: ""), components.month!)
            }
          }
          text += " (с \(df.string(from: date)) года)"
        }
        items.append(InfoItem(type: .position, text: NSAttributedString(string: text, attributes: UX.textAttrs)))
      }()
    }

    if let rank = person?.ranks.last {
      items.append(InfoItem(type: .rank, text: NSAttributedString(string: rank.name, attributes: UX.textAttrs)))
    } else {
      items.append(InfoItem(type: .rank, text: NSAttributedString(string: "Нет информации", attributes: UX.textAttrs)))
    }
    if let date = person?.startWorkingDate {
      if let year = Calendar.current.dateComponents([.year], from: date, to: now).year, year > 0 {
        let text = String.localizedStringWithFormat(NSLocalizedString("%d год", comment: ""), year)
        items.append(InfoItem(type: .experience, text: NSAttributedString(string: text, attributes: UX.textAttrs)))
      } else {
        items.append(InfoItem(type: .experience, text: NSAttributedString(string: "Нет информации", attributes: UX.textAttrs)))
      }
    } else {
      items.append(InfoItem(type: .experience, text: NSAttributedString(string: "Нет информации", attributes: UX.textAttrs)))
    }
    if let rewards = person?.achievements, !rewards.isEmpty {
      let text = rewards.map({ UX.bulletPrefix + $0 }).joined(separator: "\n")
      items.append(InfoItem(type: .rewards, text: NSAttributedString(string: text, attributes: UX.listTextAttrs)))
    }
  }

}


@IBDesignable
class InfoSideView : UIView {

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  private func setup() {
    layer.borderWidth = 1 / UIScreen.main.scale
    layer.borderColor = UIColor.gubSeparator.cgColor
  }

}

final class SubPositionsHeaderView : UITableViewHeaderFooterView {

  var vacancyView: VacancyView!

  override init(reuseIdentifier: String?) {
    super.init(reuseIdentifier: reuseIdentifier)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setup() {
    let label = UILabel()
    label.text = "В прямом подчинении"
    label.font = UIFont.systemFont(ofSize: 24, weight: UIFontWeightLight)
    vacancyView = VacancyView()
    let stackView = UIStackView(arrangedSubviews: [label, vacancyView])
    stackView.spacing = 20
    stackView.alignment = .center
    stackView.axis = .horizontal
    contentView.addSubview(stackView)
    stackView.snp.makeConstraints {
      $0.bottom.equalToSuperview().offset(-20)
      $0.left.equalToSuperview().offset(16)
    }
    contentView.backgroundColor = .white
    let separatorView1 = UIView()
    separatorView1.backgroundColor = .gubSeparator
    contentView.addSubview(separatorView1)
    separatorView1.snp.makeConstraints {
      $0.top.equalToSuperview()
      $0.left.equalToSuperview().offset(16)
      $0.right.equalToSuperview()
      $0.height.equalTo(1 / UIScreen.main.scale)
    }
    let separatorView2 = UIView()
    separatorView2.backgroundColor = .gubSeparator
    contentView.addSubview(separatorView2)
    separatorView2.snp.makeConstraints {
      $0.bottom.equalToSuperview()
      $0.left.equalToSuperview().offset(92)
      $0.right.equalToSuperview()
      $0.height.equalTo(1 / UIScreen.main.scale)
    }
  }

}

final class InfoControllerView : UIView {

  @IBOutlet var tableView: UITableView!
  @IBOutlet var activeViews: [UIView] = []

  override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
    if super.hitTest(point, with: event) != nil {
      for v in activeViews {
        let rect = convert(v.frame, from: v.superview)
        if rect.contains(point) {
          return v
        }
      }
      return tableView
    }
    return nil
  }


}
