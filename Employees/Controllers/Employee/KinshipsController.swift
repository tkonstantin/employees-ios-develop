//
//  KinshipsController.swift
//  Employees
//
//  Created by Sergey Erokhin on 27/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

final class KinshipItem {

  let kinship: Kinship

  var headerText: NSAttributedString
  var nameText: NSAttributedString!

  init(_ kinship: Kinship) {
    self.kinship = kinship
    headerText = {
      let tmp = NSMutableAttributedString()
      if let position = kinship.position {
        if let division = position.division {
          tmp += (division.name, KinshipItemCell.UX.divisionTextAttrs)
          tmp += ("\n" + position.name, KinshipItemCell.UX.positionTextAttrs)
        } else {
          tmp += (position.name, KinshipItemCell.UX.positionTextAttrs)
        }
      }
      return tmp
    }()
    nameText = {
      let tmp = NSMutableAttributedString()
      tmp += (kinship.type.title(for: kinship.person!) + "\n", KinshipItemCell.UX.typeTextAttrs)
      tmp += (kinship.person!.fullName, KinshipItemCell.UX.nameTextAttrs)
      return tmp
    }()
  }

}

final class KinshipsController : UIViewController {

  @IBOutlet private var collectionView: UICollectionView!

  var items: [KinshipItem] = [] {
    didSet {
      collectionView?.reloadData()
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    collectionView.register(UINib(nibName: "KinshipItemCell", bundle: nil), forCellWithReuseIdentifier: "item")
  }

}

extension KinshipsController : UICollectionViewDataSource {

  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return items.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "item", for: indexPath) as! KinshipItemCell
    cell.item = items[indexPath.row]
    return cell
  }

}

extension KinshipsController : UICollectionViewDelegate, KinshipLayoutDelegate  {

  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    collectionView.deselectItem(at: indexPath, animated: true)
    let vc = storyboard!.instantiateViewController(withIdentifier: "employee") as! EmployeeController
    if let positionId = items[indexPath.row].kinship.position?.id {
      vc.positionId = positionId
    } else {
      vc.personId = items[indexPath.row].kinship.person!.id
    }
    navigationController?.pushViewController(vc, animated: true)

  }

  func heightForItem(at indexPath: IndexPath, width: CGFloat) -> CGFloat {
    return KinshipItemCell.calculateHeight(for: items[indexPath.row], width: width)
  }
}

protocol KinshipLayoutDelegate : class {

  func heightForItem(at indexPath: IndexPath, width: CGFloat) -> CGFloat

}

final class KinshipsLayout : UICollectionViewLayout {

  var numColumns: Int = 2 {
    didSet {
      guard numColumns != oldValue else { return }
      invalidateLayout()
    }
  }

  private var attrsByIndexPath: [IndexPath : UICollectionViewLayoutAttributes] = [:]
  private var contentSize: CGSize = .zero

  override func prepare() {

    attrsByIndexPath.removeAll()
    contentSize = .zero

    guard let delegate = collectionView?.delegate as? KinshipLayoutDelegate else { return }

    var columnHeight = [CGFloat](repeating: 0, count: numColumns)
    let columnWidth = collectionView!.frame.width / CGFloat(numColumns)
    let count = collectionView!.numberOfItems(inSection: 0)

    for item in 0 ..< count {
      let indexPath = IndexPath(item: item, section: 0)
      let attrs = UICollectionViewLayoutAttributes(forCellWith: indexPath)
      var currentColumnIndex = 0
      var currentColumnHeight = CGFloat.greatestFiniteMagnitude
      for (i, height) in columnHeight.enumerated() {
        if height < currentColumnHeight {
          currentColumnIndex = i
          currentColumnHeight = height
        }
      }
      let x: CGFloat = CGFloat(currentColumnIndex) * columnWidth
      let y: CGFloat = columnHeight[currentColumnIndex]
      let height: CGFloat = delegate.heightForItem(at: indexPath, width: columnWidth)

      attrs.frame = CGRect(x: x, y: y, width: columnWidth, height: height)
      columnHeight[currentColumnIndex] += height

      attrsByIndexPath[indexPath] = attrs
    }
    contentSize.width = collectionView!.frame.width
    contentSize.height = columnHeight.reduce(0, { max($0, $1) })
  }

  override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
    return attrsByIndexPath[indexPath]
  }

  override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
    var result: [UICollectionViewLayoutAttributes] = []
    for (_, attrs) in attrsByIndexPath where rect.intersects(attrs.frame) {
      result.append(attrs)
    }
    return result
  }

  override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
    return !collectionView!.bounds.size.equalTo(newBounds.size)
  }

  override var collectionViewContentSize : CGSize {
    return contentSize
  }

}
