//
//  ReserveController.swift
//  Employees
//
//  Created by Sergey Erokhin on 03/05/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

final class ReserveController : UIViewController {

  enum Section {
    case header(NSAttributedString)
    case suitableCandidates
    case recommendedCandidates
  }

  fileprivate var sections: [Section] = []

  var position: Position?
  var candidates: [VacancyCandidate] = [] {
    didSet {
      suitableCandidates = candidates.filter({ !$0.flags.contains(.bossApproval) })
      recommendedCandidates = candidates.filter({ $0.flags.contains(.bossApproval) })
    }
  }

  fileprivate var suitableCandidates: [VacancyCandidate] = []
  fileprivate var recommendedCandidates: [VacancyCandidate] = []

  @IBOutlet private var tableView: UITableView!

  override func viewDidLoad() {
    super.viewDidLoad()
    sections.append(.header({
      let text = NSMutableAttributedString()
      text += "Подходящие кандидаты на должность ".attributed(with: .sysFont(16, .regular))
      text += position!.name.attributed(with: .sysFont(16, .bold))
      return text
    }()))
    sections.append(.suitableCandidates)
//    sections.append(.header("Предложены начальником".attributed(with: .sysFont(16, .regular))))
//    sections.append(.recommendedCandidates)
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 100
  }
}

extension ReserveController : UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    return sections.count
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch sections[section] {
    case .header(_):
      return 1
    case .recommendedCandidates:
      return recommendedCandidates.count
    case .suitableCandidates:
      return suitableCandidates.count
    }
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch sections[indexPath.section] {
    case .header(let text):
      let cell = tableView.dequeueReusableCell(withIdentifier: "header", for: indexPath) as! ReserveHeaderCell
      cell.headerLabel.attributedText = text
      return cell
    case .suitableCandidates:
      let cell = tableView.dequeueReusableCell(withIdentifier: "candidate", for: indexPath) as! VacancyCandidateCell
      cell.candidate = suitableCandidates[indexPath.row]
      return cell
    case .recommendedCandidates:
      let cell = tableView.dequeueReusableCell(withIdentifier: "candidate", for: indexPath) as! VacancyCandidateCell
      cell.candidate = recommendedCandidates[indexPath.row]
      return cell
    }
  }

}

extension ReserveController : UITableViewDelegate {

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    if let cell = tableView.cellForRow(at: indexPath) as? VacancyCandidateCell {
      let vc = storyboard!.instantiateViewController(withIdentifier: "employee") as! EmployeeController
      vc.positionId = cell.candidate.candidate!.id
      navigationController?.pushViewController(vc, animated: true)
    }
  }

}

final class ReserveHeaderCell : UITableViewCell {

  @IBOutlet private(set) var headerLabel: UILabel!

}
