//
//  LoadingController.swift
//  Employees
//
//  Created by Sergey Erokhin on 02/06/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import Alamofire
import RxSwift

final class LoadingController : UIViewController {

  @IBOutlet private var progressView: UIProgressView!
  @IBOutlet private var stackView: UIStackView!

  private var disposeBag = DisposeBag()

  private var pendingUpdate = false

  override func viewDidLoad() {
    super.viewDidLoad()
    stackView.isHidden = true
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    startUpdate()
  }

  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }

  func startUpdate() {
    guard !pendingUpdate else { return }
    pendingUpdate = true
    checkUpdate().subscribe(onNext: { [weak self] date in
      self?.checkVersion(with: date)
    }, onError: { [weak self] error in
      print(error)
      self?.performSegue(withIdentifier: "finish", sender: nil)
    }).addDisposableTo(disposeBag)
  }

  func checkVersion(with version: Date) {
    let curVersion = UserDefaults.standard.object(forKey: "dataVersion") as? Date ?? Date(timeIntervalSince1970: 0)
    if curVersion < version {
      stackView.isHidden = false
      downloadUpdate().subscribe(onNext: { [weak self] progress in
        self?.progressView.progress = progress
      }, onError: { [weak self] error in
        self?.alert(title: "Не удалось загрузить обновление", message: error.localizedDescription) {
          self?.performSegue(withIdentifier: "finish", sender: nil)
        }
      }, onCompleted: { [weak self] in
        UserDefaults.standard.set(version, forKey: "dataVersion")
        self?.updateCompleted()
      }).addDisposableTo(disposeBag)
    } else {
      updateCompleted()
    }
  }

  private func updateCompleted() {
    do {
      try LocalServer.instance.restart()
    } catch let error {
      alert(message: error.localizedDescription)
      LocalServer.instance.restoreLocalVersion()
    }
    performSegue(withIdentifier: "finish", sender: nil)
  }

  func checkUpdate() -> Observable<Date> {
    let req = Alamofire.request("\(EmployeesServerURLPrefix)/api/databases/latest", method: .head)
    return Observable.create { observer in
      req.validate(statusCode: [200]).responseData { response in
        switch response.result {
        case .success(_):
          var date = Date(timeIntervalSince1970: 0)
          if let value = response.response?.allHeaderFields["Latest-Update"] as? String {
            let df = DateFormatter()
            df.locale = Locale(identifier: "en_US_POSIX")
            df.dateFormat = "eee, dd MMM yyyy HH:mm:ss zzz"
            if let tmp = df.date(from: value) {
              date = tmp
            }
          }
          observer.onNext(date)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        req.cancel()
      }
    }
  }

  func downloadUpdate() -> Observable<Float> {
    let req = Alamofire.download("\(EmployeesServerURLPrefix)/api/databases/latest", to: { tmpURL, response in
      return (LocalServer.instance.dbURL, [.removePreviousFile])
    })
    return Observable.create { observer in
      req.downloadProgress(closure: { progress in
        print("progress: \(progress.fractionCompleted)")
        observer.onNext(Float(progress.fractionCompleted))
      }).response { response in
        if let error = response.error {
          observer.onError(error)
        } else {
          observer.onNext(1)
          observer.onCompleted()
        }
      }
      return Disposables.create {
        req.cancel()
      }
    }
  }
}

extension UIViewController {

  func alert(title: String? = nil, message: String? = nil, completion: (() -> Void)? = nil) {
    let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alertVC.addAction(UIAlertAction(title: "ОК", style: .default) { _ in
      completion?()
    })
    present(alertVC, animated: true)
  }

}
