//
//  LoginController.swift
//  Employees
//
//  Created by Sergey Erokhin on 21/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

final class LoginController : UIViewController {
    
    @IBOutlet private var logoView: UIImageView!
    @IBOutlet fileprivate var loginTextField: UITextField!
    @IBOutlet fileprivate var passwordTextField: UITextField!
    @IBOutlet private var bottomConstraint: NSLayoutConstraint!
    @IBOutlet private var contentView: UIView!
    
    private var fakeLogoView: UIImageView!
    
    private var showAnimation: Bool = true
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowOrHide(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowOrHide(_:)), name: .UIKeyboardWillHide, object: nil)
        contentView.alpha = 0
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        if showAnimation && fakeLogoView == nil {
            fakeLogoView = UIImageView()
            fakeLogoView.autoresizingMask = []
            fakeLogoView.image = logoView.image
            view.addSubview(fakeLogoView)
            fakeLogoView.sizeToFit()
            fakeLogoView.center = CGPoint(x: view.bounds.width / 2, y: view.bounds.height / 2)
            fixTextFieldLayout()
        }
        super.viewDidLayoutSubviews()
    }
    
    private func fixTextFieldLayout() {
        loginTextField.text = "_"
        passwordTextField.text = "_"
        loginTextField.layoutIfNeeded()
        passwordTextField.layoutIfNeeded()
        loginTextField.text = "adm"
        passwordTextField.text = "adm"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if showAnimation {
            showAnimation = false
            UIView.animate(withDuration: 0.3, delay: 0.3, options: [], animations: {
                self.fakeLogoView.frame = self.logoView.frame
            }) { _ in
                UIView.animate(withDuration: 0.3, animations: {
                    self.contentView.alpha = 1
                }) { _ in
                    self.fakeLogoView.removeFromSuperview()
                    self.fakeLogoView = nil
                }
            }
        }
    }
    
    func keyboardWillShowOrHide(_ notification: Notification) {
        guard view.window != nil else {
            bottomConstraint.constant = 0
            return
        }
        let userInfo = notification.userInfo!
        let dt = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
        var kbFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        kbFrame = view!.convert(kbFrame, from: nil)
        let bottom = max(view!.frame.height - kbFrame.origin.y, 0)
        UIView.animate(withDuration: dt, delay: 0, options: .beginFromCurrentState, animations: {
            self.bottomConstraint?.constant = bottom
            self.view!.layoutIfNeeded()
        }, completion: nil)
    }
    
    var validCredentials: [String: String] = [
        "adm":"adm",
        "A.Krasnov":"Qwerty123"
    ]
    
    @IBAction func login() {
        view.endEditing(true)
        let login = loginTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        guard let currentPassword = validCredentials[login] else {
            showError(title: "Неправильный логин", message: "Проверьте свой логин и попробуйте еще раз.") {
                self.passwordTextField.text = nil
                self.loginTextField.text = nil
                self.loginTextField.becomeFirstResponder()
            }
            return
        }
        guard password == currentPassword else {
            showError(title: "Неверный пароль", message: "Введен неверный пароль. Пожалуйста, попробуйте еще раз.") {
                self.passwordTextField.becomeFirstResponder()
            }
            return
        }
        EventLog.instance.add("Успешная авторизация")
        performSegue(withIdentifier: "main", sender: nil)
    }
    
    private func showError(title: String? = nil, message: String? = nil, completion: (() -> Void)? = nil) {
        EventLog.instance.add("Ошибка авторизации")
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "ОК", style: .default) { _ in
            completion?()
        })
        present(alertVC, animated: true)
    }
}

extension LoginController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == loginTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            login()
        }
        return false
    }
    
}
