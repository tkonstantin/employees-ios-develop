//
//  ModalNavigationController.swift
//  Employees
//
//  Created by Sergey Erokhin on 02/05/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

final class ModalNavigationController : UINavigationController {

  override func viewDidLoad() {
    super.viewDidLoad()
    navigationBar.isTranslucent = false
    navigationBar.barTintColor = UIColor(rgba: 0xf9f9f9ff)
    navigationBar.tintColor = UIColor(rgba: 0x2589deff)
    navigationBar.titleTextAttributes = String.attributes(.sysFont(17, .regular), .fg(.black))
  }
}
