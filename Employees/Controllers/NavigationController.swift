//
//  NavigationController.swift
//  Employees
//
//  Created by Sergey Erokhin on 20/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

final class NavigationController : UINavigationController {

  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    navigationBar.isTranslucent = false
    navigationBar.barTintColor = .gubNavbar
    navigationBar.tintColor = .white
    navigationBar.titleTextAttributes = String.attributes(.sysFont(17, .regular), .fg(.white))
  }

}
