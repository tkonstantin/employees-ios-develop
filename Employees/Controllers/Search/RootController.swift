//
//  RootController.swift
//  Employees
//
//  Created by Sergey Erokhin on 11/04/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import SafariServices

final class RootController : UIViewController {

  enum Tab {
    case cabinet
    case structure
    case statistic
    case vacancies
    case portal
    case training
  }

  @IBOutlet private var collectionView: UICollectionView!
  @IBOutlet private var containerView: UIView!

  fileprivate var tabs: [Tab] = [.structure, .vacancies, .statistic, .portal, .training, .cabinet]
  private var controllers: [Tab : UIViewController] = [:]

  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }

  fileprivate var selectedTab: Tab = .statistic {
    didSet {
      var vc = controllers[selectedTab]
      guard selectedTab != oldValue else {
        if let nc = vc as? UINavigationController {
          nc.popToRootViewController(animated: true)
        }
        return
      }
      if vc == nil {
        switch selectedTab {
        case .cabinet:
          vc = storyboard?.instantiateViewController(withIdentifier: "cabinet")
        case .structure:
          vc = storyboard!.instantiateViewController(withIdentifier: "structure")
        case .statistic:
          vc = storyboard!.instantiateViewController(withIdentifier: "stat")
        case .vacancies:
          vc = storyboard!.instantiateViewController(withIdentifier: "vacancies")
        case .portal, .training:
          let url = URL(string: selectedTab == .training ? "http://lms.gossluzhba.mosreg.ru/" : "http://gossluzhba.mosreg.ru/")!
          let vc = SFSafariViewController(url: url)
          vc.modalPresentationStyle = .formSheet
          vc.modalTransitionStyle = .crossDissolve
          var size = UIScreen.main.bounds.size
          size.width *= 0.8
          size.height *= 0.8
          vc.preferredContentSize = size
          present(vc, animated: true)
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.selectedTab = oldValue
            self.collectionView.reloadData()
          }
          return
        }
        let nc = NavigationController(rootViewController: vc!)
        controllers[selectedTab] = nc
        vc = nc
      }
      currentVC = vc
    }
  }

  private var currentVC: UIViewController! {
    didSet {
      if let old = oldValue {
        old.willMove(toParentViewController: nil)
        old.view.removeFromSuperview()
        old.removeFromParentViewController()
        old.didMove(toParentViewController: nil)
      }
      if let new = currentVC {
        new.willMove(toParentViewController: self)
        new.view.frame = containerView.bounds
        new.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addChildViewController(new)
        containerView.addSubview(new.view)
        new.didMove(toParentViewController: self)
      }
    }
  }

  func show(tab: Tab) {
    if let index = tabs.index(of: tab) {
      selectedTab = tab
      collectionView.selectItem(at: IndexPath(row: index, section: 0), animated: false, scrollPosition: .top)
      collectionView.reloadData()
    }
  }

  @IBAction func handleLogoTap(_ sender: UITapGestureRecognizer) {
    if selectedTab != .structure {
      if let nc = controllers[.structure] as? UINavigationController {
        nc.popToRootViewController(animated: false)
      }
      show(tab: .structure)
    } else {
      let nc = currentVC as! UINavigationController
      if nc.viewControllers.count > 1 {
        nc.popToRootViewController(animated: true)
      } else {
        let vc = nc.viewControllers.first! as! StructureController_v2
        vc.collapseAll()
      }
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    collectionView.contentInset.top = 20
    selectedTab = tabs.first!
    collectionView.selectItem(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .top)
  }

}

// MARK: - UICollectionViewDataSource

extension RootController : UICollectionViewDataSource {

  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return tabs.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tab", for: indexPath) as! RootTabCell
    let tab = tabs[indexPath.row]
    cell.tab = tab
    cell.isTabSelected = tab == selectedTab
    return cell
  }

}

// MARK: - UICollectionViewDelegate

extension RootController : UICollectionViewDelegate {

  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    selectedTab = tabs[indexPath.row]
    collectionView.reloadData()
  }

}


final class RootTabCell : UICollectionViewCell {

  typealias Tab = RootController.Tab

  @IBOutlet private var imageView: UIImageView!
  @IBOutlet private var label: UILabel!

  fileprivate var tab: Tab = .structure {
    didSet {
      switch tab {
      case .cabinet:
        label.text = "Поручения"
        imageView.image = #imageLiteral(resourceName: "icTabCabinet")
      case .statistic:
        label.text = "Статистика"
        imageView.image = #imageLiteral(resourceName: "icTabStat")
      case .structure:
        label.text = "Структура"
        imageView.image = #imageLiteral(resourceName: "icTabStruct")
      case .vacancies:
        label.text = "Вакансии"
        imageView.image = #imageLiteral(resourceName: "icTabVacancies")
      case .portal:
        label.text = "Портал"
        imageView.image = #imageLiteral(resourceName: "icTabPortal")
      case .training:
        label.text = "Обучение"
        imageView.image = #imageLiteral(resourceName: "icTabTraining")
      }
    }
  }

  var isTabSelected: Bool = true {
    didSet {
      if !isTabSelected {
        label.alpha = 0.6
        imageView.alpha = 0.6
      } else {
        label.alpha = 1
        imageView.alpha = 1
      }
    }
  }

}

extension UIViewController {

  var rootController: RootController? {
    var vc = parent
    while vc != nil {
      if let rootVC = vc as? RootController {
        return rootVC
      }
      vc = vc!.parent
    }
    return nil
  }

}
