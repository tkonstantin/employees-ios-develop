//
//  SearchBaseController.swift
//  Employees
//
//  Created by Sergey Erokhin on 29.03.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import RxSwift

class SearchBaseController : UIViewController {

  static let sharedSearch = Search()
  private let searchBar = UISearchBar()

  fileprivate var search = SearchBaseController.sharedSearch
  private(set) var disposeBag = DisposeBag()

  override func viewDidLoad() {
    super.viewDidLoad()
    setupSearchBar()
  }

  private func setupSearchBar() {

    searchBar.placeholder = "Поиск"
    searchBar.searchBarStyle = .minimal
    searchBar.tintColor = .white
    searchBar.delegate = self
    if let field = searchBar.value(forKey: "searchField") as? UITextField {
      field.textColor = .white
      field.font = UIFont.systemFont(ofSize: 14)
      field.attributedPlaceholder = "Поиск".attributed(with: .sysFont(14, .regular), .fg(UIColor.white.withAlphaComponent(0.8)))
    }
    searchBar.setImage(#imageLiteral(resourceName: "icSearch"), for: .search, state: .normal)
    searchBar.setImage(#imageLiteral(resourceName: "icClear"), for: .clear, state: .normal)
    searchBar.setImage(#imageLiteral(resourceName: "icClear"), for: .clear, state: .highlighted)
    let barButtonItem = UIBarButtonItem(customView: searchBar)
    navigationItem.rightBarButtonItem = barButtonItem
    barButtonItem.width = 288
    searchBar.frame = CGRect(x: 0, y: 0, width: 288, height: 44)
    searchBar.text = search.text
    searchBar.rx.text.orEmpty.debounce(0.3, scheduler: MainScheduler.instance).subscribe(onNext: { [weak self] text in
      self?.search.text = text
    }).addDisposableTo(disposeBag)
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    searchBar.text = search.text
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    EventLog.instance.add("Переход на экран '\(navigationItem.title ?? "")'")
  }

}

extension SearchBaseController : UISearchBarDelegate {

  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    let vc = SearchController()
    vc.searchBar = searchBar
    vc.search = search
    vc.handler = { [unowned self] item in
      if item.person == nil {
        let vacancyVC = self.storyboard!.instantiateViewController(withIdentifier: "vacancy") as! VacancyController
        vacancyVC.positionId = item.positionId
        vc.dismiss(animated: true) {
          self.navigationController?.pushViewController(vacancyVC, animated: true)
        }
      } else {
        let employeeVC = self.storyboard?.instantiateViewController(withIdentifier: "employee") as! EmployeeController
        employeeVC.positionId = item.positionId
        vc.dismiss(animated: true) {
          self.navigationController?.pushViewController(employeeVC, animated: true)
        }
      }
    }
    vc.modalPresentationStyle = .popover
    vc.preferredContentSize.width = 426
    present(vc, animated: true)
    vc.popoverPresentationController?.passthroughViews = [searchBar]
    vc.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
  }

  func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    if text.characters.count > 1 {
      return false
    }
    if text.hasPrefix(" ") && (searchBar.text!.isEmpty || searchBar.text!.hasSuffix(" ")) {
      return false
    }
    return true
  }
  
}
