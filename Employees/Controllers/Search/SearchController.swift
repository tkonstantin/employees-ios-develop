//
//  SearchController.swift
//  Employees
//
//  Created by Sergey Erokhin on 28.03.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import RxSwift

final class SearchController : UIViewController {

  @IBOutlet private var tableView: UITableView!
  @IBOutlet private var titleLabel: UILabel!
  @IBOutlet private var placeholderLabel: UILabel!

  private var disposable: Disposable?
  fileprivate var cellHeight: [IndexPath : CGFloat] = [:]
  fileprivate var historyItems: [SearchItem] = []
  fileprivate var showHistory: Bool = false {
    didSet {
      if showHistory {
        historyItems = SearchHistory.shared.items
        titleLabel.text = "Недавно искали"
      } else {
        titleLabel.text = "Варианты"
      }
      if !search!.text.isEmpty && !search!.isLoading && search!.items.isEmpty {
        self.tableView.isHidden = true
        self.placeholderLabel.superview?.isHidden = false
        let text = NSMutableAttributedString()
        text += "По запросу ".attributed(with: .sysFont(17, .regular))
        text += self.search!.text.attributed(with: .sysFont(17, .bold))
        text += " ничего не найдено".attributed(with: .sysFont(17, .regular))
        self.placeholderLabel.attributedText = text
      } else {
        self.tableView.isHidden = false
        self.placeholderLabel.superview?.isHidden = true
      }
      tableView.reloadData()
    }
  }

  var search: Search? {
    didSet {
      disposable?.dispose()
      disposable = search?.loadingResult.subscribe(onNext: { [weak self] result in
        guard let `self` = self, self.isViewLoaded else { return }
        switch result {
        case .success(let range):
          if range.startIndex == 0 {
            self.cellHeight.removeAll()
            self.tableView.contentOffset = .zero
            self.update()
          } else {
            self.tableView.reloadData()
          }
        case .failure(let error):
          debugPrint(error)
        }
      })
      tableView?.reloadData()
    }
  }

  var searchBar: UISearchBar?

  var handler: ((SearchItem) -> Void)?

  init() {
    super.init(nibName: "SearchController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    update()
    tableView.estimatedRowHeight = 114
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.register(UINib(nibName: "SearchItemCell", bundle: nil), forCellReuseIdentifier: "item")
    tableView.tableFooterView = UIView()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    EventLog.instance.add("Начало поиска")
  }

  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    EventLog.instance.add("Конец поиска")
  }

  private func update() {
    showHistory = search!.text.isEmpty
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    searchBar?.resignFirstResponder()
  }

}

extension SearchController : UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard !showHistory else {
      return historyItems.count
    }
    return search?.items.count ?? 0
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "item", for: indexPath) as! SearchItemCell
    cell.highlighter = search
    if showHistory {
      cell.item = historyItems[indexPath.row]
    } else {
      cell.item = search!.items[indexPath.row]
    }
    return cell
  }

}

extension SearchController : UITableViewDelegate {

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    let item = showHistory ? historyItems[indexPath.row] : search!.items[indexPath.row]
    SearchHistory.shared.add(item: item)
    EventLog.instance.add("Поиск > Выбор '\(item.positionName)'")
    handler?(item)
  }

  func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    cellHeight[indexPath] = cell.frame.height
  }

  func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    if let height = cellHeight[indexPath] {
      return height
    }
    return tableView.estimatedRowHeight
  }

  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    guard !showHistory && !search!.isLoading && !search!.ended else { return }
    let maxOffset = scrollView.contentSize.height - scrollView.frame.height
    if scrollView.contentOffset.y >= maxOffset {
      search?.loadMore()
    }
  }

}
