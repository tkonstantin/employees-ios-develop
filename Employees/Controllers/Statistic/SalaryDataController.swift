//
//  SalaryDataController.swift
//  Employees
//
//  Created by Sergey Erokhin on 04/05/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

final class SalaryDataController : SearchBaseController {

  @IBOutlet private var webView: UIWebView!

  override func viewDidLoad() {
    super.viewDidLoad()
    webView.loadRequest(URLRequest(url: Bundle.main.url(forResource: "table", withExtension: "html")!))
  }

}
