//
//  StatisticController.swift
//  Employees
//
//  Created by Sergey Erokhin on 01.04.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

final class StatisticController : SearchBaseController {

  @IBOutlet private var statisticViews: [StatisticView] = []

  override func viewDidLoad() {
    super.viewDidLoad()
    loadStatistic()
  }

  private func loadStatistic() {
    Client.instance.getStatistic().subscribe(onNext: { [weak self] stat in
      self?.statisticViews.forEach {
        $0.statistic = stat
      }
    }).addDisposableTo(disposeBag)
  }
}
