//
//  StatisticDummyView.swift
//  Employees
//
//  Created by Sergey Erokhin on 13/04/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

@IBDesignable
final class StatisticDummyView : StatisticView {

  @IBOutlet private var nameLabels: [UILabel] = []
  @IBOutlet private var sizeLabels: [UILabel] = []
  @IBOutlet private var countLabels: [UILabel] = []

  override var statistic: Statistic? {
    didSet {
      for (i, item) in statistic!.salaryData.enumerated() {
        nameLabels[i].text = item.name
        sizeLabels[i].text = "\(item.size) т. р."
        countLabels[i].text = item.count > 100 ? "\(item.count) чел." : String(item.count)
      }
        if statistic!.salaryData.count != 0 {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, animations: {
                    self.alpha = 1
                })
            }
        }
    }
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  private func setup() {
    self.alpha = 0

    let view = UINib(nibName: "StatisticDummyView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil).first! as! UIView
    view.frame = bounds
    view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    addSubview(view)
    let gr = UITapGestureRecognizer(target: self, action: #selector(showTable))
    view.addGestureRecognizer(gr)
  }

  @objc private func showTable() {
    guard let vc = findViewController() else { return }
    let tableVC = vc.storyboard!.instantiateViewController(withIdentifier: "salaryData")
    vc.navigationController?.pushViewController(tableVC, animated: true)
  }
  
}

@IBDesignable
final class DashedLineView : UIView {

  override var backgroundColor: UIColor? {
    set {
      shapeLayer.strokeColor = newValue?.cgColor
    }
    get {
      if let cgColor = shapeLayer.strokeColor {
        return UIColor(cgColor: cgColor)
      }
      return nil
    }
  }

  override class var layerClass: AnyClass {
    return CAShapeLayer.self
  }

  private var shapeLayer: CAShapeLayer {
    return layer as! CAShapeLayer
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  private func setup() {
    let sl = shapeLayer
    sl.lineWidth = 1 / UIScreen.main.scale
    sl.lineDashPattern = [3, 3]
    sl.fillColor = UIColor.clear.cgColor
  }

  override func layoutSubviews() {
    let path = CGMutablePath()
    path.move(to: .zero)
    path.addLine(to: CGPoint(x: frame.width, y: 0))
    shapeLayer.path = path
  }

  override var intrinsicContentSize: CGSize {
    return CGSize(width: frame.size.width, height: 1 / UIScreen.main.scale)
  }

}
