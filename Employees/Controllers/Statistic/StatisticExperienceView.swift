//
//  StatisticExperienceView.swift
//  Employees
//
//  Created by Sergey Erokhin on 14/04/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import Charts

@IBDesignable
final class StatisticExperienceView : StatisticView {

  private var chartView: BarChartView!

  override var statistic: Statistic? {
    didSet {
      counters = statistic?.positionsByExperience ?? []
    }
  }

  private var counters: [Counter] = [] {
    didSet {
      let entries = counters.enumerated().map { BarChartDataEntry(x: Double($0.offset), y: Double($0.element.value)) }
      let set1 = BarChartDataSet(values: entries, label: nil)
      set1.setColor(UIColor(rgba: 0x2589deff))
      let data = BarChartData(dataSet: set1)
      data.highlightEnabled = false
      data.setValueFont(UIFont.systemFont(ofSize: 16, weight: UIFontWeightMedium))
      data.setValueTextColor(UIColor(rgba: 0x2589deff))

      chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: counters.map({ $0.name }))
      
      chartView.data = data
      chartView.animate(yAxisDuration: 0.5)
        
        if entries.count != 0 {
            UIView.animate(withDuration: 0.3) {
                self.alpha = 1
            }
        }
    }
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  private func setup() {
    self.alpha = 0
    chartView = BarChartView()
    chartView.frame = bounds
    chartView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    addSubview(chartView)

    chartView.legend.enabled = false
    chartView.chartDescription?.enabled = false
    chartView.isUserInteractionEnabled = false

    let yAxis = chartView.leftAxis
    yAxis.drawLabelsEnabled = false
    yAxis.drawZeroLineEnabled = false
    yAxis.axisMinimum = 0
    yAxis.gridColor = .gubGreyish
    yAxis.axisLineColor = .gubGreyish
    yAxis.drawAxisLineEnabled = false
    yAxis.axisLineDashLengths = [4, 4]

    let xAxis = chartView.xAxis
    xAxis.labelFont = UIFont.systemFont(ofSize: 14, weight: UIFontWeightLight)
    xAxis.labelTextColor = UIColor(rgba: 0x868686ff)
    xAxis.labelPosition = .bottom
    xAxis.drawGridLinesEnabled = false
    xAxis.gridLineWidth = 1 / UIScreen.main.scale
    xAxis.axisLineWidth = 1 / UIScreen.main.scale
    xAxis.axisLineColor = UIColor(rgba: 0x868686ff)


    chartView.rightAxis.enabled = false
  }

  override func didMoveToWindow() {
    super.didMoveToWindow()
    if window != nil && statistic != nil {
      chartView.animate(yAxisDuration: 0.5)
    }
  }

  override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
//    counters = [
//      Counter(name: "до 1 года", value: 260),
//      Counter(name: "от 1 до\n3 лет", value: 433),
//      Counter(name: "от 3 до\n5 лет", value: 302),
//      Counter(name: "от 5 до\n7 лет", value: 1023),
//      Counter(name: "от 7 до\n10 лет", value: 160),
//      Counter(name: "от 10 лет", value: 70),
//    ]
  }

}
