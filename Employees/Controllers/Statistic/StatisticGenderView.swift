//
//  StatisticGenderView.swift
//  Employees
//
//  Created by Sergey Erokhin on 13/04/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

class StatisticView : UIView {

  var statistic: Statistic?

}

@IBDesignable
final class StatisticGenderView : StatisticView {

  @IBOutlet private var tableView: UITableView!
  @IBOutlet private var totalMaleLabel: UILabel!
  @IBOutlet private var totalFemaleLabel: UILabel!

  fileprivate var counters: [GenderCounter] = [] {
    didSet {
      tableView.reloadData()
    }
  }

  override var statistic: Statistic? {
    didSet {
        guard statistic != nil else {
            return;
        }
      counters = statistic!.genderCounters
      let total = Float(statistic!.totalMale + statistic!.totalFemale)
      let nf = NumberFormatter()
      nf.numberStyle = .percent
      nf.maximumFractionDigits = 0
      totalMaleLabel.text = nf.string(from: (Float(statistic!.totalMale) / total) as NSNumber)
      totalFemaleLabel.text = nf.string(from: (Float(statistic!.totalFemale) / total) as NSNumber)
        if counters.count != 0 {
            UIView.animate(withDuration: 0.3) {
                self.alpha = 1
            }
        }
    }
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  private func setup() {
    self.alpha = 0
    let bundle = Bundle(for: type(of: self))
    let nib = UINib(nibName: "StatisticGenderView", bundle: bundle)
    let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
    view.frame = bounds
    view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    addSubview(view)

    tableView.register(UINib(nibName: "StatisticGenderViewCell", bundle: bundle), forCellReuseIdentifier: "item")
  }

}

// MARK: - UITableViewDataSource

extension StatisticGenderView : UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return counters.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "item", for: indexPath) as! StatisticGenderViewCell
    cell.counter = counters[indexPath.row]
    return cell
  }

  override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
//    counters = [
//      GenderCounter(name: "Высшее Руководство", male: 100, female: 70),
//      GenderCounter(name: "Замминистры и замначальники ГУ", male: 500, female: 500),
//      GenderCounter(name: "Начальники управлений и заместители", male: 80, female: 20),
//      GenderCounter(name: "Начальники отделов и заместители", male: 200, female: 800),
//      GenderCounter(name: "Специалисты и консультанты", male: 400, female: 600)
//    ]
  }

}

// MARK: - UITableViewDelegate

extension StatisticGenderView : UITableViewDelegate {

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return tableView.frame.height / 5
  }

}

final class StatisticGenderViewCell : UITableViewCell {

  @IBOutlet private var progressView: UIProgressView!
  @IBOutlet private var maleLabel: UILabel!
  @IBOutlet private var femaleLabel: UILabel!
  @IBOutlet private var nameLabel: UILabel!

  var counter: GenderCounter! {
    didSet {
      nameLabel.text = counter.name
      progressView.progress = Float(counter.maleValue) / Float(counter.maleValue + counter.femaleValue)
      maleLabel.text = "\(counter.maleValue) чел."
      femaleLabel.text = "\(counter.femaleValue) чел."
    }
  }

}
