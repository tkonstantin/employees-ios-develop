//
//  StatisticMoscowInhabitantsView.swift
//  Employees
//
//  Created by Sergey Erokhin on 13/04/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit


@IBDesignable
final class StatisticMoscowInhabitantsView : StatisticView {

  private var percentConstraint: NSLayoutConstraint!
  @IBOutlet private var percentLabel: UILabel!
  @IBOutlet private var imageView: UIImageView!
  @IBOutlet private var fillView: UIView!

  override var statistic: Statistic? {
    didSet {
      percent = statistic?.mosregPercent ?? 0
    }
  }

  private var percent: Float = 0 {
    didSet {
      let formatter = NumberFormatter()
      formatter.numberStyle = .decimal
      formatter.maximumFractionDigits = 1
      percentLabel.text = formatter.string(from: percent as NSNumber)! + "%"
      percentConstraint?.isActive = false
      percentConstraint = NSLayoutConstraint(item: fillView, attribute: .height, relatedBy: .equal, toItem: imageView, attribute: .height, multiplier:  CGFloat(percent) / 100, constant: 0)
      percentConstraint.isActive = true
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1
        }
        
    }
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  private func setup() {
    self.alpha = 0
    let view = UINib(nibName: "StatisticMoscowInhabitantsView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil).first! as! UIView
    view.frame = bounds
    view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    addSubview(view)
  }

  override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
//    percent = 100
  }

}
