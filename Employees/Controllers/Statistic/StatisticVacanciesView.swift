//
//  StatisticVacanciesView.swift
//  Employees
//
//  Created by Sergey Erokhin on 26/04/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

@IBDesignable
final class StatisticVacanciesView : StatisticView {

  @IBOutlet private var nameLabels: [UILabel] = []
  @IBOutlet private var progressViews: [UIProgressView] = []
  @IBOutlet private var valueLabels: [UILabel] = []

  private var counters: [Counter] = [] {
    didSet {
      let count = min(nameLabels.count, counters.count)
      let maxCount = counters.reduce(0, { max($0, $1.value) })
      for i in 0 ..< count {
        let counter = counters[i]
        nameLabels[i].text = counter.name
        valueLabels[i].text = String(counter.value)
        progressViews[i].progress = Float(counter.value) / Float(maxCount)
      }
        
        for index in 1...3 {
            if count < index {
                nameLabels[index - 1].text = "-"
                valueLabels[index - 1].text = "-"
                 progressViews[index - 1].progress = 0
            }
        }
        
        if count != 0 {
            UIView.animate(withDuration: 0.3) {
                self.alpha = 1
            }
        }
    }
  }

  override var statistic: Statistic? {
    didSet {
      counters = statistic!.topOGVByVacancies
    }
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  private func setup() {
    self.alpha = 0
    let nib = UINib(nibName: "StatisticVacanciesView", bundle: Bundle(for: type(of: self)))
    let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
    view.frame = bounds
    view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    addSubview(view)
    let gr = UITapGestureRecognizer(target: self, action: #selector(showAllVacancies))
    view.addGestureRecognizer(gr)
  }

  @IBAction func showAllVacancies() {
    NotificationCenter.default.post(name: .GUBShowOGVVacancies, object: nil)
    findViewController()?.rootController?.show(tab: .vacancies)
  }

  override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
//    counters = [
//      Counter(name: "Администрация Губернатора Московской области", value: 35),
//      Counter(name: "Главное управление государственного административно-технического надзора Московской области", value: 35),
//      Counter(name: "Управление по обеспечению деятельности мировых судей Москвоской области", value: 32)
//    ]
  }

}
