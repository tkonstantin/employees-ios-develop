//
//  StructureController_v2.swift
//  Employees
//
//  Created by Sergey Erokhin on 03.04.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import RxSwift

final class StructureController_v2 : SearchBaseController, Structure {

  @IBOutlet private var collectionView: UICollectionView!

  enum Section {
    case root
    case levels
  }

  fileprivate var isLoaded: Bool = false
  fileprivate var levels: [StructureLevel] = []
  fileprivate var sections: [Section] = [.root, .levels]

  fileprivate var rootNode: GraphNode?
  fileprivate var rootCell: StructureRootCell_v2?

  override func viewDidLoad() {
    super.viewDidLoad()
    let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    activityIndicatorView.color = .gubGreyish
    activityIndicatorView.startAnimating()
    activityIndicatorView.hidesWhenStopped = true
    collectionView.backgroundView = activityIndicatorView
    (collectionView.collectionViewLayout as! StructureLayout).structure = self
    loadGraph()
  }

  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }

  private func loadGraph() {
    Client.instance.getRootNode().flatMap({ root -> Observable<[GraphNode]> in
      self.rootNode = root
      return Client.instance.getChildNodes(parentId: root.id)
    }).subscribe(onNext: { nodes in
      self.addLevel(nodes)
      self.isLoaded = true
      self.collectionView.backgroundView = nil
      self.collectionView.reloadData()
    }, onError: { error in
      debugPrint(error)
      DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
        self?.loadGraph()
      }
    }).addDisposableTo(disposeBag)
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    NotificationCenter.default.addObserver(self, selector: #selector(levelSelectionChanged(_:)), name: StructureLevel.selectionChanged, object: nil)
  }

  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    NotificationCenter.default.removeObserver(self, name: StructureLevel.selectionChanged, object: nil)
  }

  @discardableResult
  private func addLevel(_ nodes: [GraphNode]) -> StructureLevel {
    let items = nodes.map({ StructureItem(node: $0) })
    let level = StructureLevel(depth: levels.count, items: items)
    level.parent = levels.last
    levels.append(level)
    return level
  }

  func level(depth: Int) -> StructureLevel {
    return levels[depth]
  }

  var numLevels: Int {
    return levels.count
  }

  @objc private func levelSelectionChanged(_ notification: Notification) {
    guard let level = notification.object as? StructureLevel else { return }
    let cell = collectionView.cellForItem(at: IndexPath(row: level.depth, section: 1)) as? StructureLevelCell_v2
    cell?.update(animated: true)
    if level.selectedItem != nil {
      if level.depth == 0 {
        rootCell?.setShiftEnabled(true)
      }
      view.isUserInteractionEnabled = false
      Client.instance.getChildNodes(parentId: level.selectedItem!.node.id).subscribe(onNext: { nodes in
        self.view.isUserInteractionEnabled = true
        let newLevel = self.addLevel(nodes)
        let indexPath = IndexPath(row: newLevel.depth, section: 1)
        self.collectionView.insertItems(at: [indexPath])
        self.collectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
      }).addDisposableTo(disposeBag)
    } else {
      if level.depth == 0 {
        rootCell?.setShiftEnabled(false)
      }
      let range = level.depth + 1 ..< levels.count
      let indexPaths = range.map({ IndexPath(row: $0, section: 1) })
      levels.removeSubrange(range)
      collectionView.deleteItems(at: indexPaths)
      collectionView.scrollToItem(at: IndexPath(item: level.depth, section: 1), at: .centeredVertically, animated: true)
    }
  }

  func collapseAll() {
    guard let level = levels.first, level.selectedItem != nil else { return }
    level.selectedItem = nil
  }

}

extension StructureController_v2 : UICollectionViewDataSource {

  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return isLoaded ? sections.count : 0
  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    switch sections[section] {
    case .root:
      return 1
    case .levels:
      return levels.count
    }
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    switch sections[indexPath.section] {
    case .root:
      if rootCell == nil {
        rootCell = collectionView.dequeueReusableCell(withReuseIdentifier: "root", for: indexPath) as? StructureRootCell_v2
        rootCell?.node = rootNode
      }
      return rootCell!
    case .levels:
      return collectionView.dequeueReusableCell(withReuseIdentifier: "level", for: indexPath)
    }
  }

}

// MARK: - UICollectionViewDelegate

extension StructureController_v2 : UICollectionViewDelegate {

  func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    if sections[indexPath.section] == .levels {
      let cell = cell as! StructureLevelCell_v2
      UIView.performWithoutAnimation {
        cell.layoutIfNeeded()
      }
      cell.level = levels[indexPath.row]
    }
  }

}

protocol Structure : class {

  func level(depth: Int) -> StructureLevel
  var numLevels: Int { get }

}

final class StructureLayout : UICollectionViewLayout {

  private var attrsByIndexPath: [IndexPath : UICollectionViewLayoutAttributes] = [:]
  private var contentHeight: CGFloat = 0

  weak var structure: Structure?

  override func prepare() {
    attrsByIndexPath.removeAll()
    contentHeight = 0
    guard collectionView!.numberOfSections > 0 else { return }
    guard let structure = self.structure else { return }
    let width = collectionView!.frame.width
    let indexPath = IndexPath(item: 0, section: 0)
    let attrs = UICollectionViewLayoutAttributes(forCellWith: indexPath)
    attrs.frame = CGRect(x: 0, y: 0, width: width, height: 195)
    attrs.zIndex = 1000
    attrsByIndexPath[indexPath] = attrs
    contentHeight = attrs.frame.maxY

    for depth in 0 ..< structure.numLevels {
      let level = structure.level(depth: depth)
      let indexPath = IndexPath(item: depth, section: 1)
      let attrs = UICollectionViewLayoutAttributes(forCellWith: indexPath)
      attrs.zIndex = 1000 - indexPath.row
      attrs.frame = CGRect(x: 0, y: contentHeight, width: width, height: level.height)
      attrsByIndexPath[indexPath] = attrs
      contentHeight = attrs.frame.maxY - 16
    }
    contentHeight += 34
  }

  override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
    return attrsByIndexPath[indexPath]
  }

  override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
    var result: [UICollectionViewLayoutAttributes] = []
    for (_, attrs) in attrsByIndexPath where rect.intersects(attrs.frame) {
      result.append(attrs)
    }
    return result
  }

  override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
    let attrs = UICollectionViewLayoutAttributes(forCellWith: itemIndexPath)
    attrs.frame = layoutAttributesForItem(at: itemIndexPath)!.frame
    attrs.frame.origin.x = -attrs.frame.size.width
    attrs.alpha = 0
    attrs.zIndex = 100 - itemIndexPath.row
    return attrs
  }

  override func finalLayoutAttributesForDisappearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
    guard let cell = collectionView?.cellForItem(at: itemIndexPath) else { return nil }
    let attrs = UICollectionViewLayoutAttributes(forCellWith: itemIndexPath)
    attrs.frame = cell.frame
    attrs.frame.origin.x = -attrs.frame.size.width
    attrs.alpha = 0
    attrs.zIndex = 100 - itemIndexPath.row
    return attrs
  }

  override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
    return !collectionView!.bounds.size.equalTo(newBounds.size)
  }

  override var collectionViewContentSize : CGSize {
    return CGSize(width: collectionView?.frame.width ?? 0, height: contentHeight)
  }

}

