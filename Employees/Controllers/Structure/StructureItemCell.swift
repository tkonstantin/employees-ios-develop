//
//  StructureItemCell.swift
//  Employees
//
//  Created by Sergey Erokhin on 19.03.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import SnapKit

final class StructureItemCell : UICollectionViewCell {

  private var detailButton: UIButton!
  private var topLine: UIView!
  private var bottomLine: UIView!
  private var overlayView: UIView!
  private var mainView: StructureItemMainView!

  var item: StructureItem! {
    didSet {
      mainView.item = item
      refresh()
    }
  }

  override var isHighlighted: Bool {
    didSet {
      if isHighlighted && overlayView.isHidden && (item.node.position != nil || item.canSelect) {
        mainView.layer.borderWidth = 2
        mainView.layer.borderColor = UIColor.gubAccent.cgColor
      } else {
        mainView.layer.borderWidth = 0
        mainView.layer.borderColor = UIColor.clear.cgColor
      }
    }
  }

  func refresh() {
    overlayView.isHidden = true
    if item.isSelected {
      bottomLine.isHidden = false
      detailButton.isHidden = false
      detailButton.setImage(#imageLiteral(resourceName: "buttonUp"), for: .normal)
      detailButton.setImage(#imageLiteral(resourceName: "buttonUpBlue"), for: .highlighted)
    } else {
      bottomLine.isHidden = true
      if item.canSelect {
        detailButton.setImage(#imageLiteral(resourceName: "buttonDown"), for: .normal)
        detailButton.setImage(#imageLiteral(resourceName: "buttonDownBlue"), for: .highlighted)
        detailButton.isHidden = false
      } else {
        detailButton.isHidden = true
        if item.level?.selectedItem != nil {
          overlayView.isHidden = false
        }
      }
    }
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setup() {
    topLine = UIView()
    topLine.backgroundColor = UIColor(rgba: 0xc2c6cdff)
    contentView.addSubview(topLine)

    bottomLine = UIView()
    bottomLine.backgroundColor = topLine.backgroundColor
    contentView.addSubview(bottomLine)

    mainView = StructureItemMainView()
    mainView.backgroundColor = .white
    mainView.autoresizingMask = []
    contentView.addSubview(mainView)

    overlayView = UIView()
    overlayView.backgroundColor = .white
    overlayView.alpha = 0.9
    contentView.addSubview(overlayView)

    detailButton = UIButton(type: .custom)
    detailButton.addTarget(self, action: #selector(toggle), for: .touchUpInside)
    contentView.addSubview(detailButton)

    let gr = UITapGestureRecognizer(target: self, action: #selector(showProfile))
    gr.cancelsTouchesInView = false
    mainView.addGestureRecognizer(gr)
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    let centerX = frame.width * 0.5
    topLine.frame = CGRect(x: centerX - 1, y: 0, width: 2, height: 32)
    mainView.frame = CGRect(x: 12, y: 32, width: frame.width - 24, height: frame.height - 32 - 50)
    overlayView.frame = mainView.frame
    bottomLine.frame = CGRect(x: centerX - 1, y: mainView.frame.maxY, width: 2, height: 22)
    detailButton.frame = CGRect(x: centerX - 15, y: frame.height - 30, width: 30, height: 30)
  }

  @objc private func toggle() {
    superview?.bringSubview(toFront: self)
    item.isSelected = !item.isSelected
    if item.isSelected {
      EventLog.instance.add("Структура ↓ \(item.node.name)")
    } else {
      EventLog.instance.add("Структура ↑ \(item.node.name)")
    }
  }

  @objc private func showProfile() {
    guard let position = item.node.position, let vc = findViewController() else {
      if item.canSelect {
        toggle()
      }
      return
    }

    if position.person != nil {
      let employeeVC = vc.storyboard!.instantiateViewController(withIdentifier: "employee") as! EmployeeController
      employeeVC.positionId = position.id
      vc.navigationController?.pushViewController(employeeVC, animated: true)
    } else {
      let vacancyVC = vc.storyboard?.instantiateViewController(withIdentifier: "vacancy") as! VacancyController
      vacancyVC.positionId = position.id
      vc.navigationController?.pushViewController(vacancyVC, animated: true)
    }
  }

  static func calculateSize(item: StructureItem) -> CGSize {
    var size = StructureItemMainView.calculateSize(item: item)
    size.width += 24
    size.height += 32 + 50
    return size
  }

}

private final class StructureItemMainView : UIView {

  struct UX {
    static let positionFont = UIFont.systemFont(ofSize: 16, weight: UIFontWeightSemibold)
    static let imageSize = CGSize(width: 70, height: 70)
    static let maxTextHeight = ceil(positionFont.lineHeight * 3)
    static let positionAttrs = String.attributes(.font(positionFont))
    static let nameAttrs = String.attributes(.sysFont(14, .light))
    static let minTextWidth: CGFloat = 240
    static let minNameWidth: CGFloat = minTextWidth - imageSize.width - 16
  }

  private var imageView: UIImageView!
  private var positionLabel: UILabel!
  private var nameLabel: UILabel!
  private var vacancyView: VacancyView!

  var item: StructureItem! {
    didSet {
      if let position = item.node.position {
        positionLabel.text = item.node.name
        if let person = position.person {
          nameLabel.text = person.fullName
          var placeholder: UIImage!
          switch person.sex {
          case .male:
            placeholder = #imageLiteral(resourceName: "placeholderAvatarMale")
          case .female:
            placeholder = #imageLiteral(resourceName: "placeholderAvatarFeemale")
          case .unknown:
            placeholder = #imageLiteral(resourceName: "placeholderAvatarArmchair")
          }
          if let url = person.photoURL {
            imageView.kf.setImage(with: url, placeholder: placeholder, options: [.transition(.fade(0.3)), .processor(AvatarProcessor(width: 70))])
          } else {
            imageView.image = placeholder
          }
          vacancyView.isHidden = false
        } else {
          nameLabel.text = "Вакансия"
          imageView.image = #imageLiteral(resourceName: "placeholderAvatarArmchair")
          vacancyView.isHidden = true
        }
        if position.numWorkplaces == 0 {
          vacancyView.isHidden = true
        }
        vacancyView.total = item.node.numWorkplaces
        vacancyView.free = item.node.numVacancies
      } else {
        nameLabel.text = item.node.name
        imageView.image = #imageLiteral(resourceName: "placeholderOGV")
        vacancyView.isHidden = true
      }
      setNeedsLayout()
    }
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setup() {

    isOpaque = true
    clipsToBounds = true
    backgroundColor = .white

    positionLabel = UILabel()
    positionLabel.font = UX.positionFont
    positionLabel.textColor = .black
    positionLabel.numberOfLines = 3
    positionLabel.isOpaque = true
    positionLabel.backgroundColor = .white
    positionLabel.contentMode = .topLeft
    addSubview(positionLabel)

    imageView = UIImageView()
    imageView.contentMode = .scaleAspectFit
    imageView.clipsToBounds = true
    addSubview(imageView)

    nameLabel = UILabel()
    nameLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightLight)
    nameLabel.numberOfLines = 0
    nameLabel.isOpaque = true
    nameLabel.backgroundColor = .white
    addSubview(nameLabel)

    vacancyView = VacancyView()
    addSubview(vacancyView)
  }

  override func layoutSubviews() {
    guard let item = self.item else { return }
    if item.node.position != nil {
      imageView.frame = CGRect(x: 20, y: frame.height - 20 - UX.imageSize.height, width: UX.imageSize.width, height: UX.imageSize.height)
      let positionTextSize = positionLabel.sizeThatFits(CGSize(width: frame.width - 40, height: .greatestFiniteMagnitude))
      positionLabel.frame = CGRect(x: 20, y: 20, width: frame.width - 40, height: ceil(positionTextSize.height))
      let nameOrigin = CGPoint(x: imageView.frame.maxX + 16, y: imageView.frame.origin.y)
      if vacancyView.isHidden {
        nameLabel.frame = CGRect(origin: nameOrigin, size: CGSize(width: frame.width - nameOrigin.x - 20, height: imageView.frame.height))
      } else {
        vacancyView.sizeToFit()
        vacancyView.frame.origin = CGPoint(x: nameOrigin.x, y: imageView.frame.maxY - vacancyView.frame.height)
        nameLabel.frame = CGRect(origin: nameOrigin, size: CGSize(width: frame.width - nameOrigin.x - 20, height: imageView.frame.height - vacancyView.frame.height - 4))
      }
    } else {
      imageView.frame = CGRect(x: 20, y: (frame.height - UX.imageSize.height) * 0.5, width: UX.imageSize.width, height: UX.imageSize.height)
      let nameOrigin = CGPoint(x: imageView.frame.maxX + 16, y: 0)
      nameLabel.frame = CGRect(origin: nameOrigin, size: CGSize(width: frame.width - nameOrigin.x - 20, height: frame.height))
    }
  }

  static func calculateSize(item: StructureItem) -> CGSize {
    let step: CGFloat = 40
    var size = CGSize.zero
    if item.node.position == nil {
      var width = UX.minNameWidth - step
      repeat {
        width += step
        size = item.node.name.size(forWidth: width, attributes: UX.nameAttrs)
      } while(size.height > 80)
      size.height = max(size.height, UX.imageSize.height)
      size.height += 20 + 20
      size.width += 20 + UX.imageSize.width + 16 + 20
    } else {
      var width = UX.minTextWidth - step
      repeat {
        width += step
        size = item.node.name.size(forWidth: width, attributes: UX.positionAttrs)
      } while(size.height > UX.maxTextHeight)
      size.width = max(ceil(size.width), UX.minTextWidth)
      size.width += 20 + 20
      size.height = ceil(size.height) + 20 + 16 + UX.imageSize.height + 20
    }
    return size
  }

}
