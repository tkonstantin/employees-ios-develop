//
//  StructureLevel.swift
//  Employees
//
//  Created by Sergey Erokhin on 19.03.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import RxSwift

final class StructureLevel {

  static let selectionChanged = Notification.Name("StructureLevelSelectionChanged")

  let depth: Int
  let items: [StructureItem]
  private(set) var height: CGFloat = 0

  var selectedItem: StructureItem? {
    didSet {
      guard selectedItem !== oldValue else { return }
      NotificationCenter.default.post(name: StructureLevel.selectionChanged, object: self)
    }
  }

  var parent: StructureLevel?
  var offset: CGFloat = 0

  init(depth: Int, items: [StructureItem]) {
    self.depth = depth
    self.items = items
    items.forEach {
      $0.level = self
      height = max(height, $0.size.height)
    }
  }
}

final class StructureItem {

  fileprivate(set) weak var level: StructureLevel?
  fileprivate var index: Int = 0
  let node: GraphNode

  private(set) var size: CGSize = .zero

  var isSelected: Bool {
    get {
      return level?.selectedItem === self
    }
    set {
      guard isSelected != newValue else { return }
      if newValue {
        level?.selectedItem = self
      } else {
        level?.selectedItem = nil
      }
    }
  }

  var canSelect: Bool {
    return node.childCount > 0 && (level?.selectedItem == nil || level?.selectedItem === self)
  }

  init(node: GraphNode) {
    self.node = node
    size = StructureItemCell.calculateSize(item: self)
  }

}
