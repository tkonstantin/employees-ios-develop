//
//  StructureLevelCell_v2.swift
//  Employees
//
//  Created by Sergey Erokhin on 03.04.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

final class StructureLevelCell_v2 : UICollectionViewCell {

  @IBOutlet private var collectionView: UICollectionView!

  var level: StructureLevel! {
    didSet {
      guard level !== oldValue else { return }
      (collectionView.collectionViewLayout as! StructureLevelLayout_v2).level = level
      collectionView.reloadData()
      collectionView.contentOffset = .zero
    }
  }

  func update(animated: Bool = true) {
    let newLayout = StructureLevelLayout_v2()
    newLayout.level = level
    for cell in collectionView.visibleCells {
      (cell as! StructureItemCell).refresh()
    }
    collectionView.setCollectionViewLayout(newLayout, animated: animated)
  }

  override func awakeFromNib() {
    collectionView.contentInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
    collectionView.register(LevelHorizontalLine.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "line")
    collectionView.register(StructureItemCell.self, forCellWithReuseIdentifier: "itemPosition1")
    collectionView.register(StructureItemCell.self, forCellWithReuseIdentifier: "itemOGV1")
  }

  override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
    super.apply(layoutAttributes)
    layer.zPosition = CGFloat(layoutAttributes.zIndex)
  }

}

extension StructureLevelCell_v2 : UICollectionViewDataSource {

  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return level?.items.count ?? 0
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let item = level.items[indexPath.row]
    let cellID = item.node.position != nil ? "itemPosition1" : "itemOGV1"
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! StructureItemCell
    cell.item = item
    return cell
  }

}

extension StructureLevelCell_v2 : UICollectionViewDelegate {

  func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
    return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "line", for: indexPath)
  }

}

final class LevelHorizontalLine : UICollectionReusableView {

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  private func setup() {
    backgroundColor = .gubGraphLine
  }

}
