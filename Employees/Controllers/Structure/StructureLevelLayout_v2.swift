//
//  StructureLevelLayout_v2.swift
//  Employees
//
//  Created by Sergey Erokhin on 03.04.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

final class StructureLevelLayout_v2 : UICollectionViewLayout {

  private var attrsByIndexPath: [IndexPath : UICollectionViewLayoutAttributes] = [:]
  private let lineAttrs = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, with: IndexPath(item: 0, section: 0))
  private var start = true

  var level: StructureLevel?
  
  private var contentWidth: CGFloat = 0

  override func invalidateLayout() {
    attrsByIndexPath.removeAll()
    contentWidth = 0
    
    defer {
      super.invalidateLayout()
    }

    guard let level = self.level else { return }
    let selectedItemIndex: Int = {
      if let item = level.selectedItem {
        return level.items.index(where: { $0 === item })!
      }
      return -1
    }()
    let parentOffset: CGFloat = level.parent?.offset ?? 0
    var x: CGFloat = 0
    let height: CGFloat = level.height

    if level.items.count == 1 {
      let indexPath = IndexPath(item: 0, section: 0)
      let attrs = UICollectionViewLayoutAttributes(forCellWith: indexPath)
      var size = level.items[0].size
      size.height = height
      attrs.frame = CGRect(origin: CGPoint(x: parentOffset - size.width / 2, y: 0), size: size)
      attrsByIndexPath[indexPath] = attrs
      contentWidth = parentOffset + size.width / 2
      level.offset = attrs.frame.midX
      lineAttrs.frame = CGRect(x: parentOffset, y: 0, width: 0, height: 2)
    } else if selectedItemIndex >= 0 {
      let w2 = level.items[selectedItemIndex].size.width / 2
      let offsetX: CGFloat = max(parentOffset, max(250, w2)) - w2 + 36
      x = offsetX
      let limit = min(selectedItemIndex + 3, level.items.count)
      for index in (0 ..< level.items.count).reversed() {
        let indexPath = IndexPath(item: index, section: 0)
        let attrs = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        var size = level.items[index].size
        size.height = height
        if index == selectedItemIndex {
          attrs.frame = CGRect(origin: CGPoint(x: offsetX, y: 0), size: size)
          contentWidth = attrs.frame.maxX
          level.offset = attrs.frame.midX
          attrs.zIndex = 1000
          lineAttrs.frame = CGRect(x: -8, y: 0, width: level.offset + 8, height: 2)
        } else if index > limit {
          attrs.isHidden = true
          attrs.frame = CGRect(origin: CGPoint(x: -size.width - 50, y: 0), size: size)
        } else {
          attrs.zIndex = 1
          x -= size.width
          attrs.frame = CGRect(origin: CGPoint(x: x, y: 0), size: size)
        }
        attrsByIndexPath[indexPath] = attrs
      }
    } else {
      for index in 0 ..< level.items.count {
        let indexPath = IndexPath(item: index, section: 0)
        let attrs = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        var size = level.items[index].size
        size.height = height
        attrs.frame = CGRect(origin: CGPoint(x: x, y: 0), size: size)
        x += size.width
        attrsByIndexPath[indexPath] = attrs
        if index == 0 {
          lineAttrs.frame.origin.x = attrs.frame.midX
        } else if index == level.items.count - 1 {
          lineAttrs.frame.size.width = attrs.frame.midX - lineAttrs.frame.origin.x
        }

      }
      if level.depth == 0 {
        let maxWidth = collectionView!.bounds.width - collectionView!.contentInset.left - collectionView!.contentInset.right
        if maxWidth > x {
          let offset = (maxWidth - x) / 2
          x = maxWidth
          for (_, attr) in attrsByIndexPath {
            attr.frame.origin.x += offset
          }
          lineAttrs.frame.origin.x += offset
        }
      }
      lineAttrs.frame.origin.y = 0
      lineAttrs.frame.size.height = 2
      contentWidth = x
    }
  }

  override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
    return attrsByIndexPath[indexPath]
  }

  override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
    var result: [UICollectionViewLayoutAttributes] = []
    for (_, attrs) in attrsByIndexPath where rect.intersects(attrs.frame) {
      result.append(attrs)
    }
    result.append(lineAttrs)
    return result
  }

  override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
    guard elementKind == UICollectionElementKindSectionFooter else { return nil }
    return lineAttrs
  }

  override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
    return !collectionView!.bounds.size.equalTo(newBounds.size)
  }

  override var collectionViewContentSize : CGSize {
    return CGSize(width: contentWidth, height: collectionView?.frame.height ?? 0)
  }

}
