//
//  StructureRootCell_v2.swift
//  Employees
//
//  Created by Sergey Erokhin on 03.04.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

final class StructureRootCell_v2 : UICollectionViewCell {

  @IBOutlet private var imageView: UIImageView!
  @IBOutlet private var positionLabel: UILabel!
  @IBOutlet private var nameLabel: UILabel!
  @IBOutlet private var mainView: UIView!
  @IBOutlet private var mainViewLeftConstraint: NSLayoutConstraint!

  private var beginPoint: CGPoint = .zero

  var node: GraphNode! {
    didSet {
      positionLabel.text = node.name
      nameLabel.text = node.position?.person?.fullName
      if let url = node.position?.person?.photoURL {
        imageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholderAvatarMale"), options: [.transition(.fade(0.3)), .processor(AvatarProcessor(width: 116))])
      } else {
        imageView.image = #imageLiteral(resourceName: "placeholderAvatarMale")
      }
    }
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    backgroundColor = .clear
    let gr = UILongPressGestureRecognizer(target: self, action: #selector(handleTap(_:)))
    gr.minimumPressDuration = 0
    gr.cancelsTouchesInView = false
    gr.delaysTouchesBegan = true
    gr.delegate = self
    mainView.addGestureRecognizer(gr)
    mainView.layer.borderColor = UIColor.gubAccent.cgColor
  }

  func setShiftEnabled(_ enabled: Bool, animated: Bool = true) {
    mainViewLeftConstraint.isActive = enabled
    if animated {
      UIView.animate(withDuration: 0.3, animations: {
        self.layoutIfNeeded()
      })
    }
  }

  @objc private func handleTap(_ sender: UILongPressGestureRecognizer) {
    switch sender.state {
    case .began:
      mainView.layer.borderWidth = 2
      beginPoint = sender.location(in: mainView)
    case .changed:
      let point = sender.location(in: mainView)
      if !beginPoint.equalTo(point) {
        sender.isEnabled = false
        sender.isEnabled = true
      }
    case .cancelled, .ended, .failed:
      mainView.layer.borderWidth = 0
      if sender.state == .ended {
        showProfile()
      }
    default:
      break
    }
  }

  private func showProfile() {
    guard let position = node.position, position.person != nil, let vc = findViewController() else {
      return
    }
    let employeeVC = vc.storyboard!.instantiateViewController(withIdentifier: "employee") as! EmployeeController
    employeeVC.positionId = position.id
    vc.navigationController?.pushViewController(employeeVC, animated: true)
  }

}

extension StructureRootCell_v2 : UIGestureRecognizerDelegate {

  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    if otherGestureRecognizer is UIPanGestureRecognizer {
      return true
    }
    return false
  }

  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return false
  }

  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return true
  }

}
