//
//  VacanciesController.swift
//  Employees
//
//  Created by Sergey Erokhin on 19/04/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import RxSwift

extension Notification.Name {

  static let GUBShowOGVVacancies = Notification.Name("GUBShowOGVVacancies")

}

final class VacanciesController : SearchBaseController {

  @IBOutlet fileprivate var leftTableView: UITableView!
  @IBOutlet fileprivate var rightTableView: UITableView!
  @IBOutlet fileprivate var segmentedControl: UISegmentedControl!
  @IBOutlet private var placeholderView: UIView!

  fileprivate var allPositionsCount: Int = 0

  enum Tab {
    case ogv
    case categories
  }

  fileprivate var divisions: [Division] = []
  fileprivate var categories: [PositionCategory] = []
  fileprivate var allVacancies: [Position] = []
  fileprivate var vacancies: [Position] = [] {
    didSet {
      rightTableView.reloadData()
      placeholderView.isHidden = !vacancies.isEmpty
      DispatchQueue.main.async {
        self.rightTableView.setContentOffset(.zero, animated: false)
      }
    }
  }
  fileprivate var disposable: Disposable?
  let tabs: [Tab] = [.categories, .ogv]

  private(set) var selectedTab: Tab = .categories {
    didSet {
      guard oldValue != selectedTab else { return }
      leftTableView.reloadData()
      DispatchQueue.main.async {
        self.leftTableView.contentOffset = .zero
      }
      showAllVacancies()
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    leftTableView.tableFooterView = UIView()
    leftTableView.rowHeight = UITableViewAutomaticDimension
    leftTableView.estimatedRowHeight = 63

    rightTableView.tableFooterView = UIView()
    rightTableView.rowHeight = UITableViewAutomaticDimension
    rightTableView.estimatedRowHeight = 63

    placeholderView.isHidden = true

    NotificationCenter.default.addObserver(self, selector: #selector(showOGVVacancies), name: .GUBShowOGVVacancies, object: nil)

    load()
  }

  @objc private func showOGVVacancies() {
    navigationController?.viewControllers = [self]
    segmentedControl.selectedSegmentIndex = 1
    changeTab(segmentedControl)
  }

  deinit {
    NotificationCenter.default.removeObserver(self)
  }

  private func load() {
    Observable.zip(
      Client.instance.getNumVacanciesForOGV(),
      Client.instance.getNumVacanciesForCategories(),
      Client.instance.getAllVacancies(),
      resultSelector: { ($0, $1, $2) }).subscribe(onNext: { [weak self] divisions, categories, allVacancies in
        guard let `self` = self else { return }
        self.divisions = divisions
        self.categories = categories
        self.allVacancies = allVacancies
        self.allPositionsCount = divisions.reduce(0, { $0 + $1.numPositions })
        self.leftTableView.reloadData()
        if self.leftTableView.numberOfSections > 0 && self.leftTableView.numberOfRows(inSection: 0) > 0 {
            self.leftTableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .top)
        }
        self.showAllVacancies()
      }, onError: { [weak self] error in
        print(error)
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
          self?.load()
        }
      }).addDisposableTo(disposeBag)
  }

  @IBAction func changeTab(_ sender: UISegmentedControl) {
    selectedTab = tabs[sender.selectedSegmentIndex]
  }

  func showAllVacancies() {
    disposable?.dispose()
    disposable = nil
    vacancies = allVacancies
  }

  func showVacancies(for category: PositionCategory) {
    disposable?.dispose()
    disposable = Client.instance.getVacancies(for: category).subscribe(onNext: { [weak self] vacancies in
      guard let `self` = self else { return }
      self.vacancies = vacancies
      }, onError: { error in
        print(error)
    })

  }

  func showVacancies(for division: Division) {
    disposable?.dispose()
    disposable = Client.instance.getVacancies(for: division).subscribe(onNext: { [weak self] vacancies in
      guard let `self` = self else { return }
      self.vacancies = vacancies
      }, onError: { error in
        print(error)
    })
  }

}

// MARK: - UITableViewDataSource

extension VacanciesController : UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tableView == leftTableView {
      switch selectedTab {
      case .ogv:
        return divisions.isEmpty ? 0 : divisions.count + 1
      case .categories:
        return categories.isEmpty ? 0 : categories.count + 1
      }
    }
    return vacancies.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if tableView == leftTableView {
      let cell = tableView.dequeueReusableCell(withIdentifier: "item", for: indexPath) as! VacanciesMenuCell
      if indexPath.row == 0 {
        cell.setAllVacancies(allVacancies.count, of: allPositionsCount)
      } else {
        switch selectedTab {
        case .ogv:
          cell.division = divisions[indexPath.row - 1]
        case .categories:
          cell.category = categories[indexPath.row - 1]
        }
      }
      return cell
    } else {
      let cell = tableView.dequeueReusableCell(withIdentifier: "vacancy", for: indexPath) as! VacancyCell
      cell.position = vacancies[indexPath.row]
      return cell
    }
  }

}

extension VacanciesController : UITableViewDelegate {

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if tableView == leftTableView {
      if indexPath.row == 0 {
        showAllVacancies()
      } else {
        switch selectedTab {
        case .ogv:
          showVacancies(for: divisions[indexPath.row - 1])
        case .categories:
          showVacancies(for: categories[indexPath.row - 1])
        }
      }
    } else {
      tableView.deselectRow(at: indexPath, animated: true)
      let vacancyVC = storyboard!.instantiateViewController(withIdentifier: "vacancy") as! VacancyController
      vacancyVC.positionId = vacancies[indexPath.row].id
      navigationController?.pushViewController(vacancyVC, animated: true)
    }
  }

}
