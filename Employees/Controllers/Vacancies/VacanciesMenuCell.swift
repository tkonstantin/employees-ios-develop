//
//  VacanciesMenuCell.swift
//  Employees
//
//  Created by Sergey Erokhin on 19/04/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

final class VacanciesMenuCell : UITableViewCell {

  @IBOutlet private var nameLabel: UILabel!
  @IBOutlet private var countLabel: UILabel!
  @IBOutlet private var totalCountLabel: UILabel!

  var division: Division? {
    didSet {
      guard let division = self.division else { return }
      category = nil
      nameLabel.text = division.name
      countLabel.text = String(division.numVacancies)
      totalCountLabel.text = "из \(division.numPositions)"
    }
  }

  var category: PositionCategory? {
    didSet {
      guard let category = self.category else { return }
      division = nil
      nameLabel.text = category.name
      countLabel.text = String(category.numVacancies)
      totalCountLabel.text = "из \(category.numPositions)"
    }
  }

  func setAllVacancies(_ count: Int, of: Int) {
    category = nil
    division = nil
    nameLabel.text = "Все вакансии"
    countLabel.text = String(count)
    totalCountLabel.text = "из \(of)"
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    let color = selected ? UIColor(rgba: 0x0088ceff) : UIColor.black
    nameLabel.textColor = color
    countLabel.textColor = color
    totalCountLabel.textColor = selected ? UIColor(rgba: 0x0088ceff) : UIColor(rgba: 0x868686ff)
  }

}
