//
//  VacancyCell.swift
//  Employees
//
//  Created by Sergey Erokhin on 19/04/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

final class VacancyCell : UITableViewCell {

  @IBOutlet private var divisionLabel: UILabel!
  @IBOutlet private var nameLabel: UILabel!
  @IBOutlet private var dateLabel: UILabel!
  @IBOutlet private var candidatesImageView: UIImageView!

  var position: Position! {
    didSet {
      let headDivisionName = position.headDivision?.name ?? ""
      if let division = position.division {
        if !headDivisionName.isEmpty && headDivisionName != division.name {
          divisionLabel.text = headDivisionName + "\n" + division.name
        } else {
          divisionLabel.text = division.name
        }
        divisionLabel.isHidden = false
      } else {
        divisionLabel.isHidden = true
      }
      dateLabel.text = {
        let date = position.vacancyInfo?.openDate ?? Date()
        let df = DateFormatter()
        df.dateFormat = "dd.MM.yyyy"
        return df.string(from: date)
      }()
      nameLabel.text = position.name
      candidatesImageView.isHidden = !position.hasCandidates
    }
  }

}
