//
//  VacancyCandidateCell.swift
//  Employees
//
//  Created by Sergey Erokhin on 28.04.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import Charts
import Kingfisher

final class VacancyCandidateCell: UITableViewCell {

  struct UX {
    static let positionAttrs = String.attributes(.sysFont(16, .bold), .fg(.black), .psBefore(4))
    static let nameAttrs = String.attributes(.sysFont(16, .light), .fg(.black), .psBefore(4))
    static let divisionAttrs = String.attributes(.sysFont(15, .regular), .fg(.black), .psBefore(4))
    static let divisionAttrs2 = String.attributes(.sysFont(16, .regular), .fg(UIColor(rgba: 0x868686ff)))
  }

  @IBOutlet private var avatarImageView: UIImageView!
  @IBOutlet private var label: UILabel!
  @IBOutlet private var chartView: RadarChartView!
  @IBOutlet private var addButton: UIButton?
  @IBOutlet private var deleteButton: UIButton?

  var handler: ((VacancyCandidate, VacancyCandidate.Action) -> Void)?
  weak var highlighter: SearchHighligter?

  override var layoutMargins: UIEdgeInsets {
    get { return .zero }
    set {}
  }

  var candidate: VacancyCandidate! {
    didSet {
      if let person = candidate.person {
        let placeholder = person.sex == .male ? #imageLiteral(resourceName: "placeholderAvatarMale") : #imageLiteral(resourceName: "placeholderAvatarFeemale")
        if let url = person.photoURL {
          avatarImageView.kf.setImage(with: url, placeholder: placeholder, options: [.transition(.fade(0.3)), .processor(AvatarProcessor(width: 70))])
        } else {
          avatarImageView.image = placeholder
        }
      }
      label.attributedText = {
        let text = NSMutableAttributedString()
        if let position = candidate.candidate {
          text += (position.name + "\n", UX.positionAttrs)
          text += (candidate.person!.fullName, UX.nameAttrs)
          if let division = position.division {
            if reuseIdentifier == "item" {
              text += ("\n" + division.name, UX.divisionAttrs)
            } else {
              text.insert(NSAttributedString(string: division.name + "\n", attributes: UX.divisionAttrs2), at: 0)
            }
          }
        } else {
          text += (candidate.person!.fullName, UX.nameAttrs)
        }
        highlighter?.highlight(text: text)
        return text
      }()
      setupChartView()
      refresh()
    }
  }

  private func setupChartView() {
    let competencies = candidate.competence
    let set1 = RadarChartDataSet(values: competencies.map({ RadarChartDataEntry(value: Double($0.requiredValue)) }), label: "Требуемые компетенции")
    set1.fillAlpha = 0.7
    set1.lineWidth = 2
    set1.drawFilledEnabled = true
    set1.setColor(UIColor(rgba: 0x51ae83ff))
    set1.fillColor = set1.color(atIndex: 0)

    let set2 = RadarChartDataSet(values: competencies.map({ RadarChartDataEntry(value: Double($0.value)) }), label: "Компетенции сотрудника")
    set2.fillAlpha = 0.7
    set2.lineWidth = 2
    set2.drawFilledEnabled = true
    set2.setColor(UIColor(rgba: 0x2073d6ff))
    set2.fillColor = set2.color(atIndex: 0)

    let data = RadarChartData(dataSets: [set1, set2])
    data.labels = competencies.map({ $0.name })
    data.setDrawValues(false)
    data.highlightEnabled = false
    chartView.data = data
  }

  override func awakeFromNib() {
    super.awakeFromNib()

    addButton?.titleLabel?.textAlignment = .center

    for axis in [chartView.xAxis, chartView.yAxis] {
      axis.axisLineWidth = 1
      axis.gridLineWidth = 1
      axis.gridColor = .gubGrid
      axis.drawLabelsEnabled = false
      axis.axisMinimum = 0
      axis.labelCount = 5
    }
    chartView.legend.enabled = false
    chartView.webLineWidth = 1
    chartView.webColor = .gubGrid
    chartView.chartDescription?.enabled = false
  }

  func refresh() {
    if let button = addButton {
      if candidate.isSelected {
        button.isEnabled = false
        button.setTitle("Кандидат в\nотобранных", for: .normal)
      } else {
        button.isEnabled = true
        button.setTitle("Добавить в\nотобранные", for: .normal)
      }
    }
  }

  @IBAction func addCandidate() {
    guard !candidate.isSelected else { return }
    handler?(candidate, .add)
  }

  @IBAction func deleteCandidate() {
    guard candidate.isSelected else { return }
    handler?(candidate, .delete)
  }

}
