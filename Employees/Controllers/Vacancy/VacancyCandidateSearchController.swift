//
//  VacancyCandidateSearchController.swift
//  Employees
//
//  Created by Sergey Erokhin on 02.05.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import RxSwift

final class VacancyCandidateSearchController : UIViewController {

  @IBOutlet private var tableView: UITableView!
  @IBOutlet private var searchBar: UISearchBar!
  @IBOutlet private var bottomConstraint: NSLayoutConstraint!
  @IBOutlet private var placeholderLabel: UILabel!
  @IBOutlet private var separatorView: SeparatorView!

  fileprivate var cellHeight: [IndexPath : CGFloat] = [:]
  private var disposeBag = DisposeBag()
  fileprivate var search: VacancyCandidateSearch! {
    didSet {
      search.loadingResult.subscribe(onNext: { [weak self] result in
        guard let `self` = self, self.isViewLoaded else { return }
        switch result {
        case .success(let range):
          if range.startIndex == 0 {
            self.cellHeight.removeAll()
            self.update()
            self.tableView.reloadData()
            DispatchQueue.main.async {
              self.tableView.contentOffset = .zero
            }
          } else {
            self.tableView.reloadData()
          }
        case .failure(let error):
          debugPrint(error)
        }
      }).addDisposableTo(disposeBag)
      tableView?.reloadData()
    }
  }

  fileprivate var resultSubject = PublishSubject<VacancyCandidate>()

  var result: Observable<VacancyCandidate> {
    return resultSubject.asObservable()
  }

  var position: Position!

  override func viewDidLoad() {
    super.viewDidLoad()
    automaticallyAdjustsScrollViewInsets = false
    search = VacancyCandidateSearch(position: position)
    searchBar.rx.text.orEmpty.debounce(0.3, scheduler: MainScheduler.instance).subscribe(onNext: { [weak self] text in
      self?.search.text = text
    }).addDisposableTo(disposeBag)
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 114
    tableView.tableFooterView = UIView()
    update()
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowOrHide(_:)), name: .UIKeyboardWillShow, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowOrHide(_:)), name: .UIKeyboardWillHide, object: nil)
  }

  deinit {
    NotificationCenter.default.removeObserver(self)
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    searchBar.becomeFirstResponder()
  }

  func refresh() {
    for cell in tableView.visibleCells {
      if let cell = cell as? VacancyCandidateCell {
        cell.refresh()
      }
    }
  }

  @IBAction func done() {
    resultSubject.onCompleted()
    searchBar.resignFirstResponder()
    navigationController?.dismiss(animated: true)
  }

  func keyboardWillShowOrHide(_ notification: Notification) {
    let userInfo = notification.userInfo!
    let dt = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
    let kbFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
    var frame = view.superview!.convert(view.frame, to: nil)
    if notification.name == .UIKeyboardWillShow {
      frame.origin.y = 64
    }
    let bottom = max(frame.maxY - kbFrame.minY, 0)
    UIView.animate(withDuration: dt, delay: 0, options: .beginFromCurrentState, animations: {
      self.bottomConstraint?.constant = bottom
      self.view!.layoutIfNeeded()
    }, completion: nil)
  }

  private func update() {
    if search.items.isEmpty {
      separatorView.isHidden = true
      placeholderLabel.isHidden = false
      if search.text.isEmpty {
        placeholderLabel.text = "Введите запрос для поиска кандидата"
      } else {
        placeholderLabel.attributedText = {
          let text = NSMutableAttributedString()
          text += "По запросу ".attributed(with: .sysFont(17, .regular))
          text += search!.text.attributed(with: .sysFont(17, .bold))
          text += " ничего не найдено".attributed(with: .sysFont(17, .regular))
          return text
        }()
      }
    } else {
      separatorView.isHidden = false
      placeholderLabel.isHidden = true
    }
  }

}

extension VacancyCandidateSearchController : UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return search.items.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "item", for: indexPath) as! VacancyCandidateCell
    cell.highlighter = search
    cell.candidate = search!.items[indexPath.row]
    cell.handler = { [unowned self] candidate, _ in
      guard !candidate.isSelected else { return }
      self.resultSubject.onNext(candidate)
    }
    return cell
  }

}

extension VacancyCandidateSearchController : UITableViewDelegate {

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
  }

  func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    cellHeight[indexPath] = cell.frame.height
  }

  func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    if let height = cellHeight[indexPath] {
      return height
    }
    return tableView.estimatedRowHeight
  }

  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    guard !search!.isLoading && !search!.ended else { return }
    let maxOffset = scrollView.contentSize.height - scrollView.frame.height
    if scrollView.contentOffset.y >= maxOffset {
      search?.loadMore()
    }
  }
  
}

extension VacancyCandidateSearchController : UISearchBarDelegate {

  func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    if text.characters.count > 1 {
      return false
    }
    if text.hasPrefix(" ") && (searchBar.text!.isEmpty || searchBar.text!.hasSuffix(" ")) {
      return false
    }
    return true
  }

}

final class VacancyCandidateSearch : SearchHighligter {

  typealias LoadingResult = Result<CountableRange<Int>>

  private(set) var items: [VacancyCandidate] = []
  let position: Position

  let loadingResultSubject = PublishSubject<LoadingResult>()
  var loadingResult: Observable<LoadingResult> {
    return loadingResultSubject.asObservable()
  }

  private var request: Request?
  private var requestVersion: Int = 0
  private var currentVersion: Int = 0
  private var actualVersion: Int = 1

  var isNeedReload: Bool {
    return actualVersion != currentVersion
  }

  var isLoading: Bool {
    return request != nil
  }

  private(set) var ended: Bool = false

  var text: String = "" {
    didSet {
      guard text != oldValue else { return }
      actualVersion += 1
      loadMore()
      let tokens = text.components(separatedBy: " ").map({ $0.trimmingCharacters(in: .whitespacesAndNewlines) }).filter({ !$0.isEmpty })
      guard !tokens.isEmpty else {
        hlRegEx = nil
        return
      }
      let pattern = "(?:^|\\s)(" + tokens.joined(separator: "|") + ")"
      hlRegEx = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
      print(pattern)
    }
  }

  private var hlRegEx: NSRegularExpression?

  init(position: Position) {
    self.position = position
  }

  func loadMore() {
    let reload = requestVersion != actualVersion
    if reload && request != nil {
      request?.cancel()
      request = nil
    }

    guard !text.isEmpty else {
      currentVersion = actualVersion
      requestVersion = actualVersion
      items.removeAll()
      loadingResultSubject.onNext(.success(0 ..< 0))
      return
    }

    let ver = actualVersion
    requestVersion = ver
    let offset: Int = reload ? 0 : items.count
    request = Client.instance.searchCandidates(for: position, text: text, offset: offset, limit: 20) { [weak self] result in
      guard let `self` = self, self.requestVersion == ver else { return }
      self.request = nil
      switch result {
      case .success(let items):
        self.currentVersion = ver
        if reload {
          self.items.removeAll()
        }
        let start = self.items.count
        self.items += items
        self.ended = items.count < 20
        self.loadingResultSubject.onNext(.success(start ..< self.items.count))
      case .failure(let error):
        self.loadingResultSubject.onError(error)
      }
    }
  }

  func highlight(text: NSMutableAttributedString) {
    guard let re = hlRegEx else { return }
    re.enumerateMatches(in: text.string, options: [], range: NSRange(location: 0, length: text.length)) { result, flags, _ in
      guard let result = result else { return }
      text.addAttributes([NSForegroundColorAttributeName: UIColor(rgba: 0x2073d6ff)], range: result.rangeAt(1))
    }
  }

}
