//
//  VacancyController.swift
//  Employees
//
//  Created by Sergey Erokhin on 27.04.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import RxSwift
import KVNProgress

final class VacancyController : SearchBaseController {

  enum Section {
    case info
    case candidatesHeader
    case candidates(Int)
    case candidatesPlaceholder(String)
  }

  typealias CandidatesGroup = (name: String, candidates: [VacancyCandidate])

  enum InfoItem {
    case root
    case tasks
  }

  var positionId: Int = 0

  fileprivate var position: Position?
  fileprivate var sections: [Section] = []
  fileprivate var candidateGroups: [CandidatesGroup] = []
  fileprivate var selectedCandidates: [VacancyCandidate] = [] {
    didSet {
      selectedCandidatesPlaceholderView.isHidden = !selectedCandidates.isEmpty
    }
  }
  fileprivate var infoItems: [InfoItem] = []

  @IBOutlet fileprivate var mainTableView: UITableView!
  @IBOutlet private var activityIndicatorView: UIActivityIndicatorView!
  @IBOutlet private var addButton: UIButton!
  @IBOutlet fileprivate var selectedCandidatesTableView: UITableView!
  @IBOutlet private var selectedCandidatesPlaceholderView: UIView!

  override func viewDidLoad() {
    super.viewDidLoad()
    mainTableView.rowHeight = UITableViewAutomaticDimension
    mainTableView.estimatedRowHeight = 100
    mainTableView.superview?.isHidden = true
    selectedCandidatesTableView.rowHeight = UITableViewAutomaticDimension
    selectedCandidatesTableView.estimatedRowHeight = 200
    selectedCandidatesTableView.backgroundView = selectedCandidatesPlaceholderView
    selectedCandidatesTableView.tableHeaderView = nil
    addButton.layer.borderColor = UIColor.gubSeparator.cgColor
    load()
  }

  private func load() {
    var tmpPosition: Position?
    Client.instance.getPosition(id: positionId).flatMap({ position -> Observable<[VacancyCandidate]> in
      tmpPosition = position
      return Client.instance.getCandidates(for: position)
    }).subscribe(onNext: { [weak self] candidates in
      guard let `self` = self else { return }
      self.position = tmpPosition
      var tmp: [VacancyCandidate] = []
      tmp = candidates.filter({ $0.flags.contains(.bossApproval) })
      if !tmp.isEmpty {
        self.candidateGroups.append((name: "На согласовании у руководителя", candidates: tmp))
      }
      tmp = candidates.filter({ $0.flags.contains(.federalReserve) })
      if !tmp.isEmpty {
        self.candidateGroups.append((name: "Федеральный резерв управленческих кадров", candidates: tmp))
      }
      tmp = candidates.filter({ $0.flags.contains(.staffAgency) })
      if !tmp.isEmpty {
        self.candidateGroups.append((name: "Кадровые агентства", candidates: tmp))
      }
      tmp = candidates.filter({ $0.flags.contains(.ai) })
      if !tmp.isEmpty {
        self.candidateGroups.append((name: "Подобранные искусственным интеллектом", candidates: tmp))
      }
      self.selectedCandidates = candidates.filter({ $0.isSelected })
      self.loadingCompleted()
    }, onError: { [weak self] error in
      print(error)
      DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
        self?.load()
      }
    }).addDisposableTo(disposeBag)
  }

  private func loadingCompleted() {
    sections.append(.info)
    infoItems.append(.root)
    if let info = position?.vacancyInfo {
      if !info.tasks.isEmpty {
        infoItems.append(.tasks)
      }
    }
    sections.append(.candidatesHeader)
    if candidateGroups.isEmpty {
      sections.append(.candidatesPlaceholder("Пока нет подходящих кандидатов"))
    } else {
      for i in 0 ..< candidateGroups.count {
        sections.append(.candidates(i))
      }
    }
    activityIndicatorView.stopAnimating()
    mainTableView.superview?.isHidden = false
    mainTableView.reloadData()
    selectedCandidatesTableView.reloadData()
  }

  @IBAction func addNewCandidate() {
    let searchVC = storyboard!.instantiateViewController(withIdentifier: "candidateSearch") as! VacancyCandidateSearchController
    searchVC.position = position!
    let nc = ModalNavigationController(rootViewController: searchVC)
    nc.modalPresentationStyle = .formSheet
    present(nc, animated: true)
    searchVC.result.subscribe(onNext: { [unowned self] candidate in
      self.addCandidate(candidate) {
        searchVC.refresh()
      }
    }).addDisposableTo(disposeBag)
  }

  fileprivate func addCandidate(_ candidate: VacancyCandidate, completion: (() -> Void)? = nil) {
    guard !candidate.isSelected else { return }
    KVNProgress.show()
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
      Client.instance.addCandidate(candidate).subscribe(onNext: { candidate in
        KVNProgress.dismiss()
        self.selectedCandidates.append(candidate)
        self.selectedCandidatesTableView.insertRows(at: [IndexPath(row: self.selectedCandidates.count - 1, section: 0)], with: .none)
        self.mainTableView.visibleCells.forEach {
          if let cell = $0 as? VacancyCandidateCell {
            if cell.candidate == candidate {
              cell.candidate.isSelected = true
            }
            cell.refresh()
          }
        }
        completion?()
      }, onError: { error in
        KVNProgress.showError(withStatus: error.localizedDescription)
      }).addDisposableTo(self.disposeBag)
    }
  }

}

// MARK: - UITableViewDataSource

extension VacancyController : UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    if tableView == mainTableView {
      return sections.count
    }
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tableView == mainTableView {
      switch sections[section] {
      case .info:
        return infoItems.count
      case .candidatesHeader, .candidatesPlaceholder(_):
        return 1
      case .candidates(let groupIndex):
        return candidateGroups[groupIndex].candidates.count + 1
      }
    } else {
      return selectedCandidates.count
    }
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if tableView == mainTableView {
      switch sections[indexPath.section] {
      case .info:
        switch infoItems[indexPath.row] {
        case .root:
          let cell = tableView.dequeueReusableCell(withIdentifier: "root", for: indexPath) as! VacancyRootCell
          cell.position = position
          return cell
        case .tasks:
          let cell = tableView.dequeueReusableCell(withIdentifier: "tasks", for: indexPath) as! VacancyTasksCell
          cell.tasksLabel.text = position?.vacancyInfo?.tasks
          return cell
        }
      case .candidatesHeader:
        return tableView.dequeueReusableCell(withIdentifier: "candidatesHeader", for: indexPath)
      case .candidates(let groupIndex):
        if indexPath.row == 0 {
          let cell = tableView.dequeueReusableCell(withIdentifier: "candidatesGroupHeader", for: indexPath) as! VacancyCandidatesGroupHeaderCell
          cell.headerLabel.text = candidateGroups[groupIndex].name
          let flags = candidateGroups[groupIndex].candidates[0].flags
          if flags.contains(.bossApproval) {
            cell.logoView.image = #imageLiteral(resourceName: "logo1")
          } else if flags.contains(.federalReserve) {
            cell.logoView.image = #imageLiteral(resourceName: "logo2")
          } else if flags.contains(.staffAgency) {
            cell.logoView.image = #imageLiteral(resourceName: "logo3")
          } else {
            cell.logoView.image = #imageLiteral(resourceName: "logo4")
          }
          return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "candidate", for: indexPath) as! VacancyCandidateCell
        cell.candidate = candidateGroups[groupIndex].candidates[indexPath.row - 1]
        cell.handler = { [unowned self] candidate, _ in
          self.addCandidate(candidate)
        }
        return cell
      case .candidatesPlaceholder(let text):
        let cell = tableView.dequeueReusableCell(withIdentifier: "candidatesPlaceholder", for: indexPath) as! VacancyCandidatesPlaceholderCell
        cell.placeholderLabel.text = text
        return cell
      }
    } else {
      let cell = tableView.dequeueReusableCell(withIdentifier: "candidate", for: indexPath) as! VacancyCandidateCell
      cell.candidate = selectedCandidates[indexPath.row]
      cell.handler = { [unowned self] candidate, _ in
        KVNProgress.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
          Client.instance.deleteCandidate(candidate).subscribe(onNext: { candidate in
            KVNProgress.dismiss()
            if let index = self.selectedCandidates.index(of: candidate) {
              self.selectedCandidates.remove(at: index)
              self.selectedCandidatesTableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .none)
              if !candidate.flags.isEmpty {
                self.mainTableView.visibleCells.forEach {
                  if let cell = $0 as? VacancyCandidateCell {
                    if cell.candidate == candidate {
                      cell.candidate.isSelected = false
                    }
                    cell.refresh()
                  }
                }
              }
            }
          }, onError: { error in
            KVNProgress.showError(withStatus: error.localizedDescription)
          }).addDisposableTo(self.disposeBag)
        }
      }
      return cell
    }
  }

}

// MARK: - UITableViewDelegate

extension VacancyController : UITableViewDelegate {

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    if let cell = tableView.cellForRow(at: indexPath) as? VacancyCandidateCell {
      let candidate = cell.candidate
      if let positionId = candidate?.candidate?.id {
        let vc = storyboard!.instantiateViewController(withIdentifier: "employee") as! EmployeeController
        vc.positionId = positionId
        navigationController?.pushViewController(vc, animated: true)
      } else if let personId = candidate?.person?.id {
        let vc = storyboard!.instantiateViewController(withIdentifier: "employee") as! EmployeeController
        vc.personId = personId
        navigationController?.pushViewController(vc, animated: true)
      }
    }
  }

}

final class VacancyCandidatesGroupHeaderCell : UITableViewCell {

  @IBOutlet private(set) var logoView: UIImageView!
  @IBOutlet private(set) var headerLabel: UILabel!

}

final class VacancyCandidatesPlaceholderCell : UITableViewCell {

  @IBOutlet private(set) var placeholderLabel: UILabel!

}
