//
//  VacancyRootCell.swift
//  Employees
//
//  Created by Sergey Erokhin on 28.04.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import Charts

final class VacancyRootCell: UITableViewCell {

  @IBOutlet private var dateLabel: UILabel!
  @IBOutlet private var nameLabel: UILabel!
  @IBOutlet private var divisionLabel: UILabel!
  @IBOutlet private var chartView: RadarChartView!

  var position: Position! {
    didSet {
      let df = DateFormatter()
      df.dateFormat = "dd.MM.yyyy"
      dateLabel.text = "Вакансия открыта " + df.string(from: Date())
      nameLabel.text = position.name
      let headDivisionName = position.headDivision?.name ?? ""
      if let divisionName = position.division?.name {
        if !headDivisionName.isEmpty && divisionName != headDivisionName {
          divisionLabel.text = headDivisionName + "\n" + divisionName
        } else {
          divisionLabel.text = divisionName
        }
        divisionLabel.isHidden = false
      } else {
        divisionLabel.isHidden = true
      }
    }
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    setupChartView()
  }

  private func setupChartView() {
    let set1 = RadarChartDataSet(values: (0 ..< 9).map({ _ in RadarChartDataEntry(value: Double(arc4random_uniform(4) + 6)) }), label: nil)
    set1.fillAlpha = 0.7
    set1.lineWidth = 2
    set1.drawFilledEnabled = true
    set1.setColor(UIColor(rgba: 0x51ae83ff))
    set1.fillColor = set1.color(atIndex: 0)

    let data = RadarChartData(dataSet: set1)
    data.setDrawValues(false)
    data.highlightEnabled = false

    chartView.data = data

    for axis in [chartView.xAxis, chartView.yAxis] {
      axis.axisLineWidth = 1
      axis.gridLineWidth = 1
      axis.gridColor = .gubGrid
      axis.drawLabelsEnabled = false
      axis.axisMinimum = 0
      axis.labelCount = 5
    }
    chartView.legend.enabled = false
    chartView.webLineWidth = 1
    chartView.webColor = .gubGrid
    chartView.chartDescription?.enabled = false
  }

}
