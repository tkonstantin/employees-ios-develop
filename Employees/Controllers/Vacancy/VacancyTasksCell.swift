//
//  VacancyTasksCell.swift
//  Employees
//
//  Created by Sergey Erokhin on 28.04.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

final class VacancyTasksCell : UITableViewCell {

  @IBOutlet private(set) var tasksLabel: UILabel!

}
