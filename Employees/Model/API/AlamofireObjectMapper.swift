//
//  AlamofireObjectMapper.swift
//  Employees
//
//  Created by Sergey Erokhin on 23/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import Alamofire
import ObjectMapper

extension DataRequest {

  public static func objectMapperSerializer<T: Mappable>(_ keyPath: String?, mapToObject object: T? = nil) -> DataResponseSerializer<T> {
    return DataResponseSerializer { request, response, data, error in
      guard error == nil else {
        return .failure(error!)
      }

      guard data != nil else {
        return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
      }

      let jsonResponseSerializer = DataRequest.jsonResponseSerializer(options: .allowFragments)
      let result = jsonResponseSerializer.serializeResponse(request, response, data, error)

      let JSONToMap: Any?
      if let keyPath = keyPath, keyPath.isEmpty == false {
        JSONToMap = (result.value as AnyObject?)?.value(forKeyPath: keyPath)
      } else {
        JSONToMap = result.value
      }

      if let object = object {
        _ = Mapper<T>().map(JSONObject: JSONToMap, toObject: object)
        return .success(object)
      } else if let parsedObject = Mapper<T>().map(JSONObject: JSONToMap) {
        return .success(parsedObject)
      }

      return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
    }
  }

  @discardableResult
  public func responseObject<T: Mappable>(queue: DispatchQueue? = nil, keyPath: String? = nil, mapToObject object: T? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
    return response(queue: queue, responseSerializer: DataRequest.objectMapperSerializer(keyPath, mapToObject: object), completionHandler: completionHandler)
  }

  public static func objectMapperArraySerializer<T: Mappable>(_ keyPath: String?) -> DataResponseSerializer<[T]> {
    return DataResponseSerializer { request, response, data, error in
      guard error == nil else {
        return .failure(error!)
      }

      guard data != nil else {
        return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
      }

      let JSONResponseSerializer = DataRequest.jsonResponseSerializer(options: .allowFragments)
      let result = JSONResponseSerializer.serializeResponse(request, response, data, error)

      let JSONToMap: Any?
      if let keyPath = keyPath , keyPath.isEmpty == false {
        JSONToMap = (result.value as AnyObject?)?.value(forKeyPath: keyPath)
      } else {
        JSONToMap = result.value
      }

      if let parsedObject = Mapper<T>().mapArray(JSONObject: JSONToMap){
        return .success(parsedObject)
      }

      return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
    }
  }

  @discardableResult
  public func responseArray<T: Mappable>(queue: DispatchQueue? = nil, keyPath: String? = nil, completionHandler: @escaping (DataResponse<[T]>) -> Void) -> Self {
    return response(queue: queue, responseSerializer: DataRequest.objectMapperArraySerializer(keyPath), completionHandler: completionHandler)
  }
}
