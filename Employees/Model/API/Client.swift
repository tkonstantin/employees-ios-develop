//
//  Client.swift
//  Employees
//
//  Created by Sergey Erokhin on 20/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import Alamofire
import RxCocoa
import RxSwift
import ObjectMapper

enum API {
  case createOrder
  case orders
  case moveOrders
  case setOrderStatus(Int)
  case person(Int)
  case subordinatePositions(Int)
  case kinships(Int)
  case childNodes(Int)
  case rootNode
  case search
  case position(Int)
  case statistic
  case numVacanciesOGV
  case numVacanciesCategories
  case vacanciesOGV(Int)
  case vacanciesCategory(Int)
  case vacancies
  case candidates(Int)
  case candidate(Int, Int)
  case searchCandidates(Int)

  var path: String {
    switch self {
    case .person(let id):
      return "person/\(id)"
    case .position(let id):
      return "position/\(id)"
    case .candidates(let id):
      return "position/\(id)/candidates"
    case .subordinatePositions(let id):
      return "position/\(id)/subordinatePositions"
    case .kinships(let id):
      return "person/\(id)/kinships"
    case .rootNode:
      return "node/root"
    case .childNodes(let id):
      return "node/\(id)/childs"
    case .search:
      return "search"
    case .statistic:
      return "statistic"
    case .createOrder:
      return "order"
    case .orders:
      return "orders"
    case .setOrderStatus(let id):
      return "order/\(id)/status"
    case .moveOrders:
      return "orders/update"
    case .numVacanciesOGV:
      return "vacancies/ogv"
    case .vacanciesOGV(let id):
      return "vacancies/ogv/\(id)"
    case .numVacanciesCategories:
      return "vacancies/categories"
    case .vacanciesCategory(let id):
      return "vacancies/categories/\(id)"
    case .vacancies:
      return "vacancies"
    case .candidate(let positionId, let personId):
      return "position/\(positionId)/candidate/\(personId)"
    case .searchCandidates(let positionId):
      return "position/\(positionId)/searchCandidates"
    }
  }
}

typealias Request = Alamofire.Request
typealias Result = Alamofire.Result
typealias HTTPHeaders = Alamofire.HTTPHeaders

final class Client {

  static let instance = Client()

  private var sessionMgr: SessionManager
  private let apiURLPrefix = "guber://api.mosreg.ru/"

  private init() {
    sessionMgr = {
      let config = URLSessionConfiguration.default
      config.protocolClasses = [LocalProtocol.self]
      return SessionManager(configuration: config)
    }()
  }

  func getURL(_ method: API) -> String {
    return apiURLPrefix + method.path
  }

  func getPerson(id: Int) -> Observable<Person> {
    return Observable.create { observer in
      let request = self.sessionMgr.request(self.getURL(.person(id)), method: .get)
      request.responseObject { (response: DataResponse<Person>) in
        switch response.result {
        case .success(let person):
          observer.onNext(person)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func getPosition(id: Int) -> Observable<Position> {
    return Observable.create { observer in
      let request = self.sessionMgr.request(self.getURL(.position(id)), method: .get)
      request.responseObject { (response: DataResponse<Position>) in
        switch response.result {
        case .success(let position):
          observer.onNext(position)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func getSubordinatePositions(id: Int) -> Observable<[Position]> {
    return Observable.create { observer in
      let request = self.sessionMgr.request(self.getURL(.subordinatePositions(id)), method: .get)
      request.responseArray { (response: DataResponse<[Position]>) in
        switch response.result {
        case .success(let positions):
          observer.onNext(positions)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func getRootNode() -> Observable<GraphNode> {
    return Observable.create { observer in
      let request = self.sessionMgr.request(self.getURL(.rootNode), method: .get)
      request.responseObject { (response: DataResponse<GraphNode>) in
        switch response.result {
        case .success(let node):
          observer.onNext(node)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func getChildNodes(parentId: Int) -> Observable<[GraphNode]> {
    return Observable.create { observer in
      let request = self.sessionMgr.request(self.getURL(.childNodes(parentId)), method: .get)
      request.responseArray { (response: DataResponse<[GraphNode]>) in
        switch response.result {
        case .success(let nodes):
          observer.onNext(nodes)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func getKinships(personId: Int) -> Observable<[Kinship]> {
    return Observable.create { observer in
      let request = self.sessionMgr.request(self.getURL(.kinships(personId)), method: .get)
      request.responseArray { (response: DataResponse<[Kinship]>) in
        switch response.result {
        case .success(let kinships):
          observer.onNext(kinships)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func search(text: String, offset: Int, limit: Int, completion: @escaping (Result<[SearchItem]>) -> Void) -> Request {
    let params: Parameters = [
      "text": text,
      "offset": offset,
      "limit": limit
    ]
    let request = sessionMgr.request(getURL(.search), method: .get, parameters: params)
    request.responseArray { (response: DataResponse<[SearchItem]>) in
      switch response.result {
      case .success(let items):
        completion(.success(items))
      case .failure(let error):
        completion(.failure(error))
      }
    }
    return request
  }

  func search(text: String, offset: Int, limit: Int) -> Observable<[SearchItem]> {
    return Observable.create { observer in
      let request = self.search(text: text, offset: offset, limit: limit) { result in
        switch result {
        case .success(let items):
          observer.onNext(items)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func getStatistic() -> Observable<Statistic> {
    return Observable.create { observer in
      let request = self.sessionMgr.request(self.getURL(.statistic), method: .get)
      request.responseObject { (response: DataResponse<Statistic>) in
        switch response.result {
        case .success(let stat):
          observer.onNext(stat)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func getOrders() -> Observable<[Order]> {
    let request = sessionMgr.request(getURL(.orders), method: .get)
    return Observable.create { observer in
      request.responseArray { (response: DataResponse<[Order]>) in
        switch response.result {
        case .success(let orders):
          observer.onNext(orders)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func createOrder(_ type: OrderType, position: Position) -> Observable<Order> {
    let params: Parameters = [
      "type": type.rawValue,
      "position_id": position.id
    ]
    let request = sessionMgr.request(getURL(.createOrder), method: .post, parameters: params)
    return Observable.create { observer in
      request.responseObject { (response: DataResponse<Order>) in
        switch response.result {
        case .success(let order):
          observer.onNext(order)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func move(_ order: Order, to: OrderStatus) -> Observable<Order> {
    let params: Parameters = [
      "new_status": to.rawValue
    ]
    let request = sessionMgr.request(getURL(.setOrderStatus(order.id)), method: .post, parameters: params)
    return Observable.create { observer in
      request.responseObject { (response: DataResponse<Order>) in
        switch response.result {
        case .success(let order):
          observer.onNext(order)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func moveAllOrders(from: OrderStatus, to: OrderStatus) -> Observable<[Order]> {
    let params: Parameters = [
      "old_status": from.rawValue,
      "new_status": to.rawValue
    ]
    let request = sessionMgr.request(getURL(.moveOrders), method: .post, parameters: params)
    return Observable.create { observer in
      request.responseArray { (response: DataResponse<[Order]>) in
        switch response.result {
        case .success(let orders):
          observer.onNext(orders)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func getNumVacanciesForOGV() -> Observable<[Division]> {
    let request = sessionMgr.request(getURL(.numVacanciesOGV), method: .get)
    return Observable.create { observer in
      request.responseArray { (response: DataResponse<[Division]>) in
        switch response.result {
        case .success(let divisions):
          observer.onNext(divisions)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func getNumVacanciesForCategories() -> Observable<[PositionCategory]> {
    let request = sessionMgr.request(getURL(.numVacanciesCategories), method: .get)
    return Observable.create { observer in
      request.responseArray { (response: DataResponse<[PositionCategory]>) in
        switch response.result {
        case .success(let categories):
          observer.onNext(categories)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func getVacancies(for division: Division) -> Observable<[Position]> {
    let request = sessionMgr.request(getURL(.vacanciesOGV(division.id)), method: .get)
    return Observable.create { observer in
      request.responseArray { (response: DataResponse<[Position]>) in
        switch response.result {
        case .success(let vacancies):
          observer.onNext(vacancies)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func getAllVacancies() -> Observable<[Position]> {
    let request = sessionMgr.request(getURL(.vacancies), method: .get)
    return Observable.create { observer in
      request.responseArray { (response: DataResponse<[Position]>) in
        switch response.result {
        case .success(let vacancies):
          observer.onNext(vacancies)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func getVacancies(for category: PositionCategory) -> Observable<[Position]> {
    let request = sessionMgr.request(getURL(.vacanciesCategory(category.id)), method: .get)
    return Observable.create { observer in
      request.responseArray { (response: DataResponse<[Position]>) in
        switch response.result {
        case .success(let vacancies):
          observer.onNext(vacancies)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }

  }

  func getCandidates(for position: Position, fake: Bool = false) -> Observable<[VacancyCandidate]> {
    let params: Parameters = [
      "fake": fake
    ]
    let request = sessionMgr.request(getURL(.candidates(position.id)), method: .get, parameters: params)
    return Observable.create { observer in
      request.responseArray { (response: DataResponse<[VacancyCandidate]>) in
        switch response.result {
        case .success(let candidates):
          observer.onNext(candidates)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func addCandidate(_ candidate: VacancyCandidate) -> Observable<VacancyCandidate> {
    let request = sessionMgr.request(getURL(.candidate(candidate.positionId, candidate.person!.id)), method: .post)
    return Observable.create { observer in
      request.responseObject { (response: DataResponse<Empty>) in
        switch response.result {
        case .success(_):
          candidate.isSelected = true
          observer.onNext(candidate)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func deleteCandidate(_ candidate: VacancyCandidate) -> Observable<VacancyCandidate> {
    let request = sessionMgr.request(getURL(.candidate(candidate.positionId, candidate.person!.id)), method: .delete)
    return Observable.create { observer in
      request.responseObject { (response: DataResponse<Empty>) in
        switch response.result {
        case .success(_):
          candidate.isSelected = false
          observer.onNext(candidate)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }

  func searchCandidates(for position: Position, text: String, offset: Int, limit: Int, completion: @escaping (Result<[VacancyCandidate]>) -> Void) -> Request {
    let params: Parameters = [
      "text": text,
      "offset": offset,
      "limit": limit
    ]
    let request = sessionMgr.request(getURL(.searchCandidates(position.id)), method: .get, parameters: params)
    request.responseArray { (response: DataResponse<[VacancyCandidate]>) in
      switch response.result {
      case .success(let candidates):
        completion(.success(candidates))
      case .failure(let error):
        completion(.failure(error))
      }
    }
    return request
  }
}

struct Empty : Mappable {
  init?(map: Map) {}
  mutating func mapping(map: Map) {}
}
