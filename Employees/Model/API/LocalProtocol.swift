//
//  LocalProtocol.swift
//  Employees
//
//  Created by Sergey Erokhin on 20/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import Foundation
import ObjectMapper

final class LocalProtocol : URLProtocol {

  private var token: CancelationToken?

  override class func canInit(with request: URLRequest) -> Bool {
    return request.url?.scheme == "guber"
  }

  override class func canonicalRequest(for request: URLRequest) -> URLRequest {
    return request
  }

  override var cachedResponse: CachedURLResponse? {
    return URLCache.shared.cachedResponse(for: request)
  }

  override func startLoading() {

    let path = request.url!.path

    if !path.hasPrefix("/order"), !path.contains("/candidate"), !path.contains("/searchCandidates"), let cachedResponse = self.cachedResponse {
      client?.urlProtocol(self, cachedResponseIsValid: cachedResponse)
      return
    }

    let components = path.components(separatedBy: "/").filter({ !$0.isEmpty })

    switch components[0] {
    case "node":
      if components.count == 3 && components.last == "childs" {
        let id = Int(components[1])!
        token = LocalServer.instance.getChildNodes(id: id) { result in
          self.complete(result)
        }
      } else if components.count == 2 && components.last == "root" {
        token = LocalServer.instance.getRootNode { result in
          self.complete(result)
        }
      } else {
        fatalError()
      }
    case "person":
      if components.count == 3 && components.last == "kinships" {
        let id: Int = Int(components[1]) ?? 0
        token = LocalServer.instance.getKinships(personId: id) { result in
          self.complete(result)
        }
      } else if components.count == 2 {
        let id: Int = Int(components.last!) ?? 0
        token = LocalServer.instance.getPerson(id: id) { result in
          self.complete(result)
        }
      } else {
        fatalError()
      }
    case "search":
      let params = parseQueryParams(query: request.url?.query ?? "")
      token = LocalServer.instance.search(text: params["text"] ?? "", offset: Int(params["offset"] ?? "") ?? 0, limit: Int(params["limit"] ?? "") ?? 20) { result in
        self.complete(result)
      }
    case "position":
      if components.count == 2 {
        let id = Int(components[1])!
        token = LocalServer.instance.getPosition(id: id) { result in
          self.complete(result)
        }
      } else if components.count == 3 {
        let id = Int(components[1])!
        switch components[2] {
        case "subordinatePositions":
          token = LocalServer.instance.getSubordinatePositions(positionId: id) { result in
            self.complete(result)
          }
        case "candidates":
          let fake = request.getParams()["fake"] ?? "" == "1"
          token = LocalServer.instance.getCandidates(positionId: id, fake: fake) { result in
            self.complete(result)
          }
        case "searchCandidates":
          let params = request.getParams()
          let text = params["text"] ?? ""
          let offset = Int(params["offset"] ?? "0")!
          let limit = Int(params["limit"] ?? "20")!
          token = LocalServer.instance.searchCandidates(positionId: id, text: text, offset: offset, limit: limit) { result in
            self.complete(result)
          }
        default:
          fatalError()
        }
      } else if components.count == 4 {
        let positionId = Int(components[1])!
        let personId = Int(components[3])!
        if request.httpMethod == "POST" {
          token = LocalServer.instance.addCandidate(positionId: positionId, personId: personId) { result in
            self.complete(result)
          }
        } else {
          token = LocalServer.instance.deleteCandidate(positionId: positionId, personId: personId) { result in
            self.complete(result)
          }
        }
      } else {
        fatalError()
      }
    case "statistic":
      if components.count == 1 {
        token = LocalServer.instance.getStatistic() { result in
          self.complete(result)
        }
      } else {
        fatalError()
      }
    case "order":
      if components.count == 1 {
        let params = request.getParams()
        let type = OrderType(rawValue: Int(params["type"] ?? "") ?? 0)!
        let positionId = Int(params["position_id"] ?? "") ?? 0
        token = LocalServer.instance.createOrder(type, positionId: positionId) { result in
          self.complete(result)
        }
      } else if components.count == 3 {
        if components.last == "status" {
          let params = request.getParams()
          let newStatus = OrderStatus(rawValue: Int(params["new_status"] ?? "") ?? 0)!
          let orderId = Int(components[1]) ?? 0
          token = LocalServer.instance.setOrderStatus(orderId: orderId, newStatus: newStatus) { result in
            self.complete(result)
          }
        }
      } else {
        fatalError()
      }
    case "orders":
      if components.count == 1 {
        token = LocalServer.instance.getOrders() { result in
          self.complete(result)
        }
      } else if components.count == 2 {
        if components.last == "update" {
          let params = request.getParams()
          let oldStatus = OrderStatus(rawValue: Int(params["old_status"] ?? "") ?? 0)!
          let newStatus = OrderStatus(rawValue: Int(params["new_status"] ?? "") ?? 0)!
          token = LocalServer.instance.updateOrders(oldStatus: oldStatus, newStatus: newStatus) { result in
            self.complete(result)
          }
        } else {
          fatalError()
        }
      }
    case "vacancies":
      if components.count == 1 {
        token = LocalServer.instance.getAllVacancies { result in
          self.complete(result)
        }
      } else if components.count == 2 {
        switch components.last! {
        case "ogv":
          token = LocalServer.instance.getNumVacanciesForOGV { result in
            self.complete(result)
          }
        case "categories":
          token = LocalServer.instance.getNumVacanciesForCategories { result in
            self.complete(result)
          }
        default:
          fatalError()
        }
      } else if components.count == 3 {
        switch components[1] {
        case "ogv":
          token = LocalServer.instance.getVacancies(divisionId: Int(components.last!)!) { result in
            self.complete(result)
          }
        case "categories":
          token = LocalServer.instance.getVacancies(categoryId: Int(components.last!)!) { result in
            self.complete(result)
          }
        default:
          fatalError()
        }
      }
    default:
      fatalError()
    }
  }

  private func complete(_ result: Result<Void>) {
    switch result {
    case .success:
      complete(statusCode: 200, data: "{}".data(using: .utf8))
    case .failure(let error):
      client?.urlProtocol(self, didFailWithError: error)
    }
  }

  private func complete<T : Mappable>(_ result: Result<T>) {
    switch result {
    case .success(let obj):
      let data = Mapper<T>().toJSONString(obj)?.data(using: .utf8)
      complete(statusCode: 200, data: data)
    case .failure(let error):
      client?.urlProtocol(self, didFailWithError: error)
    }
  }

  private func complete<T : Mappable>(_ result: Result<[T]>) {
    switch result {
    case .success(let obj):
      let data = Mapper<T>().toJSONString(obj)?.data(using: .utf8)
      complete(statusCode: 200, data: data)
    case .failure(let error):
      client?.urlProtocol(self, didFailWithError: error)
    }
  }

  private func complete(statusCode: Int, data: Data? = nil, cache: Bool = true) {
    let data = data ?? Data()
    let headers: HTTPHeaders = [
      "Content-Type": "application/json; charset=utf-8",
    ]
    let response = HTTPURLResponse(url: request.url!, statusCode: statusCode, httpVersion: "1.1", headerFields: headers)!
    client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: cache ? .allowedInMemoryOnly : .notAllowed)
    client?.urlProtocol(self, didLoad: data)
    client?.urlProtocolDidFinishLoading(self)
  }

  override func stopLoading() {
    token?.cancel()
  }

}

func parseQueryParams(query: String) -> [String : String] {
  var result: [String : String] = [:]
  for kv in query.components(separatedBy: "&") {
    let tmp = kv.components(separatedBy: "=")
    guard tmp.count == 2 else { continue }
    result[tmp[0]] = tmp[1].removingPercentEncoding
  }
  return result
}

extension URLRequest {

  func getParams() -> [String : String] {
    if httpMethod == "GET" {
      guard let query = url?.query, !query.isEmpty else {
        return [:]
      }
      return parseQueryParams(query: query)
    } else {
      let data = httpBody ?? httpBodyStream?.readAll() ?? Data()
      let str = String(data: data, encoding: .utf8)!
      return parseQueryParams(query: str)
    }
  }

}

extension InputStream {

  func readAll() -> Data {
    var buffer = [UInt8](repeating: 0, count: 1024)
    var out = Data()
    open()
    while hasBytesAvailable {
      let count = read(&buffer, maxLength: buffer.count)
      out.append(&buffer, count: count)
    }
    return out
  }

}
