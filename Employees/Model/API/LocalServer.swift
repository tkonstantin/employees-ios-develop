//
//  LocalServer.swift
//  Employees
//
//  Created by Sergey Erokhin on 20/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import Foundation
import SQLite


final class CancelationToken {
  private(set) var isCanceled: Bool = false

  func cancel() {
    isCanceled = true
  }
}

private let fakeCompetencies: [String] = [
  "Лидерство",
  "Работа в\nкоманде",
  "Тип\nдеятельности",
  "Эмоциональный\nинтеллект",
  "Управление\nрисками",
  "Отношение к\nкоррупции",
  "Профессиональные\nкомпетенции",
  "Факторы\nвыгорания"
]

final class LocalServer {

  let vacanciesCounters: [Counter] = [
    Counter(name: "Министретство строительства", value: 1048),
    Counter(name: "Министретство образования", value: 640),
    Counter(name: "Министерство здравоохранения", value: 320),
    Counter(name: "Министрество внутренних дел", value: 220),
    Counter(name: "Остальные", value: 1459)
  ]

  enum Error : Swift.Error {
    case internal_
    case notFound
  }

  static let instance = LocalServer()
  let dateFormatter: DateFormatter = {
    let df = DateFormatter()
    df.dateFormat = "yyyy-MM-dd"
    return df
  }()

  private var db: Connection!
  private var queue: DispatchQueue
  private(set) var dbURL: URL

  private init() {
    queue = DispatchQueue(label: "com.mosreg.employees.LocalServer", qos: .default)
    let fm = FileManager.default
    let documentsURL = try! fm.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
    dbURL = URL(fileURLWithPath: "data.sqlite", relativeTo: documentsURL)
    if !fm.fileExists(atPath: dbURL.path) {
      try! fm.copyItem(at: Bundle.main.url(forResource: "data", withExtension: "sqlite")!, to: dbURL)
    }
    try! restart()
  }

  func restart() throws {
    db = try Connection(dbURL.path, readonly: false)
  }

  func restoreLocalVersion() {
    let fm = FileManager.default
    try! fm.copyItem(at: Bundle.main.url(forResource: "data", withExtension: "sqlite")!, to: dbURL)
  }

  func getPerson(id: Int, completion: @escaping (Result<Person>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        if let person = try self.getPersonImpl(id: id) {
          completion(.success(person))
        } else {
          completion(.failure(Error.notFound))
        }
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func getPosition(id: Int, completion: @escaping (Result<Position>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        if let position = try self.getPositionImpl(id: id) {
          completion(.success(position))
        } else {
          completion(.failure(Error.notFound))
        }
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func getSubordinatePositions(positionId: Int, completion: @escaping (Result<[Position]>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        let positions = try self.getSubordinatePositionsImpl(positionId: positionId)
        completion(.success(positions))
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func getRootNode(completion: @escaping (Result<GraphNode>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        let nodes = try self.getChildNodesImpl(id: 0)
        completion(.success(nodes.first!))
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func getChildNodes(id: Int, completion: @escaping (Result<[GraphNode]>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        let nodes = try self.getChildNodesImpl(id: id)
        completion(.success(nodes))
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func getKinships(personId: Int, completion: @escaping (Result<[Kinship]>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        let kinships = try self.getKinshipsImpl(personId: personId)
        completion(.success(kinships))
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func getStatistic(completion: @escaping (Result<Statistic>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        let stat = try self.getStatisticImpl()
        completion(.success(stat))
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func search(text: String, offset: Int = 0, limit: Int = 20, completion: @escaping (Result<[SearchItem]>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        let items = try self.searchImpl(text: text, offset: offset, limit: limit)
        completion(.success(items))
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func createOrder(_ type: OrderType, positionId: Int, completion: @escaping (Result<Order>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        let order = try self.createOrderImpl(positionId: positionId, type: type)
        completion(.success(order))
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func setOrderStatus(orderId: Int, newStatus: OrderStatus, completion: @escaping (Result<Order>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        try self.setOrderStatusImpl(orderId: orderId, newStatus: newStatus)
        let order = try self.getOrderImpl(id: orderId)
        completion(.success(order))
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func updateOrders(oldStatus: OrderStatus, newStatus: OrderStatus, completion: @escaping (Result<[Order]>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        try self.updateOrdersImpl(oldStatus: oldStatus, newStatus: newStatus)
        let items = try self.getOrdersImpl()
        completion(.success(items))
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func getOrders(completion: @escaping (Result<[Order]>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        let items = try self.getOrdersImpl()
        completion(.success(items))
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func getNumVacanciesForOGV(completion: @escaping (Result<[Division]>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        let divisions = try self.getNumVacanciesForOGVImpl()
        completion(.success(divisions))
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func getNumVacanciesForCategories(completion: @escaping (Result<[PositionCategory]>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        let categories = try self.getNumVacanciesForCategoriesImpl()
        completion(.success(categories))
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func getCandidates(positionId: Int, fake: Bool, completion: @escaping (Result<[VacancyCandidate]>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        if !fake {
          let candidates = try self.getCandidatesImpl(positionId: positionId)
          completion(.success(candidates))
        } else {
          let candidates = try self.getFakeCandidatesImpl(positionId: positionId)
          completion(.success(candidates))
        }
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func searchCandidates(positionId: Int, text: String, offset: Int, limit: Int, completion: @escaping (Result<[VacancyCandidate]>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        let candidates = try self.searchCandidatesImpl(positionId: positionId, text: text, offset: offset, limit: limit)
        completion(.success(candidates))
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func addCandidate(positionId: Int, personId: Int, completion: @escaping (Result<Void>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        try self.addCandidateImpl(positionId: positionId, personId: personId)
        completion(.success())
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func deleteCandidate(positionId: Int, personId: Int, completion: @escaping (Result<Void>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        try self.deleteCandidateImpl(positionId: positionId, personId: personId)
        completion(.success())
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func getVacancies(divisionId: Int, completion: @escaping (Result<[Position]>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        let positions = try self.getVacanciesImpl(divisionId: divisionId)
        completion(.success(positions))
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func getVacancies(categoryId: Int, completion: @escaping (Result<[Position]>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        let positions = try self.getVacanciesImpl(categoryId: categoryId)
        completion(.success(positions))
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  func getAllVacancies(completion: @escaping (Result<[Position]>) -> Void) -> CancelationToken {
    let token = CancelationToken()
    queue.async {
      guard !token.isCanceled else { return }
      do {
        let positions = try self.getAllVacanciesImpl()
        completion(.success(positions))
      } catch let error {
        completion(.failure(error))
      }
    }
    return token
  }

  private func getPersonImpl(id: Int) throws -> Person? {
    let stmt = try db.prepare("SELECT id,first_name,last_name,middle_name,photo_name,sex,birth_date,start_working_date,cur_pos_date FROM persons WHERE id=(?)")
    if let data = stmt.bind(id).next() {
      let person = Person()
      person.id = Int(data[0] as! Int64)
      person.firstName = data[1] as? String
      person.lastName = data[2] as? String
      person.middleName = data[3] as? String
      person.photoFileName = data[4] as? String
      person.sex = Sex(rawValue: Int(data[5] as! Int64)) ?? .male
      person.birthDate = dateFormatter.date(from: data[6] as? String ?? "")
      person.startWorkingDate = dateFormatter.date(from: data[7] as? String ?? "")
      person.currentPositionDate = dateFormatter.date(from: data[8] as? String ?? "")
      person.contacts = try getContactsImpl(personId: id)
      person.ranks = try getRanks(personId: person.id)
      person.competencies = try getCompetenciesImpl(personId: id, positionId: 0)
      try fillDetailInfo(person)
      return person
    }
    return nil
  }

  private func fillDetailInfo(_ person: Person) throws {
    let stmt = try db.prepare("SELECT 1 AS type,name,id FROM achievements WHERE person_id=(:id) UNION SELECT 2 AS type,name,id FROM education WHERE person_id=(:id) UNION SELECT 3 AS type,name,id FROM experience WHERE person_id=(:id) UNION SELECT 4 AS type,name,id FROM seminars WHERE person_id=(:id) ORDER BY type, id")
    _ = stmt.bind([":id": person.id])
    for data in stmt {
      switch data[0] as! Int64 {
      case 1:
        person.achievements.append(data[1] as? String ?? "")
      case 2:
        person.education.append(data[1] as? String ?? "")
      case 3:
        person.experience.append(data[1] as? String ?? "")
      case 4:
        person.seminars.append(data[1] as? String ?? "")
      default:
        break
      }
    }
  }

  private func getContactsImpl(personId: Int) throws -> [Contact] {
    let stmt = try db.prepare("SELECT type,value FROM contacts WHERE person_id=(?) ORDER BY type")
    try stmt.run(personId)
    var result: [Contact] = []
    for data in stmt {
      let contact = Contact()
      contact.type = ContactType(rawValue: Int(data[0] as! Int64))!
      contact.value = data[1] as! String
      result.append(contact)
    }
    return result
  }

  private func getPositionImpl(id: Int) throws -> Position? {
    let stmt = try db.prepare("SELECT p.name,p.person_id,p.division_id,d.name,p.num_workplaces,p.num_vacancies,p.level,v.id,v.open_date,v.tasks,d2.id,d2.name,p.type_id FROM positions AS p LEFT JOIN divisions AS d ON p.division_id=d.id LEFT JOIN vacancies AS v ON p.id=v.position_id LEFT JOIN divisions AS d2 ON p.root_division_id=d2.id WHERE p.id=(?)")
    if let data = stmt.bind(id).next() {
      let position = Position()
      position.id = id
      position.name = data[0] as? String
      let personId = Int(data[1] as? Int64 ?? 0)
      if personId > 0 {
        position.person = try getPersonImpl(id: personId)
        position.competencies = try getCompetenciesImpl(personId: personId, positionId: id)
      }
      let divisionId = Int(data[2] as? Int64 ?? 0)
      if divisionId > 0 {
        let division = Division()
        division.id = divisionId
        division.name = data[3] as? String ?? ""
        position.division = division
      }
      position.numWorkplaces = Int(data[4] as? Int64 ?? 0)
      position.numVacancies = Int(data[5] as? Int64 ?? 0)
      position.level = Int(data[6] as? Int64 ?? 0)
      if let vacancyId = data[7] as? Int64, vacancyId > 0 {
        let info = VacancyInfo()
        info.openDate = dateFormatter.date(from: data[8] as? String ?? "")
        info.tasks = data[9] as? String ?? ""
        position.vacancyInfo = info
      }
      let headDivisionId = Int(data[10] as? Int64 ?? 0)
      if headDivisionId > 0 {
        let division = Division()
        division.id = headDivisionId
        division.name = data[11] as? String ?? ""
        position.headDivision = division
      }
      position.typeId = Int(data[12] as? Int64 ?? 0)
      return position
    }
    return nil
  }

  private func getSubordinatePositionsImpl(positionId: Int) throws -> [Position] {
    let stmt = try db.prepare("SELECT p.id,p.name,p.person_id,p.division_id,d.name,p.num_workplaces,p.num_vacancies,p2.first_name,p2.middle_name,p2.last_name,p2.sex,p2.photo_name FROM positions AS p LEFT JOIN divisions AS d ON p.division_id=d.id LEFT JOIN persons AS p2 ON p2.id = p.person_id WHERE p.parent_id=(?) ORDER BY p2.first_name, p2.middle_name, p2.first_name, p.name")
    try stmt.run(positionId)
    var result: [Position] = []
    for data in stmt {
      let position = Position()
      position.id = Int(data[0] as! Int64)
      position.name = data[1] as? String
      let personId = Int(data[2] as? Int64 ?? 0)
      if personId > 0 {
        let person = Person()
        position.person = person
        person.id = personId
        person.firstName = data[7] as? String
        person.middleName = data[8] as? String
        person.lastName = data[9] as? String
        person.sex = Sex(rawValue: Int(data[10] as? Int64 ?? 2))!
        person.photoFileName = data[11] as? String
      }
      let divisionId = Int(data[3] as? Int64 ?? 0)
      if divisionId > 0 {
        let division = Division()
        division.id = divisionId
        division.name = data[4] as? String ?? ""
        position.division = division
      }
      position.numWorkplaces = Int(data[5] as? Int64 ?? 0)
      position.numVacancies = Int(data[6] as? Int64 ?? 0)
      result.append(position)
    }
    return result
  }

  private func getKinshipsImpl(personId: Int) throws -> [Kinship] {
    let stmt = try db.prepare("SELECT pe.id, pe.first_name, pe.middle_name, pe.last_name, k.type, po.id, po.name, pe.sex, pe.photo_name, d.id, d.name, k.alert_level FROM kinships AS k, persons AS pe LEFT JOIN positions AS po ON po.person_id=pe.id LEFT JOIN divisions AS d ON d.id=po.root_division_id WHERE k.person_id1=(?) AND k.person_id2 = pe.id")
    try stmt.run(personId)
    var result: [Kinship] = []
    for data in stmt {
      let kinship = Kinship()
      let person = Person()
      kinship.type = KinshipType(rawValue: Int(data[4] as! Int64))!
      kinship.person = person
      person.id = Int(data[0] as! Int64)
      person.firstName = data[1] as? String
      person.middleName = data[2] as? String
      person.lastName = data[3] as? String
      if let posId = data[5] as? Int64 {
        let position = Position()
        position.id = Int(posId)
        position.name = data[6] as? String
        kinship.position = position
      }
      person.sex = Sex(rawValue: Int(data[7] as! Int64)) ?? .male
      person.photoFileName = data[8] as? String
      if let divisionId = data[9] as? Int64 {
        let division = Division()
        division.id = Int(divisionId)
        division.name = data[10] as? String ?? ""
        kinship.position?.division = division
      }
      kinship.alertLevel = Int(data[11] as? Int64 ?? 0)
      person.contacts = try getContactsImpl(personId: person.id)
      try fillDetailInfo(person)
      result.append(kinship)
    }
    return result
  }

  private func getChildNodesImpl(id: Int) throws -> [GraphNode] {
    let stmt = try db.prepare("SELECT n.id, n.name, n.child_count, p1.id, p1.name, p1.num_workplaces, p1.num_vacancies, p2.id, p2.first_name, p2.middle_name, p2.last_name, p2.photo_name, p2.sex, n.num_workplaces, n.num_vacancies FROM nodes AS n LEFT JOIN positions AS p1 ON n.position_id=p1.id LEFT JOIN persons AS p2 ON p1.person_id=p2.id WHERE n.parent_id=(?) AND n.hidden=0 ORDER BY p1.weight DESC")
    try stmt.run(id)
    var result: [GraphNode] = []
    for data in stmt {
      let node = GraphNode()
      node.id = Int(data[0] as! Int64)
      node.name = data[1] as? String ?? ""
      node.childCount = Int(data[2] as? Int64 ?? 0)
      let positionId = Int(data[3] as? Int64 ?? 0)
      if positionId > 0 {
        let position = Position()
        position.id = positionId
        position.name = data[4] as? String
        position.numWorkplaces = Int(data[5] as? Int64 ?? 0)
        position.numVacancies = Int(data[6] as? Int64 ?? 0)
        let personId = Int(data[7] as? Int64 ?? 0)
        if personId > 0 {
          let person = Person()
          person.id = personId
          person.firstName = data[8] as? String
          person.middleName = data[9] as? String
          person.lastName = data[10] as? String
          person.photoFileName = data[11] as? String
          person.sex = Sex(rawValue: Int(data[12] as? Int64 ?? 2))!
          position.person = person
        }
        node.position = position
        node.numWorkplaces = Int(data[13] as? Int64 ?? 0)
        node.numVacancies = Int(data[14] as? Int64 ?? 0)
      }
      result.append(node)
    }
    return result
  }

  private func getRanks(personId: Int) throws -> [Rank] {
    let stmt = try db.prepare("SELECT date,name FROM ranks,person_ranks WHERE person_id=(?) AND person_ranks.rank_id=ranks.id")
    try stmt.run(personId)
    var result: [Rank] = []
    for data in stmt {
      let rank = Rank()
      rank.date = dateFormatter.date(from: data[0] as? String ?? "")
      rank.name = data[1] as? String ?? ""
      result.append(rank)
    }
    return result
  }

  private func searchImpl(text: String, offset: Int = 0, limit: Int = 100) throws -> [SearchItem] {
    let text = text.components(separatedBy: " ").filter({ !$0.isEmpty }).joined(separator: "* ") + "*"
    let stmt = try db.prepare("SELECT docid, s.position_name, s.division_name, p.id, p.sex, p.photo_name, p.first_name, p.middle_name, p.last_name FROM positions_search AS s, positions LEFT JOIN persons AS p ON p.id = positions.person_id WHERE positions_search MATCH (?) AND positions.id=docid AND positions.hidden=0 ORDER BY p.id LIMIT (?) OFFSET (?)")
    try stmt.run(text, limit, offset)
    var result: [SearchItem] = []
    for data in stmt {
      let item = SearchItem()
      item.positionId = Int(data[0] as? Int64 ?? 0)
      item.positionName = data[1] as? String ?? ""
      item.divisionName = data[2] as? String ?? ""
      let personId = Int(data[3] as? Int64 ?? 0)
      if personId > 0 {
        let person = Person()
        person.id = personId
        person.sex = Sex(rawValue: Int(data[4] as? Int64 ?? 2))!
        person.photoFileName = data[5] as? String
        person.firstName = data[6] as? String
        person.middleName = data[7] as? String
        person.lastName = data[8] as? String
        item.person = person
      }
      result.append(item)
    }
    return result
  }

  private func getCompetenciesImpl(personId: Int, positionId: Int) throws -> [Competence] {
    return fakeCompetencies.map {
      let c = Competence()
      c.name = $0
      c.requiredValue = Int(arc4random_uniform(4) + 6)
      c.value = Int(arc4random_uniform(4) + 6)
      return c
    }
//    let stmt = try db.prepare("SELECT c.name, cp1.value, cp2.value FROM competence AS c, person_competence AS cp1, position_competence AS cp2 WHERE cp1.person_id = (?) AND cp2.position_id = (?) AND cp1.competence_id = cp2.competence_id AND cp1.competence_id = c.id")
//    try stmt.run(personId, positionId)
//    var result: [Competence] = []
//    for data in stmt {
//      let competence = Competence()
//      competence.name = data[0] as? String ?? ""
//      competence.value = Int(data[1] as? Int64 ?? 0)
//      competence.requiredValue = Int(data[2] as? Int64 ?? 0)
//      result.append(competence)
//    }
//    return result
  }

    private func dateString(yearsAgo count: Int) -> String {
        let date = Calendar.current.date(byAdding: .year, value: -count, to: Date()) ?? Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }

    
  private func getStatisticImpl() throws -> Statistic {
    let statistic = Statistic()

    var stmt = try db.prepare("SELECT 0, count(p1.id), 'до 1 года' AS dt FROM positions p1, persons p2 WHERE p1.person_id = p2.id AND cur_pos_date BETWEEN '\(dateString(yearsAgo: 1))' AND 'now' UNION" +
      " SELECT 1, count(p1.id), 'от 1 до\n3 лет' AS dt FROM positions p1, persons p2 WHERE p1.person_id = p2.id AND cur_pos_date BETWEEN '\(dateString(yearsAgo: 3))' AND '\(dateString(yearsAgo: 1))' UNION" +
      " SELECT 2, count(p1.id), 'от 3 до\n5 лет' AS dt FROM positions p1, persons p2 WHERE p1.person_id = p2.id AND cur_pos_date BETWEEN '\(dateString(yearsAgo: 5))' AND '\(dateString(yearsAgo: 3))' UNION" +
      " SELECT 3, count(p1.id), 'от 5 до\n7 лет' AS dt FROM positions p1, persons p2 WHERE p1.person_id = p2.id AND cur_pos_date BETWEEN '\(dateString(yearsAgo: 7))' AND '\(dateString(yearsAgo: 5))' UNION" +
      " SELECT 4, count(p1.id), 'от 7 до\n10 лет' AS dt FROM positions p1, persons p2 WHERE p1.person_id = p2.id AND cur_pos_date BETWEEN '\(dateString(yearsAgo: 10))' AND '\(dateString(yearsAgo: 7))' UNION" +
      " SELECT 5, count(p1.id), 'от 10 лет' AS dt FROM positions p1, persons p2 WHERE p1.person_id = p2.id AND cur_pos_date < '\(dateString(yearsAgo: 10))'")
    try stmt.run()
    for data in stmt {
      let counter = Counter()
      counter.value = Int(data[1] as! Int64)
      counter.name = data[2] as! String
      statistic.positionsByExperience.append(counter)
    }

    stmt = try db.prepare("SELECT p.category_id, c.name, count(p.id) FROM positions AS p, position_categories AS c WHERE c.id = p.category_id AND p.person_id != 0 GROUP BY category_id")
    try stmt.run()
    for data in stmt {
      let counter = Counter()
      counter.name = data[1] as! String
      counter.value = Int(data[2] as! Int64)
      statistic.positionsByCategory.append(counter)
    }

    statistic.vacancies = vacanciesCounters

    stmt = try db.prepare("SELECT pe.sex, pt.name, COUNT(po.id) FROM positions AS po, persons AS pe, position_types AS pt WHERE po.person_id=pe.id AND po.type_id=pt.id GROUP BY pt.id, pe.sex")
    try stmt.run()
    var counter: GenderCounter!
    for data in stmt {
      let name = data[1] as? String ?? ""
      guard !name.isEmpty else { continue }
      switch data[0] as! Int64 {
      case 0:
        counter = GenderCounter()
        counter.name = name
        counter.femaleValue = Int((data[2] as! Int64))
      case 1:
        counter.maleValue = Int((data[2] as! Int64))
        statistic.genderCounters.append(counter)
      default:
        break
      }
    }

    stmt = try db.prepare("SELECT pe.mosreg, COUNT(po.id) FROM positions AS po, persons AS pe WHERE po.person_id = pe.id GROUP BY mosreg")
    try stmt.run()
    var inMosreg: Int64 = 0
    var outMosreg: Int64 = 0
    for data in stmt {
      switch data[0] as! Int64 {
      case 0:
        outMosreg = data[1] as! Int64
      case 1:
        inMosreg = data[1] as! Int64
      default:
        break
      }
    }

    stmt = try db.prepare("SELECT d.name, COUNT(po.id) AS c FROM divisions AS d, positions AS po WHERE po.is_vacancy=1 AND po.root_division_id = d.id GROUP BY po.root_division_id ORDER BY c DESC LIMIT 3")
    try stmt.run()
    for data in stmt {
      let counter = Counter()
      counter.name = data[0] as! String
      counter.value = Int(data[1] as! Int64)
      statistic.topOGVByVacancies.append(counter)
    }

//    stmt = try db.prepare("SELECT name,cnt,size FROM salary_data ORDER BY id")
    stmt = try db.prepare("SELECT p.name, p.avg_salary, COUNT(p.name) as cnt FROM positions p WHERE p.avg_salary IS NOT NULL GROUP BY p.name")
    try stmt.run()
    for data in stmt {
      let item = SalaryDataItem()
      item.name = data[0] as! String
      item.size = Int(data[1] as! Int64)
      item.count = Int(data[2] as! Int64)
      statistic.salaryData.append(item)
    }
    statistic.mosregPercent = 100 * Float(inMosreg) / Float(inMosreg + outMosreg)

    stmt = try db.prepare("SELECT sex, count(*) FROM persons AS pe, positions AS po WHERE po.person_id=pe.id AND sex < 2 GROUP BY pe.sex")
    try stmt.run()
    for data in stmt {
      switch data[0] as? Int64 ?? -1 {
      case 0:
        statistic.totalFemale = Int(data[1] as? Int64 ?? 0)
      case 1:
        statistic.totalMale = Int(data[1] as? Int64 ?? 0)
      default:
        break
      }
    }
    return statistic
  }

  private func createOrderImpl(positionId: Int, type: OrderType) throws -> Order {
    let stmt = try db.prepare("INSERT INTO orders (type,status,position_id,created,updated) VALUES(?,?,?,'now','now')")
    try stmt.run(type.rawValue, 0, positionId)
    return try getOrderImpl(id: Int(db.lastInsertRowid))
  }

  private func setOrderStatusImpl(orderId: Int, newStatus: OrderStatus) throws {
    let stmt = try db.prepare("UPDATE orders SET status=?, updated='now' WHERE id=?")
    try stmt.run(newStatus.rawValue, orderId)
    guard db.changes > 0 else {
      throw Error.notFound
    }
  }

  private func updateOrdersImpl(oldStatus: OrderStatus, newStatus: OrderStatus) throws {
    if newStatus == .trash {
      let stmt = try db.prepare("DELETE FROM orders WHERE status=?")
      try stmt.run(oldStatus.rawValue)
    } else {
      let stmt = try db.prepare("UPDATE orders SET status=?, updated='now' WHERE status=?")
      try stmt.run(newStatus.rawValue, oldStatus.rawValue)
    }
  }

  private func getOrderImpl(id: Int) throws -> Order {
    let stmt = try db.prepare("SELECT o.id, o.type, o.status, o.created, updated, po.id, po.name, pe.id, pe.first_name, pe.middle_name, pe.last_name, pe.photo_name, pe.sex, d.id, d.name FROM orders AS o, positions AS po, persons AS pe LEFT JOIN divisions AS d ON po.division_id=d.id WHERE o.id=? AND o.position_id=po.id AND po.person_id=pe.id")
    if let data = stmt.bind(id).next() {
      return getOrder(data: data)
    }
    throw Error.notFound
  }

  private func getOrdersImpl() throws -> [Order] {
    var result: [Order] = []
    let stmt = try db.prepare("SELECT o.id, o.type, o.status, o.created, o.updated, po.id, po.name, pe.id, pe.first_name, pe.middle_name, pe.last_name, pe.photo_name, pe.sex, d.id, d.name FROM orders AS o, positions AS po, persons AS pe LEFT JOIN divisions AS d ON po.division_id=d.id WHERE o.position_id=po.id AND po.person_id=pe.id")
    try stmt.run()
    for data in stmt {
      result.append(getOrder(data: data))
    }
    return result
  }

  private func getOrder(data: Statement.Element) -> Order {
    let order = Order()
    order.id = Int(data[0] as! Int64)
    order.type = OrderType(rawValue: Int(data[1] as! Int64))!
    order.status = OrderStatus(rawValue: Int(data[2] as! Int64))!
    order.createDate = dateFormatter.date(from: data[3] as? String ?? "")
    order.changeDate = dateFormatter.date(from: data[4] as? String ?? "")
    if let positionId = data[5] as? Int64, positionId > 0 {
      let position = Position()
      order.position = position
      position.id = Int(positionId)
      position.name = data[6] as? String
      if let personId = data[7] as? Int64, personId > 0 {
        let person = Person()
        position.person = person
        person.id = Int(personId)
        person.firstName = data[8] as? String
        person.middleName = data[9] as? String
        person.lastName = data[10] as? String
        person.photoFileName = data[11] as? String
        person.sex = Sex(rawValue: Int(data[12] as? Int64 ?? 2))!
      }
      let divisionId = Int(data[13] as? Int64 ?? 0)
      if divisionId > 0 {
        let division = Division()
        division.id = divisionId
        division.name = data[14] as! String
        position.division = division
      }
    }
    return order
  }

  private func getNumVacanciesForOGVImpl() throws -> [Division] {
    var result: [Division] = []
    let stmt = try db.prepare("SELECT d.id, d.name, COUNT(po.id) AS c, SUM(CASE WHEN po.is_vacancy = 1 THEN 1 ELSE 0 END) AS cv FROM divisions AS d LEFT JOIN positions AS po ON po.root_division_id = d.id GROUP BY po.root_division_id ORDER BY d.name")
    try stmt.run()
    for data in stmt {
      let division = Division()
      division.id = Int(data[0] as! Int64)
      division.name = data[1] as! String
      division.numPositions = Int(data[2] as! Int64)
      division.numVacancies = Int(data[3] as! Int64)
      result.append(division)
    }
    return result
  }

  private func getNumVacanciesForCategoriesImpl() throws -> [PositionCategory] {
    var result: [PositionCategory] = []
    let stmt = try db.prepare("SELECT t.id, t.name, COUNT(po.id) AS c, SUM(CASE WHEN po.is_vacancy = 1 THEN 1 ELSE 0 END) AS cv FROM position_types AS t LEFT JOIN positions AS po ON po.type_id = t.id WHERE t.name != '' GROUP BY po.type_id ORDER BY t.name")
    try stmt.run()
    for data in stmt {
      let cat = PositionCategory()
      cat.id = Int(data[0] as! Int64)
      cat.name = data[1] as! String
      cat.numPositions = Int(data[2] as! Int64)
      cat.numVacancies = Int(data[3] as! Int64)
      result.append(cat)
    }
    return result
  }

  private func getVacanciesImpl(divisionId: Int) throws -> [Position] {
    var result: [Position] = []
    let stmt = try db.prepare("SELECT po.id, po.name, d.id, d.name, v.num_candidates FROM positions AS po LEFT JOIN vacancies AS v ON po.id=v.position_id LEFT JOIN divisions AS d ON po.division_id=d.id WHERE root_division_id=? AND po.is_vacancy=1 ORDER BY po.weight")
    try stmt.run(divisionId)
    for data in stmt {
      let position = Position()
      position.id = Int(data[0] as! Int64)
      position.name = data[1] as! String
      let divisionId = data[2] as? Int64 ?? 0
      if divisionId > 0 {
        let division = Division()
        division.id = Int(divisionId)
        division.name = data[3] as! String
        position.division = division
      }
      position.hasCandidates = (data[4] as? Int64 ?? 0) > 0
      result.append(position)
    }
    return result
  }

  private func getVacanciesImpl(categoryId: Int) throws -> [Position] {
    var result: [Position] = []
    let stmt = try db.prepare("SELECT po.id, po.name, d.id, d.name, v.num_candidates, d2.id, d2.name FROM positions AS po LEFT JOIN vacancies AS v ON po.id=v.position_id LEFT JOIN divisions AS d ON po.division_id=d.id LEFT JOIN divisions AS d2 ON po.root_division_id=d2.id WHERE type_id=? AND po.is_vacancy=1 ORDER BY po.weight")
    try stmt.run(categoryId)
    for data in stmt {
      let position = Position()
      position.id = Int(data[0] as! Int64)
      position.name = data[1] as! String
      let divisionId = data[2] as? Int64 ?? 0
      if divisionId > 0 {
        let division = Division()
        division.id = Int(divisionId)
        division.name = data[3] as! String
        position.division = division
      }
      position.hasCandidates = (data[4] as? Int64 ?? 0) > 0
      if let id = data[5] as? Int64 {
        let division = Division()
        division.id = Int(id)
        division.name = data[6] as! String
        position.headDivision = division
      }
      result.append(position)
    }
    return result
  }

  private func getAllVacanciesImpl() throws -> [Position] {
    var result: [Position] = []
    let stmt = try db.prepare("SELECT po.id, po.name, d.id, d.name, v.num_candidates, d2.id, d2.name FROM positions AS po LEFT JOIN vacancies AS v ON po.id=v.position_id LEFT JOIN divisions AS d ON po.division_id=d.id LEFT JOIN divisions AS d2 ON po.root_division_id=d2.id WHERE po.is_vacancy=1 ORDER BY po.weight")
    try stmt.run()
    for data in stmt {
      let position = Position()
      position.id = Int(data[0] as! Int64)
      position.name = data[1] as! String
      let divisionId = data[2] as? Int64 ?? 0
      if divisionId > 0 {
        let division = Division()
        division.id = Int(divisionId)
        division.name = data[3] as! String
        position.division = division
      }
      position.hasCandidates = (data[4] as? Int64 ?? 0) > 0
      if let id = data[5] as? Int64 {
        let division = Division()
        division.id = Int(id)
        division.name = data[6] as! String
        position.headDivision = division
      }
      result.append(position)
    }
    return result
  }

  private func getCandidatesImpl(positionId: Int) throws -> [VacancyCandidate] {
    var result: [VacancyCandidate] = []
    let stmt = try db.prepare("SELECT po.id, po.name, pe.id, pe.first_name, pe.middle_name, pe.last_name, pe.sex, pe.photo_name, vc.flags, d.id, d.name FROM persons AS pe, vacancy_candidates AS vc LEFT JOIN positions AS po ON po.person_id=pe.id LEFT JOIN divisions AS d ON po.division_id = d.id WHERE vc.position_id=? AND vc.person_id=pe.id")
    try stmt.run(positionId)
    for data in stmt {
      let candidate = VacancyCandidate()
      candidate.positionId = positionId
      if let posId = data[0] as? Int64 {
        let position = Position()
        position.id = Int(posId)
        position.name = data[1] as! String
        if let divisionId = data[9] as? Int64 {
          let division = Division()
          division.id = Int(divisionId)
          division.name = data[10] as! String
          position.division = division
        }
        candidate.candidate = position
      }
      let person = Person()
      person.id = Int(data[2] as! Int64)
      person.firstName = data[3] as? String
      person.middleName = data[4] as? String
      person.lastName = data[5] as? String
      person.sex = Sex(rawValue: Int(data[6] as? Int64 ?? 2))!
      person.photoFileName = data[7] as? String
      candidate.person = person

      candidate.competence = try getCompetenciesImpl(personId: person.id, positionId: positionId)
      candidate.flags = VacancyCandidate.Flags(rawValue: data[8] as? Int64 ?? 0)
      result.append(candidate)
    }
    return result
  }

  private func getFakeCandidatesImpl(positionId: Int) throws -> [VacancyCandidate] {
    var result: [VacancyCandidate] = []
    let stmt = try db.prepare("SELECT po.id, po.name, pe.id, pe.first_name, pe.middle_name, pe.last_name, pe.sex, pe.photo_name FROM positions AS po, persons AS pe WHERE po.level > 3 AND po.person_id=pe.id AND po.id != ? AND po.type_id=(SELECT type_id FROM positions WHERE id=?) AND po.name NOT LIKE 'Руководитель аппарата мирового судьи Московской области%' AND po.name NOT LIKE 'Секретарь суд%' LIMIT 3 OFFSET ((? % 10) + 1)")
    try stmt.run(positionId, positionId, positionId)
    for data in stmt {
      let candidate = VacancyCandidate()
      candidate.positionId = positionId
      let position = Position()
      position.id = Int(data[0] as! Int64)
      position.name = data[1] as! String
      let person = Person()
      candidate.person = person
      person.id = Int(data[2] as! Int64)
      person.firstName = data[3] as? String
      person.middleName = data[4] as? String
      person.lastName = data[5] as? String
      person.sex = Sex(rawValue: Int(data[6] as? Int64 ?? 2))!
      person.photoFileName = data[7] as? String

      candidate.candidate = position
      candidate.competence = try getCompetenciesImpl(personId: person.id, positionId: positionId)
      if result.count >= 3 {
        candidate.flags.update(with: .bossApproval)
      }

      result.append(candidate)
    }
    if let candidate = try getFirstSubordinate(positionId: positionId) {
      result.append(candidate)
    }
    return result
  }

  private func getFirstSubordinate(positionId: Int) throws -> VacancyCandidate? {
    let stmt = try db.prepare("SELECT po.id, po.name, pe.id, pe.first_name, pe.middle_name, pe.last_name, pe.sex, pe.photo_name FROM positions AS po, persons AS pe WHERE po.person_id=pe.id AND po.parent_id=? AND po.name NOT LIKE 'Руководитель аппарата мирового судьи Московской области%' AND po.name NOT LIKE 'Секретарь суд%' LIMIT 1")
    try stmt.run(positionId)
    for data in stmt {
      let candidate = VacancyCandidate()
      candidate.positionId = positionId
      let position = Position()
      position.id = Int(data[0] as! Int64)
      position.name = data[1] as! String
      let person = Person()
      candidate.person = person
      person.id = Int(data[2] as! Int64)
      person.firstName = data[3] as? String
      person.middleName = data[4] as? String
      person.lastName = data[5] as? String
      person.sex = Sex(rawValue: Int(data[6] as? Int64 ?? 2))!
      person.photoFileName = data[7] as? String

      candidate.candidate = position
      candidate.competence = try getCompetenciesImpl(personId: person.id, positionId: positionId)
      return candidate
    }
    return nil
  }

  private func addCandidateImpl(positionId: Int, personId: Int) throws {
    if try db.run(Update("UPDATE vacancy_candidates SET flags = flags | ? WHERE position_id=? AND person_id=?", [VacancyCandidate.Flags.selected.rawValue, positionId, personId])) > 0 { return }
    try db.run(Insert("INSERT INTO vacancy_candidates(position_id,person_id,flags) VALUES(?,?,?)", [positionId, personId, VacancyCandidate.Flags.selected.rawValue]))
  }

  private func deleteCandidateImpl(positionId: Int, personId: Int) throws {
    if try db.run(Delete("DELETE FROM vacancy_candidates WHERE position_id=? AND person_id=? AND flags=?", [positionId, personId, VacancyCandidate.Flags.selected.rawValue])) > 0 { return }
    try db.run(Update("UPDATE vacancy_candidates SET flags = flags & ? WHERE position_id=? AND person_id=?", [~VacancyCandidate.Flags.selected.rawValue, positionId, personId]))
  }

  private func searchCandidatesImpl(positionId: Int, text: String, offset: Int, limit: Int) throws -> [VacancyCandidate] {
    let text = text.components(separatedBy: " ").filter({ !$0.isEmpty }).joined(separator: "* ") + "*"
    var result: [VacancyCandidate] = []
    let stmt = try db.prepare("SELECT po.id, po.name, d.id, d.name, pe.id, pe.first_name, pe.middle_name, pe.last_name, pe.sex, pe.photo_name, vc.flags FROM positions_search, positions AS po, persons AS pe LEFT JOIN divisions AS d ON po.division_id=d.id LEFT JOIN vacancy_candidates AS vc ON vc.position_id=? AND vc.person_id=po.person_id WHERE positions_search MATCH (?) AND po.id=docid AND po.hidden=0 AND po.person_id = pe.id LIMIT (?) OFFSET (?)")
    try stmt.run(positionId, text, limit, offset)
    for data in stmt {
      let candidate = VacancyCandidate()
      candidate.positionId = positionId
      let position = Position()
      position.id = Int(data[0] as! Int64)
      position.name = data[1] as! String
      if let divisionId = data[2] as? Int64, divisionId > 0 {
        let division = Division()
        division.id = Int(divisionId)
        division.name = data[3] as? String ?? ""
        position.division = division
      }
      let person = Person()
      candidate.person = person
      person.id = Int(data[4] as! Int64)
      person.firstName = data[5] as? String
      person.middleName = data[6] as? String
      person.lastName = data[7] as? String
      person.sex = Sex(rawValue: Int(data[8] as? Int64 ?? 2))!
      person.photoFileName = data[9] as? String

      candidate.candidate = position
      candidate.competence = try getCompetenciesImpl(personId: person.id, positionId: positionId)
      candidate.flags = VacancyCandidate.Flags(rawValue: data[10] as? Int64 ?? 0)

      result.append(candidate)
    }
    return result
  }

}
