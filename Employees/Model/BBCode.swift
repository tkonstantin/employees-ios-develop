//
//  BBCode.swift
//  Employees
//
//  Created by Sergey Erokhin on 26/04/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

protocol TextHighlighter : class {
  
  func highlight(text: NSMutableAttributedString)

}

final class BBCodeParser {
  let tagRegEx = try! NSRegularExpression(pattern: "\\[(\\/)?(\\w+)(?:=([^\\[]+))?\\]", options: [])
  init() {}
  var supportedTags: Set<String> = ["b", "url"]
  func parse(text: String) -> [BBCodeElement] {
    var parentNode: BBCodeNode?
    var result: [BBCodeElement] = []
    let text = NSMutableString(string: text)
    let range = NSRange(location: 0, length: text.length)
    var pos: Int = 0
    for match in tagRegEx.matches(in: text as String, options: [], range: range) {
      if match.range.location > pos {
        let text = BBCodeText(text.substring(with: NSRange(location: pos, length: match.range.location - pos)))
        if let parent = parentNode {
          parent.childs.append(text)
        } else {
          result.append(text)
        }
      }
      let tag = text.substring(with: match.rangeAt(2))
      guard supportedTags.contains(tag) else { continue }
      let isClose = match.rangeAt(1).location != NSNotFound
      if isClose {
        if let parent = parentNode, parent.name == tag {
          parentNode = parent.parent
        }
      } else {
        let valueRange = match.rangeAt(3)
        let node = BBCodeNode(tag, value: valueRange.location == NSNotFound ? nil : text.substring(with: valueRange))
        if let parent = parentNode {
          node.parent = parent
          parent.childs.append(node)
        } else {
          result.append(node)
        }
        parentNode = node
      }
      pos = match.range.location + match.range.length
    }
    if pos <= text.length - 1 {
      result.append(BBCodeText(text.substring(from: pos)))
    }
    return result
  }
}

final class BBCodeHelper {
  typealias AttrData = (range: NSRange, name: String, value: Any)
  var font: UIFont = UIFont.systemFont(ofSize: 14)
  var boldFont: UIFont = UIFont.systemFont(ofSize: 14, weight: UIFontWeightBold)
  var textColor: UIColor = .black
  var highlighter: TextHighlighter?
  private var rawText: String = ""
  private var attrData: [AttrData] = []
  private(set) var paymentURL: URL?
  private(set) var imageURLs: [URL] = []

  func translate(text: String) -> NSAttributedString {
    attrData.removeAll()
    rawText = ""
    paymentURL = nil
    imageURLs.removeAll()
    let elements = BBCodeParser().parse(text: text)
    elements.forEach { apply($0) }
    let result = NSMutableAttributedString(string: rawText, attributes: [NSForegroundColorAttributeName: textColor, NSFontAttributeName: font])
    for attr in attrData.reversed() {
      result.addAttribute(attr.name, value: attr.value, range: attr.range)
    }
    highlighter?.highlight(text: result)
    if rawText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
      return NSAttributedString()
    }
    return result
  }

  func apply(_ element: BBCodeElement) {
    if let text = element as? BBCodeText {
      rawText += text.text
    } else if let node = element as? BBCodeNode {
      let location = rawText.characters.count

      for e in node.childs {
        apply(e)
      }

      let range = NSRange(location: location, length: rawText.characters.count - location)

      switch node.name {
      case "url":
        if let url = URL(string: node.value ?? "") {
          attrData.append((range, NSLinkAttributeName, url))
        }
      case "b":
        attrData.append((range, NSFontAttributeName, boldFont))
      default:
        break
      }
    }
  }
}

enum BBCodeElementType {
  case text
  case node
}

protocol BBCodeElement : class {
  var type: BBCodeElementType { get }
}

final class BBCodeText : BBCodeElement {
  let text: String
  var type: BBCodeElementType { return .text }
  init(_ text: String) {
    self.text = text
  }
}

final class BBCodeNode : BBCodeElement {
  let name: String
  let value: String?
  var type: BBCodeElementType { return .node }
  weak var parent: BBCodeNode?
  var childs: [BBCodeElement] = []
  init(_ name: String, value: String?) {
    self.name = name
    self.value = value
  }
}

