//
//  Biography.swift
//  Employees
//
//  Created by Sergey Erokhin on 23/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

final class Biography {

  struct UX {
    static let bulletPrefix = "   •   "
    static let prefixWidth = (bulletPrefix as NSString).size(attributes: String.attributes(.sysFont(16, .regular))).width
    static let textAttrs = String.attributes(.sysFont(16, .regular), .ps(20), .headIndent(prefixWidth), .firstLineHeadIndent(0))
  }

  private(set) var items: [BiographyItem] = []

  init(person: Person) {
    if !person.education.isEmpty {
      let text = person.education.map({ UX.bulletPrefix + $0 }).joined(separator: "\n")
      let attrText = NSAttributedString(string: text, attributes: UX.textAttrs)
      items.append(BiographyItem(type: .education, text: attrText))
    }
    if !person.seminars.isEmpty {
      let text = person.seminars.map({ UX.bulletPrefix + $0 }).joined(separator: "\n")
      let attrText = NSAttributedString(string: text, attributes: UX.textAttrs)
      items.append(BiographyItem(type: .seminars, text: attrText))
    }
    if !person.experience.isEmpty {
      let text = person.experience.map({ UX.bulletPrefix + $0 }).joined(separator: "\n")
      let attrText = NSAttributedString(string: text, attributes: UX.textAttrs)
      items.append(BiographyItem(type: .experience, text: attrText))
    }
    if !person.achievements.isEmpty {
      let text = person.achievements.map({ UX.bulletPrefix + $0 }).joined(separator: "\n")
      let attrText = NSAttributedString(string: text, attributes: UX.textAttrs)
      items.append(BiographyItem(type: .achievements, text: attrText))
    }
  }

}

enum BiographyItemType {
  case education
  case experience
  case seminars
  case achievements
}

final class BiographyItem {

  let text: NSAttributedString
  let type: BiographyItemType

  init(type: BiographyItemType, text: NSAttributedString) {
    self.type = type
    self.text = text
  }

}
