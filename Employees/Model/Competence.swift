//
//  Competence.swift
//  Employees
//
//  Created by Sergey Erokhin on 29.03.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import ObjectMapper

final class Competence : Mappable {

  var name: String = ""
  var requiredValue: Int = 0
  var value: Int = 0


  init(name: String, requiredValue: Int, value: Int) {
    self.name = name
    self.requiredValue = requiredValue
    self.value = value
  }

  init() {}

  init?(map: Map) {}

  func mapping(map: Map) {
    name <- map["name"]
    requiredValue <- map["requiredValue"]
    value <- map["value"]
  }

}
