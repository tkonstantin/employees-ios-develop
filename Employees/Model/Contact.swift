//
//  Contact.swift
//  Employees
//
//  Created by Sergey Erokhin on 22/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import ObjectMapper

enum ContactType: Int
{
  case unknown = 0
  case workPhone = 1
  case mobilePhone = 2
  case email = 3
  case address = 4
  case www = 5
  case vkontakte = 10
  case facebook = 11
  case googlePlus = 12
  case twitter = 13
  case instagram = 14
  case odnoklassniki = 15
}

final class Contact : Mappable {

  var type: ContactType = .unknown
  var value: String = ""

  init() {}

  init?(map: Map) {}

  func mapping(map: Map) {
    type <- map["type"]
    value <- map["value"]
  }

}
