//
//  Employee.swift
//  Employees
//
//  Created by Sergey Erokhin on 31.03.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import ObjectMapper

final class Employee : Mappable {

  var person: Person?
  var position: Position?
  var competencies: [Competence] = []

  init?(map: Map) {}

  func mapping(map: Map) {
    person <- map["person"]
    position <- map["position"]
    competencies <- map["competencies"]
  }

}
