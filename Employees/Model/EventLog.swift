//
//  EventLogger.swift
//  Employees
//
//  Created by Sergey Erokhin on 04/04/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import Alamofire
import ObjectMapper
import AdSupport

final class EventLog {

  final class Event : Mappable {
    private(set) var hash: String!
    private(set) var text: String!
    private(set) var date: Date!

    init(text: String, hash: String) {
      self.text = text
      self.hash = hash
      date = Date()
    }

    init?(map: Map) {}

    func mapping(map: Map) {
      text <- map["event"]
      hash <- map["hash"]
      date <- (map["date"], ISO8601DateTransform())
    }

  }

  private var events: [Event] = []
  private var hash = ASIdentifierManager.shared().advertisingIdentifier.uuidString.replacingOccurrences(of: "-", with: "")
  private let logsPath: String
  private var isSending: Bool = false
  private var files: [String] = []

  private var sendLogUrl = "\(EmployeesServerURLPrefix)/api/logs"

  static let instance = EventLog()

  private init() {
    logsPath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first! + "/Events"
    let fm = FileManager.default
    if !fm.fileExists(atPath: logsPath) {
      try! fm.createDirectory(atPath: logsPath, withIntermediateDirectories: true)
    }
  }

  func add(_ event: String) {
    print(event)
    events.append(Event(text: event, hash: hash))
    if events.count >= 100 {
      save()
    }
  }

  func save() {
    guard !events.isEmpty else { return }
    let log = Mapper<Event>().toJSONString(events, prettyPrint: false)!
    let logFilePath = "\(logsPath)/\(UUID().uuidString).log"
    do {
      try log.write(toFile: logFilePath, atomically: true, encoding: .utf8)
      events.removeAll()
    } catch let error {
      print("Failed to save event log: \(error.localizedDescription)")
    }
  }

  func sendAll() {
    guard !isSending else { return }
    isSending = true
    let fm = FileManager.default
    do {
      files = try fm.contentsOfDirectory(atPath: logsPath).filter({ $0.hasSuffix(".log") })
      guard !files.isEmpty else {
        isSending = false
        return
      }
      sendNextLog()
    } catch let error {
      print(error)
      isSending = false
    }
  }

  private func sendNextLog() {
    guard let name = files.first else {
      isSending = false
      return
    }
    let path = logsPath + "/" + name
    let data = try! Data(contentsOf: URL(fileURLWithPath: path))
    let headers: HTTPHeaders = [
      "Content-Type": "application/json",
      "Accept": "application/json"
    ]
    let request = Alamofire.upload(data, to: sendLogUrl, headers: headers)
    request.validate(statusCode: [200]).responseData {
      switch $0.result {
      case .success(_):
        try? FileManager.default.removeItem(atPath: path)
        self.files.remove(at: 0)
        print("Send log \(name)")
        self.sendNextLog()
      case .failure(let error):
        print(error)
        self.isSending = false
      }
    }
  }

}
