//
//  GraphNode.swift
//  Employees
//
//  Created by Sergey Erokhin on 24/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import ObjectMapper

final class GraphNode : Mappable {

  var id: Int = 0
  var name: String = ""
  var parentId: Int = 0
  var position: Position?
  var childCount: Int = 0
  var numVacancies: Int = 0
  var numWorkplaces: Int = 0

  init() {}

  init?(map: Map) {}

  func mapping(map: Map) {
    id <- map["id"]
    name <- map["name"]
    parentId <- map["parent_id"]
    position <- map["position"]
    childCount <- map["child_count"]
    numVacancies <- map["num_vacancies"]
    numWorkplaces <- map["num_workplaces"]
  }

}
