//
//  Kinship.swift
//  Employees
//
//  Created by Sergey Erokhin on 27/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import ObjectMapper

enum KinshipType : Int {
  case child = 1
  case parent = 2
  case grandparent = 3
  case grandchild = 4
  case sib = 5
  case nephew = 6
  case auntUncle = 7
  case spouse = 8

  func title(for person: Person) -> String {
    switch self {
    case .child:
      return person.sex == .male ? "Сын" : "Дочь"
    case .parent:
      return person.sex == .male ? "Отец" : "Мать"
    case .grandparent:
      return person.sex == .male ? "Дедушка" : "Бабушка"
    case .grandchild:
      return person.sex == .male ? "Внук" : "Внучка"
    case .sib:
      return person.sex == .male ? "Брат" : "Сестра"
    case .nephew:
      return person.sex == .male ? "Племянник" : "Племянница"
    case .auntUncle:
      return person.sex == .male ? "Дядя" : "Тётя"
    case .spouse:
      return person.sex == .male ? "Муж" : "Жена"
    }
  }
}

final class Kinship : Mappable {

  var person: Person?
  var position: Position?
  var type: KinshipType = .child
  var alertLevel: Int = 0

  init() {}

  required init?(map: Map) {}

  func mapping(map: Map) {
    person <- map["person"]
    type <- map["type"]
    position <- map["position"]
    alertLevel <- map["alert_level"]
  }

}
