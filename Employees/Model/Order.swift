//
//  Order.swift
//  Employees
//
//  Created by Sergey Erokhin on 14/04/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import ObjectMapper

enum OrderType : Int {
  case training
  case translate
  case dismiss
}

enum OrderStatus : Int {
  case new
  case confirmed
  case pending
  case done
  case canceled
  case trash
}

final class Order : Mappable {

  var id: Int = 0
  var type: OrderType = .dismiss
  var position: Position?
  var createDate: Date!
  var changeDate: Date!
  var status: OrderStatus = .new

  init() {}

  init?(map: Map) {}

  func mapping(map: Map) {
    id <- map["id"]
    type <- map["type"]
    position <- map["position"]
    createDate <- (map["date"], DateTransform())
    changeDate <- (map["change_date"], DateTransform())
    status <- map["status"]
  }

}
