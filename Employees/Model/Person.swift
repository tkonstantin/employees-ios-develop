//
//  Person.swift
//  Employees
//
//  Created by Sergey Erokhin on 22/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import ObjectMapper

enum Sex : Int {
  case female = 0
  case male = 1
  case unknown = 2
}

final class Person : Mappable {

  var id: Int = 0
  var firstName: String!
  var lastName: String!
  var middleName: String!
  var photoFileName: String!
  var contacts: [Contact]!
  var experience: [String] = []
  var education: [String] = []
  var achievements: [String] = []
  var seminars: [String] = []
  var sex: Sex = .male
  var ranks: [Rank] = []
  var birthDate: Date!
  var startWorkingDate: Date!
  var currentPositionDate: Date!
  var competencies: [Competence] = []

  var fullName: String {
    var tmp: [String] = []
    for name in [lastName, firstName, middleName] {
      if let name = name {
        tmp.append(name)
      }
    }
    return tmp.joined(separator: " ")
  }

  var photoURL: URL? {
    guard let photoFileName = self.photoFileName else { return nil }
//    return URL(string: "\(EmployeesServerURLPrefix)/assets/\(photoFileName).jpg")
    return URL(string: photoFileName)
  }

  init() {}

  init?(map: Map) {}

  func mapping(map: Map) {
    id <- map["id"]
    firstName <- map["first_name"]
    lastName <- map["last_name"]
    middleName <- map["middle_name"]
    contacts <- map["contacts"]
    photoFileName <- map["photo_file_name"]
    experience <- map["experience"]
    achievements <- map["achievements"]
    education <- map["education"]
    seminars <- map["seminars"]
    sex <- map["sex"]
    ranks <- map["ranks"]
    birthDate <- (map["birth_date"], DateTransform())
    startWorkingDate <- (map["start_working_date"], DateTransform())
    currentPositionDate <- (map["cur_pos_date"], DateTransform())
    competencies <- map["competencies"]
  }

}

final class Rank : Mappable {

  var date: Date!
  var name: String = ""

  init() {}

  init?(map: Map) {}

  func mapping(map: Map) {
    date <- map["date"]
    name <- map["name"]
  }

}
