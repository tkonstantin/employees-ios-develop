//
//  Position.swift
//  Employees
//
//  Created by Sergey Erokhin on 23/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import ObjectMapper

final class Position : Mappable {

  var id: Int = 0
  var name: String!
  var person: Person?
  var competencies: [Competence] = []
  var division: Division?
  var numVacancies: Int = 0
  var numWorkplaces: Int = 0
  var level: Int = 0
  var vacancyInfo: VacancyInfo?
  var headDivision: Division?
  var typeId: Int = 0
  var hasCandidates: Bool = false

  init() {}

  init?(map: Map) {}

  func mapping(map: Map) {
    id <- map["id"]
    name <- map["name"]
    person <- map["person"]
    competencies <- map["competencies"]
    division <- map["division"]
    numVacancies <- map["num_vacancies"]
    numWorkplaces <- map["num_workplaces"]
    level <- map["level"]
    vacancyInfo <- map["vacancy_info"]
    headDivision <- map["head_division"]
    typeId <- map["type_id"]
    hasCandidates <- map["has_candidates"]
  }

}

final class VacancyInfo : Mappable {

  var tasks: String = ""
  var requiredExperience: String = ""
  var openDate: Date?

  init() {}

  init?(map: Map) {}

  func mapping(map: Map) {
    tasks <- map["tasks"]
    openDate <- map["open_date"]
    requiredExperience <- map["required_experience"]
  }

}
