//
//  PositionCategory.swift
//  Employees
//
//  Created by Sergey Erokhin on 20/04/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import ObjectMapper

final class PositionCategory : Mappable {

  var id: Int = 0
  var name: String = ""
  var numVacancies: Int = 0
  var numPositions: Int = 0

  init() {}

  init?(map: Map) {}

  func mapping(map: Map) {
    id <- map["id"]
    name <- map["name"]
    numVacancies <- map["num_vacancies"]
    numPositions <- map["num_positions"]
  }

}
