//
//  Search.swift
//  Employees
//
//  Created by Sergey Erokhin on 28.03.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import Foundation
import RxSwift

final class Search : SearchHighligter {

  typealias LoadingResult = Result<CountableRange<Int>>

  private(set) var items: [SearchItem] = []

  let loadingResultSubject = PublishSubject<LoadingResult>()
  var loadingResult: Observable<LoadingResult> {
    return loadingResultSubject.asObservable()
  }

  private var request: Request?
  private var requestVersion: Int = 0
  private var currentVersion: Int = 0
  private var actualVersion: Int = 1

  var isNeedReload: Bool {
    return actualVersion != currentVersion
  }

  var isLoading: Bool {
    return request != nil
  }

  private(set) var ended: Bool = false

  var text: String = "" {
    didSet {
      guard text != oldValue else { return }
      actualVersion += 1
      loadMore()
      let tokens = text.components(separatedBy: " ").map({ $0.trimmingCharacters(in: .whitespacesAndNewlines) }).filter({ !$0.isEmpty })
      guard !tokens.isEmpty else {
        hlRegEx = nil
        return
      }
      let pattern = "(?:^|\\.|\\s)(" + tokens.joined(separator: "|") + ")"
      hlRegEx = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
      print(pattern)
    }
  }

  private var hlRegEx: NSRegularExpression?

  func loadMore() {
    let reload = requestVersion != actualVersion
    if reload && request != nil {
      request?.cancel()
      request = nil
    }

    guard !text.isEmpty else {
      currentVersion = actualVersion
      requestVersion = actualVersion
      items.removeAll()
      loadingResultSubject.onNext(.success(0 ..< 0))
      return
    }

    let ver = actualVersion
    requestVersion = ver
    let offset: Int = reload ? 0 : items.count
    request = Client.instance.search(text: text, offset: offset, limit: 20) { [weak self] result in
      guard let `self` = self, self.requestVersion == ver else { return }
      self.request = nil
      switch result {
      case .success(let items):
        self.currentVersion = ver
        if reload {
          self.items.removeAll()
        }
        let start = self.items.count
        self.items += items
        self.ended = items.count < 20
        self.loadingResultSubject.onNext(.success(start ..< self.items.count))
      case .failure(let error):
        self.loadingResultSubject.onError(error)
      }
    }
  }

  func highlight(text: NSMutableAttributedString) {
    guard let re = hlRegEx else { return }
    re.enumerateMatches(in: text.string, options: [], range: NSRange(location: 0, length: text.length)) { result, flags, _ in
      guard let result = result else { return }
      text.addAttributes([NSForegroundColorAttributeName: UIColor(rgba: 0x2073d6ff)], range: result.rangeAt(1))
    }
  }
}
