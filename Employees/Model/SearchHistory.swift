//
//  SearchHistory.swift
//  Employees
//
//  Created by Sergey Erokhin on 28.03.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import ObjectMapper

final class SearchHistory {

  static let shared = SearchHistory()

  private(set) var items: [SearchItem]

  private init() {
    if let jsonString = UserDefaults.standard.string(forKey: "searchHistory") {
      items = Mapper<SearchItem>().mapArray(JSONString: jsonString) ?? []
    } else {
      items = []
    }
  }

  func save() {
    let jsonString = Mapper<SearchItem>().toJSONString(items)
    UserDefaults.standard.set(jsonString, forKey: "searchHistory")
  }

  func add(item: SearchItem) {
    if let index = items.index(of: item) {
      if index > 0 {
        items.remove(at: index)
        items.insert(item, at: 0)
      }
    } else {
      items.insert(item, at: 0)
    }
    if items.count > 5 {
      items.removeSubrange(5 ..< items.count)
    }
    save()
  }

}
