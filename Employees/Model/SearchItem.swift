//
//  SearchItem.swift
//  Employees
//
//  Created by Sergey Erokhin on 28.03.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import ObjectMapper

final class SearchItem : Mappable, Equatable {
  var positionName: String = ""
  var divisionName: String = ""
  var positionId: Int = 0
  var person: Person?

  init() {}

  init?(map: Map) {}

  func mapping(map: Map) {
    positionName <- map["position_name"]
    divisionName <- map["division_name"]
    positionId <- map["position_id"]
    person <- map["person"]
  }

}

func ==(lhs: SearchItem, rhs: SearchItem) -> Bool {
  return lhs.positionId == rhs.positionId
}
