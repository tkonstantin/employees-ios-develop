//
//  Statistic.swift
//  Employees
//
//  Created by Sergey Erokhin on 02.04.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import ObjectMapper

final class Statistic : Mappable {

  var positionsByExperience: [Counter] = []
  var positionsByCategory: [Counter] = []
  var vacancies: [Counter] = []
  var topOGVByVacancies: [Counter] = []
  var genderCounters: [GenderCounter] = []
  var mosregPercent: Float = 0
  var salaryData: [SalaryDataItem] = []
  var totalMale: Int = 0
  var totalFemale: Int = 0

  init() {}

  init?(map: Map) {}

  func mapping(map: Map) {
    positionsByExperience <- map["positions_by_experience"]
    positionsByCategory <- map["positions_by_category"]
    vacancies <- map["vacancies"]
    genderCounters <- map["gender_counters"]
    mosregPercent <- map["mosreg_percent"]
    topOGVByVacancies <- map["top_ogv"]
    salaryData <- map["salary_data"]
    totalMale <- map["total_male"]
    totalFemale <- map["total_female"]
  }

}

final class GenderCounter : Mappable {

  var name: String = ""
  var maleValue: Int = 0
  var femaleValue: Int = 0

  init() {}

  init(name: String, male: Int, female: Int) {
    self.name = name
    maleValue = male
    femaleValue = female
  }

  init?(map: Map) {}

  func mapping(map: Map) {
    name <- map["name"]
    maleValue <- map["male"]
    femaleValue <- map["female"]
  }

}

final class Counter : Mappable {

  var name: String = ""
  var value: Int = 0

  init() {}

  init(name: String, value: Int) {
    self.name = name
    self.value = value
  }

  init?(map: Map) {}

  func mapping(map: Map) {
    name <- map["name"]
    value <- map["value"]
  }

}

final class SalaryDataItem : Mappable {

  var name: String = ""
  var count: Int = 0
  var size: Int = 0

  init() {}

  init?(map: Map) {}

  func mapping(map: Map) {
    name <- map["name"]
    count <- map["count"]
    size <- map["size"]
  }

}
