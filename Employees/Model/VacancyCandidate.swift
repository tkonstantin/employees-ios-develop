//
//  VacancyCandidate.swift
//  Employees
//
//  Created by Sergey Erokhin on 28.04.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import ObjectMapper

final class VacancyCandidate : Mappable, Equatable {

  enum Action {
    case add
    case delete
  }

  struct Flags : OptionSet {
    let rawValue: Int64
    static let bossApproval = Flags(rawValue: 1 << 0)
    static let staffAgency = Flags(rawValue: 1 << 1)
    static let federalReserve = Flags(rawValue: 1 << 2)
    static let ai = Flags(rawValue: 1 << 3)
    static let selected = Flags(rawValue: 1 << 10)
  }

  var positionId: Int = 0
  var candidate: Position?
  var person: Person?
  var competence: [Competence] = []
  var isSelected: Bool {
    get {
      return flags.contains(.selected)
    }
    set {
      if newValue {
        flags.update(with: .selected)
      } else {
        flags.subtract(.selected)
      }
    }
  }
  var flags: Flags = []

  init() {}

  init?(map: Map) {}

  func mapping(map: Map) {
    positionId <- map["position_id"]
    candidate <- map["candidate"]
    person <- map["person"]
    competence <- map["competence"]
    flags <- map["flags"]
  }

}

func ==(lhs: VacancyCandidate, rhs: VacancyCandidate) -> Bool {
  return lhs.positionId == rhs.positionId && lhs.candidate?.id == rhs.candidate?.id && lhs.person?.id == rhs.person?.id
}
