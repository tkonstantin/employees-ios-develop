//
//  BiographyCell.swift
//  Employees
//
//  Created by Sergey Erokhin on 23/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

final class BiographyCell : UITableViewCell {

  @IBOutlet private var icImageView: UIImageView!
  @IBOutlet private var titleLabel: UILabel!
  @IBOutlet private var label: UILabel!

  var item: BiographyItem? {
    didSet {
      guard let item = self.item else { return }
      switch item.type {
      case .education:
        setup(#imageLiteral(resourceName: "icEducation"), title: "Образование", text: item.text)
      case .achievements:
        setup(#imageLiteral(resourceName: "icAchievements"), title: "Награды", text: item.text)
      case .experience:
        setup(#imageLiteral(resourceName: "icExperience"), title: "Работа в органах государственной власти", text: item.text)
      case .seminars:
        setup(#imageLiteral(resourceName: "icSeminars"), title: "Семинары", text: item.text)
      }
    }
  }

  var infoItem: InfoItem? {
    didSet {
      guard let infoItem = self.infoItem else { return }
      switch infoItem.type {
      case .division:
        setup(#imageLiteral(resourceName: "icDepartment"), title: "Орган государственной власти", text: infoItem.text)
      case .position:
        setup(#imageLiteral(resourceName: "icPosition"), title: "Должность", text: infoItem.text)
      case .rank:
        setup(#imageLiteral(resourceName: "icRank"), title: "Классный чин федеральной государственной гражданской службы", text: infoItem.text)
      case .experience:
        setup(#imageLiteral(resourceName: "icExperience"), title: "Общий стаж государственной службы", text: infoItem.text)
      case .rewards:
        setup(#imageLiteral(resourceName: "icAchievements"), title: "Награды", text: infoItem.text)
      }
    }
  }

  private func setup(_ image: UIImage, title: String, text: NSAttributedString) {
    icImageView.image = image
    titleLabel.text = title
    label.attributedText = text
  }

}
