//
//  ContactCell.swift
//  Employees
//
//  Created by Sergey Erokhin on 23/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

func formattedPhoneNumber(_ phone: String) -> String {
  return phone
}

final class ContactCell : UITableViewCell {

  @IBOutlet private var icImageView: UIImageView!
  @IBOutlet private var titleLabel: UILabel!
  @IBOutlet private var subtitleLabel: UILabel!

  private func setup(_ image: UIImage?, title: String, subtitle: String? = nil) {
    icImageView.image = image
    titleLabel.text = title
    if let text = subtitle, !text.isEmpty {
      subtitleLabel.text = text
      subtitleLabel.isHidden = false
    } else {
      subtitleLabel.isHidden = true
      subtitleLabel.text = nil
    }
  }

  var contact: Contact! {
    didSet {
      switch contact.type {
      case .address:
        setup(#imageLiteral(resourceName: "icAdderss"), title: "Место жительства", subtitle: contact.value)
      case .email:
        setup(#imageLiteral(resourceName: "icEmail"), title: "Электронная почта", subtitle: contact.value)
      case .workPhone:
        setup(#imageLiteral(resourceName: "icPhone"), title: "Рабочий телефон", subtitle: formattedPhoneNumber(contact.value))
      case .mobilePhone:
        setup(#imageLiteral(resourceName: "icPhone"), title: "Сотовый телефон", subtitle: formattedPhoneNumber(contact.value))
      case .www:
        setup(#imageLiteral(resourceName: "icWeb"), title: "Сайт", subtitle: contact.value)
      case .vkontakte:
        setup(#imageLiteral(resourceName: "icVk"), title: "ВКонтакте")
      case .facebook:
        setup(#imageLiteral(resourceName: "icFacebook"), title: "Facebook")
      case .twitter:
        setup(#imageLiteral(resourceName: "icTwitter"), title: "Twitter")
      case .googlePlus:
        setup(#imageLiteral(resourceName: "icGooglePlus"), title: "Google+")
      case .instagram:
        setup(#imageLiteral(resourceName: "icInstagram"), title: "Instagram")
      case .odnoklassniki:
        setup(#imageLiteral(resourceName: "icOdnoklass"), title: "Одноклассники")
      default:
        break
      }
    }
  }

}
