//
//  EmployeeCareerView.swift
//  Employees
//
//  Created by Sergey Erokhin on 12/04/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import Charts

final class EmployeeCareerView : InfoSideView {

  @IBOutlet private var pieChartView: PieChartView!
  @IBOutlet private var totalDaysLabel: UILabel!
  @IBOutlet private var daysLabel: UILabel!
  @IBOutlet private var ageLabel: UILabel!
  @IBOutlet private var separator: UIView!
  @IBOutlet private var separatorHeightConstraint: NSLayoutConstraint!

  var person: Person! {
    didSet {
      let now = Date()
      if let birthDate = person?.birthDate {
        let years = Calendar.current.dateComponents([.year], from: birthDate, to: now).year!
        ageLabel.text = String.localizedStringWithFormat(NSLocalizedString("%d год", comment: ""), years)
      } else {
        ageLabel.superview?.isHidden = true
      }
      if let date1 = person?.startWorkingDate, let date2 = person?.currentPositionDate {

        if person?.birthDate == nil {
          separator.isHidden = true
        }

        let days1 = Calendar.current.dateComponents([.day], from: date1, to: now).day!
        let days2 = Calendar.current.dateComponents([.day], from: date2, to: now).day!
        totalDaysLabel.text = Calendar.current.dateComponents([.day, .month, .year], from: date1, to: now).stringify()
        daysLabel.text = Calendar.current.dateComponents([.day, .month, .year], from: date2, to: now).stringify()

        let set1 = PieChartDataSet(values: [PieChartDataEntry(value: Double(days2)), PieChartDataEntry(value: Double(max(0, days1 - days2)))], label: nil)
        set1.colors = [UIColor(rgba: 0x38a97bff), UIColor.gubGrid]
        let data = PieChartData(dataSet: set1)
        data.setDrawValues(false)

        pieChartView.data = data
        pieChartView.animate(yAxisDuration: 1)
      } else {
        pieChartView.superview?.isHidden = true
      }
    }
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    setupChartView()
    separatorHeightConstraint.constant = 1 / UIScreen.main.scale
  }

  func setupChartView() {
    pieChartView.noDataText = "Нет данных"
    pieChartView.legend.enabled = false
    pieChartView.chartDescription = nil
    pieChartView.rotationEnabled = false
    pieChartView.highlightPerTapEnabled = false
    pieChartView.holeRadiusPercent = 0.7
    pieChartView.transparentCircleRadiusPercent = 0.75
  }

}

extension DateComponents {

  func stringify() -> String {
    var tmp: [String] = []
    if let years = self.year, years > 0 {
      tmp.append(String.localizedStringWithFormat(NSLocalizedString("%d год", comment: ""), years))
    }
    if let months = self.month, months > 0 {
      tmp.append(String.localizedStringWithFormat(NSLocalizedString("%d месяц", comment: ""), months))
    }
    if tmp.count < 2, let days = self.day, days > 0 {
      tmp.append(String.localizedStringWithFormat(NSLocalizedString("%d день", comment: ""), days))
    }
    return tmp.joined(separator: " ")
  }

}
