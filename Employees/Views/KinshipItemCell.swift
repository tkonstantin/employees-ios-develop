//
//  KinshipItemCell.swift
//  Employees
//
//  Created by Sergey Erokhin on 27.03.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import Kingfisher

final class KinshipItemCell : UICollectionViewCell {

  struct UX {
    static let divisionTextAttrs = String.attributes(.sysFont(17, .regular))
    static let positionTextAttrs = String.attributes(.sysFont(20, .semibold), .psBefore(8))
    static let typeTextAttrs = String.attributes(.sysFont(20, .regular), .ps(8))
    static let nameTextAttrs = String.attributes(.sysFont(17, .light), .lsp(4))
  }

  @IBOutlet private var imageView: UIImageView!
  @IBOutlet private var headerLabel: UILabel!
  @IBOutlet private var nameLabel: UILabel!
  @IBOutlet private var alertButton: UIButton!
  @IBOutlet private var borderView: UIView!

  var item: KinshipItem! {
    didSet {
      headerLabel.attributedText = item.headerText
      nameLabel.attributedText = item.nameText
      headerLabel.isHidden = item.headerText.length == 0
      if item.kinship.position == nil {
        alertButton.isHidden = true
      } else {
        switch item.kinship.alertLevel {
        case 2:
          alertButton.setTitle("В одной структуре ОГВ", for: .normal)
          alertButton.backgroundColor = .gubAlert2
        case 3:
          alertButton.setTitle("В Прямом подчинении", for: .normal)
          alertButton.backgroundColor = .gubAlert3
        default:
          alertButton.setTitle("Работает в ОГВ", for: .normal)
          alertButton.backgroundColor = .gubAlert1
        }
        alertButton.isHidden = false
      }
      let placeholder = item.kinship.person!.sex == .male ? #imageLiteral(resourceName: "placeholderAvatarMale") : #imageLiteral(resourceName: "placeholderAvatarFeemale")
      if let url = item.kinship.person?.photoURL {
        imageView.kf.setImage(with: url, placeholder: placeholder, options: [.transition(.fade(0.3)), .processor(AvatarProcessor(width: 90))])
      } else {
        imageView.image = placeholder
      }
    }
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    borderView.layer.borderWidth = 1 / UIScreen.main.scale
    borderView.layer.borderColor = UIColor.gubGreyish.cgColor
  }

  static func calculateHeight(for item: KinshipItem, width: CGFloat) -> CGFloat {
    var height: CGFloat = 12 + 24 + 24 + 12
    let headerTextWidth = width - 72
    let nameTextWidth = headerTextWidth - 108
    print(headerTextWidth)
    if item.headerText.length > 0 {
      height += 18 + ceil(item.headerText.size(forWidth: headerTextWidth).height)
    }
    height += max(90, item.nameText.size(forWidth: nameTextWidth).height)
    return height
  }

}
