//
//  SearchItemCell.swift
//  Employees
//
//  Created by Sergey Erokhin on 28.03.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import Kingfisher

protocol SearchHighligter : class {

  func highlight(text: NSMutableAttributedString)

}

final class SearchItemCell : UITableViewCell {

  struct UX {
    static let positionAttrs = String.attributes(.sysFont(17, .bold))
    static let nameAttrs = String.attributes(.sysFont(17, .regular), .ps(8))
    static let divisionAttrs = String.attributes(.sysFont(15, .regular))
  }

  @IBOutlet private var imgView: UIImageView!
  @IBOutlet private var label: UILabel!

  override var layoutMargins: UIEdgeInsets {
    get { return .zero }
    set {}
  }

  var highlighter: SearchHighligter?

  var item: SearchItem! {
    didSet {
      let text = NSMutableAttributedString()
      text += (item.positionName + "\n", UX.positionAttrs)
      let personName = item.person?.fullName ?? "Позиция свободна"
      if !item.divisionName.isEmpty {
        text += (personName + "\n", UX.nameAttrs)
        text += (item.divisionName, UX.divisionAttrs)
      } else {
        text += (personName, UX.nameAttrs)
      }
      if let person = item.person {
        var placeholder: UIImage = #imageLiteral(resourceName: "placeholderAvatarArmchair")
        switch person.sex {
        case .male:
          placeholder = #imageLiteral(resourceName: "placeholderAvatarMale")
        case .female:
          placeholder = #imageLiteral(resourceName: "placeholderAvatarFeemale")
        default:
          break
        }
        if let url = person.photoURL {
          imgView.kf.setImage(with: url, placeholder: placeholder, options: [.transition(.fade(0.3)), .processor(AvatarProcessor(width: 90))])
        } else {
          imgView.image = placeholder
        }
      } else {
        imgView.image = #imageLiteral(resourceName: "placeholderAvatarArmchair")
      }
      highlighter?.highlight(text: text)
      label.attributedText = text
    }
  }

}
