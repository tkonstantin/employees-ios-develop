//
//  SeparatorView.swift
//  Employees
//
//  Created by Sergey Erokhin on 02/05/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

@IBDesignable
final class SeparatorView : UIView {

  override var intrinsicContentSize: CGSize {
    return CGSize(width: frame.width, height: 1 / UIScreen.main.scale)
  }

}
