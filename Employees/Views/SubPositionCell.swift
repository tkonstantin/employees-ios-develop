//
//  SubPositionCell.swift
//  Employees
//
//  Created by Sergey Erokhin on 31.03.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit
import Kingfisher

final class SubPositionCell : UITableViewCell {

  @IBOutlet private var nameLabel: UILabel!
  @IBOutlet private var positionLabel: UILabel!
  @IBOutlet private var avatarImageView: UIImageView!

  var position: Position! {
    didSet {
      nameLabel.text = position.person?.fullName ?? "Вакансия"
      positionLabel.text = position.name
      var placeholder: UIImage?
      switch position.person?.sex ?? .unknown {
      case .male:
        placeholder = #imageLiteral(resourceName: "placeholderAvatarMale")
      case .female:
        placeholder = #imageLiteral(resourceName: "placeholderAvatarFeemale")
      case .unknown:
        placeholder = #imageLiteral(resourceName: "placeholderAvatarArmchair")
      }
      if let url = position.person?.photoURL {
        avatarImageView.kf.setImage(with: url, placeholder: placeholder, options: [.transition(.fade(0.3)), .processor(AvatarProcessor(width: 60))])
      } else {
        avatarImageView.image = placeholder
      }
    }
  }

}
