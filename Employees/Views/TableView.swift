//
//  TableView.swift
//  Employees
//
//  Created by Sergey Erokhin on 01.04.17.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

@IBDesignable
class TableView : UITableView {

  @IBInspectable
  var allowsHeaderViewsToFloat : Bool = true

  @IBInspectable
  var allowsFooterViewsToFloat : Bool = true

}
