//
//  VacancyView.swift
//  Employees
//
//  Created by Sergey Erokhin on 24/03/2017.
//  Copyright © 2017 DM Solutions. All rights reserved.
//

import UIKit

@IBDesignable
final class VacancyView : UIView {

  private var barView: UIView!
  private var trackView: UIView!
  private var maskImageView: UIImageView!
  private var totalLabel: UILabel!
  private var freeLabel: UILabel!

  @IBInspectable
  var total: Int = 0 {
    didSet {
      update()
    }
  }

  @IBInspectable
  var free: Int = 0 {
    didSet {
      update()
    }
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  private func setup() {
    let font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)

    totalLabel = UILabel()
    totalLabel.font = font
    totalLabel.textColor = .gubDarkSeafoamGreen
    totalLabel.isOpaque = true
    totalLabel.backgroundColor = .white

    freeLabel = UILabel()
    freeLabel.font = font
    freeLabel.textColor = .gubGreyish
    freeLabel.isOpaque = true
    freeLabel.backgroundColor = .white

    trackView = UIView()
    trackView.backgroundColor = .gubGreyish
    addSubview(trackView)

    barView = UIView()
    barView.backgroundColor = .gubDarkSeafoamGreen
    addSubview(barView)

    maskImageView = UIImageView(image: UIImage(named: "vacancyStencil", in: Bundle(for: VacancyView.self), compatibleWith: nil))
    maskImageView.contentMode = .scaleToFill

    addSubview(totalLabel)
    addSubview(maskImageView)
    addSubview(freeLabel)
  }

  override var intrinsicContentSize: CGSize {
    let height = max(freeLabel.font.lineHeight, maskImageView.image?.size.height ?? 0)
    let imageWidth = maskImageView.image?.size.width ?? 0
    let width: CGFloat = freeLabel.sizeThatFits(CGSize(width: 100, height: height)).width + 8 + imageWidth + 8 + totalLabel.sizeThatFits(CGSize(width: 100, height: height)).width
    return CGSize(width: width, height: height)
  }

  override func layoutSubviews() {
    totalLabel.sizeToFit()
    totalLabel.frame = CGRect(x: 0, y: 0, width: totalLabel.frame.width, height: frame.height)
    freeLabel.sizeToFit()
    freeLabel.frame = CGRect(x: frame.width - freeLabel.frame.width, y: 0, width: freeLabel.frame.width, height: frame.height)
    let progressViewOrigin = CGPoint(x: totalLabel.frame.maxX + 8, y: 0)
    maskImageView.frame = CGRect(origin: progressViewOrigin, size: CGSize(width: freeLabel.frame.minX - 8 - progressViewOrigin.x, height: frame.height))
    trackView.frame = maskImageView.frame
    barView.frame = trackView.frame
    barView.frame.size.width *= 1 - CGFloat(free) / CGFloat(max(1, total))
  }

  private func update() {
    totalLabel.text = String(total)
    freeLabel.text = String(free)
    invalidateIntrinsicContentSize()
    setNeedsLayout()
  }

  override func sizeToFit() {
    frame.size = intrinsicContentSize
  }

}
